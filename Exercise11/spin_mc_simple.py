"""
Simple Monte Carlo for Ising model

Related to course FYS-4096 Computational Physics

Problem 1:
- Make the code to work, that is, include code to where it reads "# ADD"
- Comment the parts with "# ADD" and make any additional comments you 
  think could be useful for yourself later.
- Follow the assignment from ex11.pdf.

Problem 2:
- Add observables: heat capacity, magnetization, magnetic susceptibility
- Follow the assignment from ex11.pdf.

Problem 3:
- Look at the temperature effects and locate the phase transition temperature.
- Follow the assignment from ex11.pdf.

"""


from numpy import *
from matplotlib.pyplot import *
from scipy.special import erf
import numpy as np

class Walker:
    def __init__(self,*args,**kwargs):
        self.spin = kwargs['spin']
        self.nearest_neighbors = kwargs['nn']
        self.sys_dim = kwargs['dim']
        self.coords = kwargs['coords']

    def w_copy(self):
        return Walker(spin=self.spin.copy(),
                      nn=self.nearest_neighbors.copy(),
                      dim=self.sys_dim,
                      coords=self.coords.copy())
    

def Energy(Walkers):
    E = 0.0
    J = 4.0 # given in units of k_B
    # ADD calculation of energy
    for walker in Walkers:
        E += site_Energy(Walkers, walker)
    return E/2

def site_Energy(Walkers,Walker):
    E = 0.0
    J = 4.0 # given in units of k_B
    for k in range(len(Walker.nearest_neighbors)):
        j = Walker.nearest_neighbors[k]
        E += -J*Walker.spin*Walkers[j].spin
    return E

def ising(Nblocks,Niters,Walkers,beta):
    M = len(Walkers)
    Eb = zeros((Nblocks,))
    Mb = zeros((Nblocks,))
    Cvb = zeros((Nblocks,))
    khib = zeros((Nblocks,))
    Accept=zeros((Nblocks,))
    AccCount=zeros((Nblocks,))

    obs_interval = 5
    for i in range(Nblocks):
        EbCount = 0
        for j in range(Niters):
            site = int(random.rand()*M)

            s_old = 1.0*Walkers[site].spin
 
            E_old = site_Energy(Walkers,Walkers[site])

            # ADD selection of new spin to variable s_new
            s_new = -1.0*s_old # Spin is flipped to opposite

            Walkers[site].spin = 1.0*s_new

            E_new = site_Energy(Walkers,Walkers[site])

            # ADD Metropolis Monte Carlo
            q = np.exp(-beta*(E_new-E_old))
            p = np.random.rand()
            AccCount[i] += 1
            if p < q:
                Walkers[site].spin = s_new
                Accept[i] += 1
            else:
                Walkers[site].spin = s_old

            if j % obs_interval == 0:
                E_tot = Energy(Walkers)/M # energy per spin
                Eb[i] += E_tot
                EbCount += 1
                M_tot = calc_magnetization(Walkers)/M
                Cv_tot = calc_heat_capacity(Walkers, 1.0/beta)
                khi_tot = calc_magn_susceptibility(Walkers, 1.0/beta)
                Mb[i] += M_tot
                Cvb[i] += Cv_tot
                khib[i] += khi_tot

        Mb[i] /= EbCount
        Cvb[i] /= EbCount
        Eb[i] /= EbCount
        khib[i] /= EbCount
        Accept[i] /= AccCount[i]
        if (i+1) % 20 == 0:
            print('Block {0}/{1}'.format(i+1,Nblocks))
            print('    E   = {0:.5f}'.format(Eb[i]))
            print('    Acc = {0:.5f}'.format(Accept[i]))


    return Walkers, Eb, Accept, Cvb, Mb, khib

def calc_heat_capacity(Walkers, T):
    M = len(Walkers)
    # Calculate average of energies squared
    E2 = 0.0
    J = 4.0  # given in units of k_B
    for walker in Walkers:
        E2 += (site_Energy(Walkers, walker)/2)**2
    E2 /= M
    # Average energy E
    E = Energy(Walkers)/M

    # Heat capacity calculated with eq 21
    Cv = (E2-E**2)/(T**2)
    return Cv

def calc_magnetization(Walkers):
    M = 0
    for walker in Walkers:
        M += walker.spin
    return M

def calc_magn_susceptibility(Walkers, T):
    M2 = 0
    for walker in Walkers:
        M2 += walker.spin ** 2
    M2 /= len(Walkers)
    M = calc_magnetization(Walkers)/len(Walkers)

    khi = (M2-M**2)/T
    return khi

def save_observables(Eb, Cv, Mb, khib, filename):
    np.savetxt(filename, (Eb, Cv, Mb, khib))

def prob3():
    Walkers = []

    dim = 2
    grid_side = 10
    grid_size = grid_side ** dim

    # Ising model nearest neighbors only
    mapping = zeros((grid_side, grid_side), dtype=int)  # mapping
    inv_map = []  # inverse mapping
    ii = 0
    for i in range(grid_side):
        for j in range(grid_side):
            mapping[i, j] = ii
            inv_map.append([i, j])
            ii += 1

    # ADD comment
    # Adding walkers to Walkers[] and calculating all nearest neighbours by index.
    for i in range(grid_side):
        for j in range(grid_side):
            j1 = mapping[i, (j - 1) % grid_side]
            j2 = mapping[i, (j + 1) % grid_side]
            i1 = mapping[(i - 1) % grid_side, j]
            i2 = mapping[(i + 1) % grid_side, j]
            Walkers.append(Walker(spin=0.5,
                                  nn=[j1, j2, i1, i2],
                                  dim=dim,
                                  coords=[i, j]))

    Nblocks = 200
    Niters = 1000
    eq = 20  # equilibration "time"
    Ts = linspace(0.5, 6.0, 20)
    Es = zeros_like(Ts)
    Cs = zeros_like(Ts)
    Ms = zeros_like(Ts)
    khis = zeros_like(Ts)

    for ind, T in enumerate(Ts):
        print(T)
        beta = 1.0 / T
        """
        Notice: Energy is measured in units of k_B, which is why
                beta = 1/T instead of 1/(k_B T)
        """
        Walkers, Eb, Acc, Cv, Mb, khib = ising(Nblocks, Niters, Walkers, beta)
        Eb = Eb[eq:]
        Cv = Cv[eq:]
        Mb = Mb[eq:]
        khib = khib[eq:]
        Es[ind] = mean(Eb)
        Cs[ind] = mean(Cv)
        Ms[ind] = mean(Mb)
        khis[ind] = mean(khib)
    f = figure()
    ax1 = subplot(221)
    plot(Ts, Es)
    title('Energy')
    ax2 = subplot(222)
    plot(Ts, Cs)
    title('Heat capacity')
    ax3 = subplot(223)
    plot(Ts, Ms)
    title('Magnetization')
    ax4 = subplot(224)
    plot(Ts, khis)
    title('Magnetic susceptibility')
    show()


def main():
    Walkers=[]

    dim = 2
    grid_side = 10
    grid_size = grid_side**dim
    
    # Ising model nearest neighbors only
    mapping = zeros((grid_side,grid_side),dtype=int) # mapping
    inv_map = [] # inverse mapping
    ii = 0
    for i in range(grid_side):
        for j in range(grid_side):
            mapping[i,j]=ii
            inv_map.append([i,j])
            ii += 1
 

    # ADD comment
    # Adding walkers to Walkers[] and calculating all nearest neighbours by index.
    for i in range(grid_side):
        for j in range(grid_side):
            j1=mapping[i,(j-1) % grid_side]
            j2=mapping[i,(j+1) % grid_side]
            i1=mapping[(i-1) % grid_side,j]
            i2=mapping[(i+1) % grid_side,j]
            Walkers.append(Walker(spin=0.5,
                                  nn=[j1,j2,i1,i2],
                                  dim=dim,
                                  coords = [i,j]))
 
    
    Nblocks = 200
    Niters = 1000
    eq = 20 # equilibration "time"
    T = 3.0
    beta = 1.0/T
    """
    Notice: Energy is measured in units of k_B, which is why
            beta = 1/T instead of 1/(k_B T)
    """
    Walkers, Eb, Acc, Cv, Mb, khib = ising(Nblocks,Niters,Walkers,beta)

    f = figure()
    ax1 = subplot(221)
    plot(Eb)
    title('Energy')
    ax2 = subplot(222)
    plot(Cv)
    title('Heat capacity')
    ax3 = subplot(223)
    plot(Mb)
    title('Magnetization')
    ax4 = subplot(224)
    plot(khib)
    title('Magnetic susceptibility')
    show()
    Eb = Eb[eq:]
    Cv = Cv[eq:]
    Mb = Mb[eq:]
    khib = khib[eq:]
    print('Ising total energy: {0:.5f} +/- {1:0.5f}'.format(mean(Eb), std(Eb)/sqrt(len(Eb))))
    print('Variance to energy ratio: {0:.5f}'.format(abs(std(Eb)/mean(Eb))))
    print('Ising total magnetization: {0:.5f} +/- {1:0.5f}'.format(mean(Mb), std(Mb)/sqrt(len(Mb))))
    print('Ising total heat capacity: {0:.5f} +/- {1:0.5f}'.format(mean(Cv), std(Cv) / sqrt(len(Cv))))
    print('Ising total magnetic susceptibility: {0:.5f} +/- {1:0.5f}'.format(mean(khib), std(khib) / sqrt(len(khib))))
    save_observables(Eb, Cv, Mb, khib, 'observables.txt')
    # DEFAULT PARAMETERS
    # TEMPERATURE = 3 Kelvins
    # grid size = 10
    # dimension = 2 D
    # Boundary conditions: Boundaries connected to opposite boundary
    # Exhange constant:

if __name__=="__main__":
    #main()
    prob3()
        
