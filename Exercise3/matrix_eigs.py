import numpy as np

"""
FYS-4096 Computational physics 

1. Add code to function 'largest_eig'
- use the power method to obtain 
  the largest eigenvalue and the 
  corresponding eigenvector of the
  provided matrix

2. Compare the results with scipy's eigs
- this is provided, but you should use
  that to validating your power method
  implementation

"""

from numpy import *
from matplotlib.pyplot import *
import scipy.sparse as sp
import scipy.sparse.linalg as sla
from scipy.integrate import simps


def largest_eigs(A, tol=1e-12):
    size = np.shape(A)
    b = np.transpose(np.matrix(np.random.randn(size[0])))
    AA = A
    for n in range(100000):
        b2 = A @ b
        b = b2 / np.linalg.norm(b2)
        if n % 1000 == 0:
            l = A @ b / b
            if (max(l)-min(l))/max(l) < tol:
                print("Tolerance reached at n=", n)
                break
    eig_value = np.mean(l)
    eig_vector = b
    return eig_value, eig_vector


def main():
    grid = linspace(-5, 5, 100)
    grid_size = grid.shape[0]
    dx = grid[1] - grid[0]
    dx2 = dx * dx

    # make test matrix
    H0 = sp.diags(
        [
            -0.5 / dx2 * np.ones(grid_size - 1),
            1.0 / dx2 * np.ones(grid_size) - 1.0 / (abs(grid) + 1.),
            -0.5 / dx2 * np.ones(grid_size - 1)
        ],
        [-1, 0, 1])

    # use scipy to calculate the largest eigenvalue
    # and corresponding vector
    eigs, evecs = sla.eigsh(H0, k=1, which='LA')

    # use your power method to calculate the same
    l, vec = largest_eigs(H0, tol=1e-6)
    # see how they compare
    print('largest_eig estimate: ', l)
    print('scipy eigsh estimate: ', eigs)

    psi0 = evecs[:, 0]
    norm_const = simps(abs(psi0) ** 2, x=grid)
    psi0 = psi0 / norm_const

    psi0_ = vec[:, 0].ravel()
    psi0_ = np.squeeze(np.asarray(psi0_))
    norm_const = simps(abs(psi0_) ** 2, x=grid)
    psi0_ = psi0_ / norm_const

    plot(grid, abs(psi0) ** 2, label='scipy eig. vector squared')
    plot(grid, abs(psi0_) ** 2, 'r--', label='largest_eig vector squared')
    legend(loc=0)
    show()


if __name__ == "__main__":
   main()
    #A = np.matrix(np.random.rand(10, 10))
   # l, vec = largest_eigs(A)

