from scipy.integrate import simps
import numpy as np

def integrate(gridpoints):
    f = lambda x, y: (x+y)*np.exp(-(x**2+y**2))

    xints = []
    xgrid = np.linspace(0, 2, gridpoints)
    ygrid = np.linspace(-2, 2, gridpoints)
    for y in ygrid:
        fdata = [f(xx, y) for xx in xgrid]
        xints.append(simps(fdata, xgrid))
    return simps(xints, ygrid)


def main():
    print("Integral using 10 grid points", integrate(10))
    print("Integral using 100 grid points", integrate(100))
    print("Integral using 1000 grid points", integrate(1000))


if __name__ == "__main__":
    main()