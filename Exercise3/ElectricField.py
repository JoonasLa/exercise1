"""
FYS-4096 Computational physics

Exercise 3, Problem 4: Electric Field

Author: Joonas Latukka

"""

import numpy as np
from scipy.integrate import simps
import matplotlib.pyplot as plt

def calcFieldInPoint(L, Q, r, resolution=100):
    epsilon = 8.854187e-12
    field = np.array([0., 0])
    dx = L/resolution
    for dl in np.linspace(-L/2, L/2-dx, resolution):
        r0 = np.array([dl+dx/2, 0])
        rdif = r-r0
        dE = 1/(4*np.pi*epsilon)*Q*dx/L/(np.linalg.norm(rdif)**3)*rdif
        field += dE
    return field

def analyticFieldInPoint(L, Q, d):
    epsilon = 8.854187e-12
    fieldX = 1/(4*np.pi*epsilon)*Q/L*(1/d-1/(d+L))
    return fieldX

def calcTotFieldInArea(areaLimits, L=1, Q=1e-9, gridPoints=20, integrationRes=100):
    xgrid = np.linspace(areaLimits[0], areaLimits[1], gridPoints)
    ygrid = np.linspace(areaLimits[2], areaLimits[3], gridPoints)
    X, Y = np.meshgrid(xgrid, ygrid)
    Ex = np.zeros_like(X)
    Ey = np.zeros_like(X)
    for i in range(gridPoints):
        for j in range(gridPoints):
            x = xgrid[i]
            y = ygrid[j]
            r = np.array((x, y))
            field = calcFieldInPoint(L, Q, r, integrationRes)
            Ex[j, i] = field[0]
            Ey[j, i] = field[1]
    fig = plt.figure()
    plt.quiver(X, Y, Ex, Ey)
    plt.title('Electric field')
    plt.show()

def test_calcFieldInPoint():
    d=0.5
    L = 1
    Q = 1e-9
    r = (L/2+d, 0)
    realEx = analyticFieldInPoint(L, Q, d)
    for points in [1, 10, 100, 1000]:
        estField = calcFieldInPoint(L, Q, r, resolution=points)
        estEx = estField[0]
        print("Points:", points, "Relative error", abs(estEx-realEx)/abs(realEx))

def main():
    calcTotFieldInArea([-1, 1, -1, 1])
    #test_calcFieldInPoint()

if __name__ == "__main__":
    main()