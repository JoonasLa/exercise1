import numpy as np
import matplotlib.pyplot as plt

def plot():
    #data3 =[None,-63.39598003,-63.54618273,-63.58314599,None,None,None]

    data3_fixed = [-63.39598003,-63.54618273,-63.58314599]
    d3 = [-0.2+1.2074,-0.1+1.2074,0+1.2074]



    data4a = [-22.51487991,-22.52203164,-22.52257768,-22.52262441]
    k = [100,150,200,250]

    data4b = [-20.54491147,-22.52262441,-22.71851654]
    kb = [1,2,3]

    plt.figure()
    plt.plot(d3,data3_fixed)
    plt.xlabel('d')
    plt.ylabel('Energy')

    plt.figure()
    plt.plot(k,data4a)
    plt.xlabel('ecutwfc')
    plt.ylabel('Energy')

    plt.figure()
    plt.plot(kb,data4b)
    plt.xlabel('k')
    plt.ylabel('Energy')

    plt.show()
    return

def main():
    plot()


if __name__ == "__main__":
    main()
