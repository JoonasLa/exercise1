
import numpy as np
import matplotlib.pyplot as plt

def main():
    ecutwf = np.array([100, 150, 200, 250])
    E = -22 - np.array([0.51487991, 0.52203164, 0.52257768, 0.52262441])
    k = np.array([1,2,3,4])
    E2 = -np.array([20.54485326, 22.52257758, 22.71847567, 22.75252416])

    fig = plt.figure()
    plt.subplot(1,2,1)
    plt.plot(ecutwf, E)
    plt.xlabel('Cut-off number of basis functions')
    plt.ylabel('Energy (Ry)')
    plt.grid(True)
    plt.title('Prob 4 cut-off convergence')

    plt.subplot(1,2,2)
    plt.plot(k, E2)
    plt.xlabel('Value of k-grid in all directions')
    plt.ylabel('Energy (Ry)')
    plt.grid(True)
    plt.title('Prob 4 k-grid convergence')


    plt.show()

if __name__ == '__main__':
    main()