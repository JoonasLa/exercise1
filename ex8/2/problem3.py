
from numpy import *
import matplotlib.pyplot as plt

def main():
    ds = array([1.0074, 1.09, 1.1, 1.1074, 1.15, 1.16, 1.2074, 1.21])
    E = -63 + array([-0.396, -0.53027497, -0.53984, -0.5462, -0.5718473, -0.57551964, -0.583146, -0.58315644])
    plt.figure()
    plt.plot(ds,E)
    plt.ylabel('Energy (Ry)')
    plt.xlabel('Bond length (Å)')
    plt.grid(True)
    plt.show()

if __name__ == '__main__':
    main()