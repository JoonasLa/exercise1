"""
Intentionally unfinished Hartree code for
   N-electron 1D harmonic quantum dot

TODO: Add Hartree-Fock option for exercise 8


- Related to FYS-4096 Computational Physics
- Test case should give:

    Total energy      11.502873299181957
    Kinetic energy    3.622097184934205
    Potential energy  7.880776114247752

    Density integral  3.9999938742737164

- Job description:
  -- Problem 1: add/fill needed functions and details
      --- some are marked with #FILL#
  -- Problem 2: Include input and output as text files
  -- Problem 3: Include input and output as DHF5 file
"""

from numpy import *
from matplotlib.pyplot import *
from scipy.integrate import simps
import scipy.sparse as sp
import scipy.sparse.linalg as sla
from scipy.integrate import simps
import h5py
import os


def hartree_potential(ns, x):
    """
    Hartree potential using 'Riemann' integration
    - assuming linear grid
    So, interaction between electrons by integrating electron density.
    """
    Vhartree = 0.0 * ns
    dx = x[1] - x[0]
    for ix in range(len(x)):
        r = x[ix]
        for ix2 in range(len(x)):
            rp = x[ix2]
            Vhartree[ix] += ns[ix2] * ee_potential(r - rp)
    return Vhartree * dx


def ee_potential(x):
    global ee_coef
    """ 1D electron-electron interaction """
    V_ee = ee_coef[0] / sqrt(x ** 2 + ee_coef[1])
    return V_ee


def ext_potential(x, m=1.0, omega=1.0):
    """ 1D harmonic quantum dot """
    return 0.5 * m * omega ** 2 * x ** 2


def density(orbitals):
    """ Calculates the complete electron density from column vectors of orbital data."""
    ns = zeros((len(orbitals[0]),))
    for i in range(len(orbitals)):
        ns += abs(orbitals[i]) ** 2
    return ns


def initialize_density(x, dx, normalization=1):
    rho = exp(-x ** 2)
    A = sum(rho[:-1]) * dx
    return normalization / A * rho


def check_convergence(Vold, Vnew, threshold):
    difference_ = amax(abs(Vold - Vnew))
    print('  Convergence check:', difference_)
    converged = False
    if difference_ < threshold:
        converged = True
    return converged


def diagonal_energy(T, orbitals, dx):
    """
    Calculate diagonal energy
    (using Rieman sum)
    """
    Tt = sp.csr_matrix(T)
    E_diag = 0.0
    for i in range(len(orbitals)):
        evec = orbitals[i]
        E_diag += dot(evec.conj().T, Tt.dot(evec))
    return E_diag * dx


def offdiag_potential_energy(orbitals, x):
    """
    Calculate off-diagonal energy
    (using Rieman sum)
    """
    U = 0.0
    dx = x[1] - x[0]
    for i in range(len(orbitals) - 1):
        for j in range(i + 1, len(orbitals)):
            for i1 in range(len(x)):
                for j1 in range(len(x)):
                    U += abs(orbitals[i][i1]) ** 2 * abs(orbitals[j][j1]) ** 2 * ee_potential(x[i1] - x[j1])
    return U * dx ** 2


def exchange_potential_energy_HF(orbitals, x, spins):
    """
    Calculate off-diagonal energy
    (using Rieman sum)
    """
    U = 0.0
    dx = x[1] - x[0]
    for i in range(len(orbitals)):
        for j in range(i+1,len(orbitals)):

            if spins[i] == spins[j]:
                for ix in range(len(x)):
                    for jx in range(len(x)):
                        U += conj(orbitals[i][ix])*conj(orbitals[j][jx])*orbitals[i][jx]*orbitals[j][ix]\
                             *ee_potential(x[ix]-x[jx])
    return - U * dx ** 2


def save_data_to_hdf5_file(fname, orbitals, density, N_e, occ, grid, ee_coefs, Etot, Ekin, Epot):
    f = h5py.File(fname+'.hdf5', "w")
    gset = f.create_dataset("grid", data=grid, dtype='f')
    gset.attrs["info"] = '1D grid'

    oset = f.create_dataset("orbitals", shape=(len(grid), N_e), dtype='f')
    oset.attrs["info"] = '1D orbitals as (len(grid),N_electrons)'
    for i in range(len(orbitals)):
        oset[:, i] = orbitals[i]

    dset = f.create_dataset("density", data=density, dtype='f')
    dset.attrs["info"] = 'Electron densities on grid.'

    qset = f.create_dataset("N_e", data=N_e, dtype='i')
    qset.attrs["info"] = 'Number of electrons.'

    wset = f.create_dataset("occ", data=occ, dtype='i')
    wset.attrs["info"] = 'List of energy states occupied by corresponding electrons.'

    eset = f.create_dataset("ee_coefs", data=ee_coefs, dtype='f')
    eset.attrs["info"] = 'Coefficients for electron electron Coulombic interaction.'

    etset = f.create_dataset("Etot", data=Etot, dtype='f')
    etset.attrs["info"] = 'Total energy.'

    ekset = f.create_dataset("Ekin", data=Ekin, dtype='f')
    ekset.attrs["info"] = 'Kinetic energy.'

    epset = f.create_dataset("Epot", data=Epot, dtype='f')
    epset.attrs["info"] = 'Potential energy.'

    f.close()


def load_data_from_hdf5_file(fname):
    f = h5py.File(fname+ '.hdf5', "r")
    grid = array(f["grid"])
    orbs = array(f["orbitals"])
    orbitals = []
    for i in range(len(orbs[0, :])):
        orbitals.append(orbs[:, i])
    density = array(f["density"])
    N_e = array(f["N_e"])
    occ = array(f["occ"])
    ee_coefs = array(f["ee_coefs"])

    f.close()
    return orbitals, density, N_e, occ, grid, ee_coefs


def calculate_SIC(orbitals, x):
    """ Calculates the self interaction correction for all orbital vectors in list of orbitals."""
    V_SIC = []
    for i in range(len(orbitals)):
        V_SIC.append(-hartree_potential(abs(orbitals[i]) ** 2, x))
    return V_SIC


def calculate_Vx(orbitals,x,spins):
    V_x = []
    for i in range(len(orbitals)):
        V_x.append(potential_x(orbitals[i], x, orbitals, spins[i], spins))
    return V_x


def density_HF(orbital, orbitals, spin, spins, ix, ix2):
    """ Calculates the Hartree-Fock electron density used for calculating
    correction potential v^x"""

    n = 0
    for i in range(len(orbitals)):
        if spins[i] == spin:
            n += conj(orbitals[i][ix2])*orbital[ix2]*orbitals[i][ix]/orbital[ix]
    return n


def potential_x(i_orbital, x, orbitals, i_spin, spins):
    """ Hartree-Fock potential. """
    dx = x[1] - x[0]
    V_i = zeros(shape(i_orbital))
    for ix in range(len(x)):
        r = x[ix]
        for ix2 in range(len(x)):
            rp = x[ix2]
            n_i = density_HF(i_orbital, orbitals, i_spin, spins, ix, ix2)
            V_i[ix] += -n_i * ee_potential(r-rp)
    return V_i*dx


def normalize_orbital(single_orb, dx):
    """ Normalize the orbital probability density to one """
    N_const = simps(abs(single_orb) ** 2, dx=dx)
    norm_orb = single_orb / np.sqrt(N_const)
    return norm_orb


def kinetic_hamiltonian(x):
    grid_size = x.shape[0]
    dx = x[1] - x[0]
    dx2 = dx ** 2

    H0 = sp.diags(
        [
            -0.5 / dx2 * np.ones(grid_size - 1),
            1.0 / dx2 * np.ones(grid_size),
            -0.5 / dx2 * np.ones(grid_size - 1)
        ],
        [-1, 0, 1])
    return H0


def main():
    # --- Setting up the system etc. ---
    global ee_coef
    # e-e potential parameters [strenght, smoothness]=[a,b]
    ee_coef = [1.0, 1.0]
    # number of electrons
    N_e = 6
    # 1D occupations each orbital can have max 2, i.e., spin up and spin down
    # e.g., occ = [0,0,1,1] means lowest up and down, next lowest up and down
    #       occ = [0,1,2,3] means all spin up
    occ = [0, 0, 1, 1, 2, 2]
    spins = [0, 1, 0, 1, 0, 1]  # This could be inferred from occ but this is way easier
    # grid
    x = linspace(-4, 4, 120)
    # threshold
    threshold = 1.0e-4
    # mixing value
    mix = 0.2 # alpha
    # maximum number of iterations
    maxiters = 100
    filename = 'HF_ground_S=0'
    HF = True  # True if Hartree-Fock, untrue if only Hartree
    save = False  # True if you want to save to h5df-file
    # --- End setting up the system etc. ---

    # Initialization and calculations start here
    dx = x[1] - x[0]
    T = kinetic_hamiltonian(x)
    Vext = ext_potential(x)

    print('\nCalculating initial state')
    V_corr = []
    # Check if file exists and use it:
    if os.path.isfile(filename+'.hdf5'):
        orbitals, ns, N_e, occ, x, ee_coef = load_data_from_hdf5_file(filename)
        Vhartree = hartree_potential(ns, x)
        V_corr = calculate_SIC(orbitals, x)  # Calculate self interaction correction
    else:
        ns = initialize_density(x, dx, N_e)  # Initial guess of electron density
        Vhartree = hartree_potential(ns, x)
        for i in range(N_e):
            # initialized as zero, since no orbitals yet
            V_corr.append(ns * 0.0)

    print('Density integral        ', sum(ns[:-1]) * dx)
    print(' -- should be close to  ', N_e)
    Veff = sp.diags(Vext + Vhartree, 0)  # Diagonal matrix of effective potential
    H = T + Veff  # Hamiltonian for the case

    # Creating the orbitals and changing them by iterating the potential with the new orbitals:
    for i in range(maxiters):
        print('\n\nIteration #{0}'.format(i))
        orbitals = []

        # Orbitals for 'this' potential:
        for i in range(N_e):
            print('  Calculating orbitals for electron ', i + 1)
            eigs, evecs = sla.eigs(H + sp.diags(V_corr[i], 0), k=N_e, which='SR')  # SR = smallest real part
            eigs = real(eigs)
            evecs = real(evecs)
            print('    eigenvalues', eigs)
            evecs[:, occ[i]] = normalize_orbital(evecs[:, occ[i]],
                                                 dx)  # occ chooses the lowest wanted "excited" orbital
            orbitals.append(evecs[:, occ[i]])

        # Update potential with these new orbitals:
        Veff_old = Veff
        ns = density(orbitals)  # Calculate electron density from our new orbitals
        Vhartree = hartree_potential(ns, x)  # Calculate the potential with the new density
        if HF:
            V_corr = calculate_Vx(orbitals, x, spins)
        else:
            V_corr = calculate_SIC(orbitals, x)  # Calculate self interaction correction
        Veff_new = sp.diags(Vext + Vhartree, 0)  # Add everything to our new potential
        if check_convergence(Veff_old, Veff_new, threshold):
            break
        else:
            """ Mixing the potential, since no convergence was achieved """
            Veff = mix * Veff_old + (1 - mix) * Veff_new
            H = T + Veff

    print('\n\n')
    if HF:
        off = offdiag_potential_energy(orbitals, x) + exchange_potential_energy_HF(orbitals, x, spins)
    else:
        off = offdiag_potential_energy(orbitals, x)
    E_kin = diagonal_energy(T, orbitals, dx)
    E_pot = diagonal_energy(sp.diags(Vext, 0), orbitals, dx) + off
    E_tot = E_kin + E_pot
    print('Total energy     ', E_tot)
    print('Kinetic energy   ', E_kin)
    print('Potential energy ', E_pot)
    print('\nDensity integral ', sum(ns[:-1]) * dx)

    # WRITE OUT density / orbitals / energetics / etc.
    if save:
        save_data_to_hdf5_file(filename, orbitals, ns, N_e, occ, x, ee_coef, E_tot, E_kin, E_pot)


    plot(x, abs(ns))
    xlabel(r'$x$ (a.u.)')
    ylabel(r'$n(x)$ (1/a.u.)')
    title('N-electron density for N={0}'.format(N_e))
    grid(True)
    text(-2, 0,
         "Total energy    {:.4f} \nKinetic energy    {:.4f} \nPotential energy  {:.4f}".format(E_tot, E_kin, E_pot),
         fontsize=12)
    show()


if __name__ == "__main__":
    main()