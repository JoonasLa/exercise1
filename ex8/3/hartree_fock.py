""" Solution to problem 1

Built on hartree_1d.py of the previous exercises

NOT YET DONE
"""

import os
import typing as tp

import h5py
# from matplotlib.pyplot import *
import matplotlib.pyplot as plt
import numba
# from numpy import *
import numpy as np
# from scipy.integrate import simps
import scipy.integrate
import scipy.sparse as sp
import scipy.sparse.linalg as sla

ee_coef = np.array([1.0, 1.0])


# For disabling electron-electron interactions
# ee_coef = np.array([0.0, 1.0])


@numba.njit
def hartree_potential(ns: np.ndarray, x: np.ndarray) -> np.ndarray:
    """ Hartree potential using 'Riemann' integration

    Assuming linear grid
    Using equation 11?
    """
    Vhartree = 0.0 * ns
    dx = x[1] - x[0]
    for ix in range(len(x)):
        r = x[ix]
        for ix2 in range(len(x)):
            rp = x[ix2]
            Vhartree[ix] += ns[ix2] * ee_potential(r - rp)
    return Vhartree * dx


@numba.njit
def ee_potential(x: float) -> float:
    """1D electron-electron interaction

    According to the exercise description.
    The use of global constants
    """
    # global ee_coef

    # if not isinstance(ee_coef, list):
    #     raise RuntimeError

    return ee_coef[0] / np.sqrt(x ** 2 + ee_coef[1])


@numba.njit
def ext_potential(x: np.ndarray, m: float = 1.0, omega: float = 1.0) -> np.ndarray:
    """ 1D harmonic quantum dot """
    return 0.5 * m * omega ** 2 * x ** 2


def density(orbitals: tp.List[np.ndarray]) -> np.ndarray:
    """ Total density of the orbitals

    Using equation 18
    """
    # ns = zeros((len(orbitals[0]),))
    # return ns

    orbitals_arr = np.array(orbitals)
    ret = np.sum(np.abs(orbitals_arr) ** 2, axis=0)

    # if ret.shape != orbitals[0].shape:
    #     raise RuntimeError
    # elif ret.min() < 0:
    #     raise RuntimeError
    # elif ret.max() > len(orbitals):
    #     raise RuntimeError

    return ret


@numba.njit
def initialize_density(x: np.ndarray, dx: float, normalization: float = 1) -> np.ndarray:
    """ Initialize all particles to begin from "probably the left edge" """
    rho = np.exp(-x ** 2)
    A = np.sum(rho[:-1]) * dx
    return normalization / A * rho


def check_convergence(Vold, Vnew, threshold: float) -> bool:
    """ If the potentials have changed less than the threshold, consider the situation as converged """
    difference_ = np.amax(abs(Vold - Vnew))
    print('  Convergence check:', difference_)
    converged = False
    if difference_ < threshold:
        converged = True
    return converged


def diagonal_energy(T, orbitals: tp.List[np.ndarray], dx):
    """
    Calculate diagonal energy
    (using Rieman sum)
    """
    Tt = sp.csr_matrix(T)
    E_diag = 0.0
    for i in range(len(orbitals)):
        evec = orbitals[i]
        E_diag += np.dot(evec.conj().T, Tt.dot(evec))
    return E_diag * dx


def offdiag_potential_energy(orbitals: tp.List[np.ndarray], x: np.ndarray) -> float:
    """
    Calculate off-diagonal energy
    (using Rieman sum)
    """
    U = 0.0
    dx = x[1] - x[0]
    for i in range(len(orbitals) - 1):
        for j in range(i + 1, len(orbitals)):
            for i1 in range(len(x)):
                for j1 in range(len(x)):
                    U += abs(orbitals[i][i1]) ** 2 * abs(orbitals[j][j1]) ** 2 * ee_potential(x[i1] - x[j1])
    return U * dx ** 2


def save_ns_in_ascii(ns: np.ndarray, filename) -> None:
    s = np.shape(ns)

    with open(filename + '.txt', 'w') as f:
        for ix in range(s[0]):
            f.write('{0:12.8f}\n'.format(ns[ix]))

    with open(filename + '_shape.txt', 'w') as f:
        f.write('{0:5}'.format(s[0]))


def save_array_list_in_ascii(array_list: tp.List[np.ndarray], filename) -> None:
    arr = np.array(array_list)
    np.savetxt(filename + ".txt", arr, delimiter=",")


def load_ns_from_ascii(filename) -> np.ndarray:
    with open(filename + '_shape.txt', 'r') as f:
        for line in f:
            s = np.array(line.split(), dtype=int)

    ns = np.zeros((s[0],))
    d = np.loadtxt(filename + '.txt')
    k = 0
    for ix in range(s[0]):
        ns[ix] = d[k]
        k += 1
    return ns


def load_array_list_from_ascii(filename) -> np.array:
    return np.loadtxt(filename + ".txt", delimiter=",")


def save_data_to_hdf5_file(fname, orbitals, density, N_e, grid, ee_coefs, occ, v_ext, v_hartree, v_sic,
                           threshold) -> None:
    with h5py.File(fname, "w") as f:
        f.attrs["electron_count"] = N_e
        f.attrs["threshold"] = threshold

        coefs_set = f.create_dataset("ee_coefs", data=ee_coefs, dtype="f")
        coefs_set.attrs["info"] = "electron-electron interaction coefficients"

        grid_set = f.create_dataset("grid", data=grid, dtype="f")
        grid_set.attrs["info"] = "1D grid"

        density_set = f.create_dataset("density", data=density, dtype="f")
        density_set.attrs["info"] = "density"

        orb_set = f.create_dataset("orbitals", data=np.array(orbitals), dtype="f")
        orb_set.attrs["info"] = "electron wavefunctions"

        occ_set = f.create_dataset("occ", data=np.array(occ, dtype=np.int_), dtype="int")
        occ_set.attrs["info"] = "electron occupations"

        v_ext_set = f.create_dataset("Vext", data=np.array(v_ext), dtype="float")
        v_ext_set.attrs["info"] = "external potential"

        v_hartree_set = f.create_dataset("Vhartree", data=np.array(v_hartree), dtype="float")
        v_hartree_set.attrs["info"] = "Hartree potential"

        v_sic_set = f.create_dataset("VSIC", data=np.array(v_sic), dtype="float")
        v_sic_set.attrs["info"] = "Self-interaction potential"


def calculate_SIC(orbitals: tp.List[np.ndarray], x) -> tp.List[np.ndarray]:
    """ Self interaction correction

    The potential that needs to be subtracted from the total Hartree potential when calculating its effect on a single
    electron.
    """
    V_SIC = []
    for i in range(len(orbitals)):
        V_SIC.append(-hartree_potential(abs(orbitals[i]) ** 2, x))
    return V_SIC


def normalize_orbital(single_orb: np.ndarray, dx: float) -> np.ndarray:
    """ Normalize the orbitals to one """
    total_prob = scipy.integrate.simps(y=np.abs(single_orb) ** 2) * dx

    # The square root is important
    # (Big thanks to Alpi for pointing this out)
    norm_orb = single_orb / np.sqrt(total_prob)

    norm_prob = scipy.integrate.simps(y=np.abs(norm_orb) ** 2) * dx

    if not (0.9 < norm_prob < 1.1):
        raise RuntimeError(norm_prob)

    return norm_orb


def kinetic_hamiltonian(x: np.ndarray):
    """ Create the matrix of the kinetic Hamiltonian operator

    Each row on the matrix corresponds to x_{i-1}^2 - 2x_i^2 + x_{i+1}^2
    """
    grid_size = x.shape[0]
    dx = x[1] - x[0]
    dx2 = dx ** 2

    H0 = sp.diags(
        [
            -0.5 / dx2 * np.ones(grid_size - 1),
            1.0 / dx2 * np.ones(grid_size),
            -0.5 / dx2 * np.ones(grid_size - 1)
        ],
        [-1, 0, 1])
    return H0


def main():
    # Set the file format to load here
    # file_format = "ASCII"
    # file_format = "HDF5"
    file_format = None
    hdf5_filename = "data.hdf5"

    file = None
    if file_format == "HDF5" and os.path.isfile(hdf5_filename):
        file = h5py.File(hdf5_filename, "r")
    # file = None

    # --- Setting up the system etc. ---
    # global ee_coef
    # e-e potential parameters [strength, smoothness]=[a,b]
    # ee_coef = [1.0, 1.0]
    # number of electrons
    if file is not None:
        print("Loading N_e from HDF5")
        N_e = file.attrs["electron_count"]
    else:
        N_e = 4
        # N_e = 6

    # 1D occupations each orbital can have max 2, i.e., spin up and spin down
    # e.g., occ = [0,0,1,1] means lowest up and down, next lowest up and down
    #       occ = [0,1,2,3] means all spin up
    if file is not None:
        print("Loading occupations from HDF5")
        occ = np.array(file["occ"])
        print("Loaded occ:", occ)
    else:
        occ = np.array([0, 1, 2, 3])

        # S=0
        # occ = np.array([0, 0, 1, 1, 2, 2])

        # S=3
        # occ = np.array([0, 1, 2, 3, 4, 5])

    if occ.size != N_e:
        raise ValueError

    # grid
    if file is not None:
        print("Loading existing grid from HDF5")
        x = np.array(file["grid"])
    elif file_format == "ASCII" and os.path.isfile("grid.txt"):
        print("Loading existing grid from ASCII")
        x = load_ns_from_ascii("grid")
    else:
        x = np.linspace(-4, 4, 120)

    # threshold
    threshold = 1.0e-8
    # mixing value
    mix = 0.2
    # maximum number of iterations
    maxiters = 100

    # --- End setting up the system etc. ---

    # Initialization and calculations start here

    dx: float = x[1] - x[0]
    print("dx: ", dx)
    T = kinetic_hamiltonian(x)
    Vext = ext_potential(x)

    # READ in density / orbitals / etc.
    if file is not None:
        print("Loading existing density from HDF5")
        ns = np.array(file["density"])
    elif file_format == "ASCII" and os.path.isfile('density.txt'):
        print("Loading existing density from ASCII")
        ns = load_ns_from_ascii('density')
    else:
        ns = initialize_density(x, dx, N_e)

    if file is not None:
        file.close()

    print('Density integral        ', sum(ns[:-1]) * dx)
    print(' -- should be close to  ', N_e)

    print('\nCalculating initial state')
    Vhartree = hartree_potential(ns, x)

    if file is not None:
        VSIC = np.array(file["VSIC"])
    elif file_format == "ASCII" and os.path.isfile("VSIC.txt"):
        print("Loading existing VSIC from ASCII")
        VSIC = load_array_list_from_ascii("VSIC")
    else:
        VSIC = []
        for i in range(N_e):
            # initialized as zero, since no orbitals yet
            # #FILL#-->modify for problems 2 and 3
            VSIC.append(ns * 0.0)

    Veff = sp.diags(Vext + Vhartree, 0)
    H = T + Veff

    for i in range(maxiters):
        print('\n\nIteration #{0}'.format(i))
        orbitals = []

        # Solve the eigenvalue equation (eq. 25) for each electron
        for j in range(N_e):
            print('  Calculating orbitals for electron ', j + 1)
            eigs, evecs = sla.eigs(H + sp.diags(VSIC[j], 0), k=N_e, which='SR')
            eigs = np.real(eigs)
            evecs = np.real(evecs)
            print('    eigenvalues', eigs)
            evecs[:, occ[j]] = normalize_orbital(evecs[:, occ[j]], dx)
            orbitals.append(evecs[:, occ[j]])

        # Calculate the new electron density and potentials
        Veff_old = Veff
        ns = density(orbitals)
        Vhartree = hartree_potential(ns, x)
        VSIC = calculate_SIC(orbitals, x)
        Veff_new = sp.diags(Vext + Vhartree, 0)

        # Break if converged, otherwise mix old and new solutions and repeat
        if check_convergence(Veff_old, Veff_new, threshold):
            break
        else:
            """ Mixing the potential """
            Veff = mix * Veff_old + (1 - mix) * Veff_new

            H = T + Veff

    print('\n\n')
    off = offdiag_potential_energy(orbitals, x)
    E_kin = diagonal_energy(T, orbitals, dx)
    E_pot = diagonal_energy(sp.diags(Vext, 0), orbitals, dx) + off
    E_tot = E_kin + E_pot
    print('Total energy     ', E_tot)
    print('Kinetic energy   ', E_kin)
    print('Potential energy ', E_pot)
    print('\nDensity integral ', sum(ns[:-1]) * dx)

    # WRITE OUT density / orbitals / energetics / etc.
    # save_ns_in_ascii(ns,'density')
    save_ns_in_ascii(ns, "density")
    save_ns_in_ascii(x, "grid")
    save_array_list_in_ascii(VSIC, "VSIC")
    save_array_list_in_ascii(orbitals, "orbitals")

    save_data_to_hdf5_file(
        fname=hdf5_filename,
        orbitals=orbitals,
        density=ns,
        N_e=N_e,
        occ=occ,
        grid=x,
        ee_coefs=ee_coef,
        v_ext=Vext,
        v_hartree=Vhartree,
        v_sic=VSIC,
        threshold=threshold
    )

    # Plotting

    plt.figure()
    plt.plot(x, np.abs(ns))
    plt.xlabel(r'$x$ (a.u.)')
    plt.ylabel(r'$n(x)$ (1/a.u.)')
    plt.title('N-electron density for N={0}'.format(N_e))

    plt.figure()
    for i, orbital in enumerate(orbitals):
        plt.plot(x, np.abs(orbital) ** 2, label=str(i + 1))
    plt.xlabel(r'$x$ (a.u.)')
    plt.ylabel(r'$n(x)$ (1/a.u.)')
    plt.title("Electron density for each orbital")
    plt.legend()

    plt.figure()
    plt.plot(x, Vext, label="$V_{ext}$")
    plt.plot(x, Vhartree, label="$V_{hartree}$")
    # plt.plot(x, Veff, label="$V_{eff}$")
    plt.xlabel(r'$x$ (a.u.)')
    plt.ylabel(r'$V$ (a.u.)')
    plt.legend()

    plt.show()


if __name__ == "__main__":
    main()
