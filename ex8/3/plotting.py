import matplotlib.pyplot as plt
# import numpy as np


def print_d():
    d_eq = 1.2074  # nuclei separation in Angstrom
    d_sep = 0.1
    point_count = 7
    d_min = d_eq - (point_count - 1) // 2 * d_sep
    d_vals = []

    for i in range(point_count):
        d = d_min + i * d_sep
        d_vals.append(d)

    print(d_vals)


def prob3():
    d = [0.9074, 1.0074, 1.1074, 1.2074, 1.3074, 1.4074, 1.5074]
    # The results did not converge for higher d with the given maximum iterations.
    # I discussed this with Ilkka and it's ok.
    energy = [None, -63.39598003, -63.54618273, -63.58314599, None, None, None]

    plt.plot(d, energy)
    plt.xlabel("d")
    plt.ylabel("E")
    plt.show()


def prob4a():
    # OLD
    # cutoffs = [100, 150, 200, 250]
    # energy = [-20.53343377, -20.54419787, -20.54485326, -20.54491147]

    cutoffs = [50, 100, 150, 200, 250, 300]
    energy = [
        -22.40864005, -22.51487991, -22.52203164,
        -22.52257768, -22.52262441, -22.52262803
    ]

    plt.plot(cutoffs, energy)
    plt.title("Problem 4a")
    plt.xlabel("cutoff")
    plt.ylabel("E")
    plt.show()


def prob4b():
    # OLD
    # k = [1, 2, 3]
    # energy = [-20.54485326, -22.52257768, -22.71847567]

    k = [1, 2, 3, 4, 5]
    energy = [
        -20.54485326, -22.52257768, -22.71847567,
        -22.75252416, -22.76016739
    ]

    plt.plot(k, energy)
    plt.title("Problem 4b")
    plt.xlabel("k")
    plt.ylabel("E")
    plt.show()


if __name__ == "__main__":
    # print_d()
    prob3()
    prob4a()
    prob4b()
