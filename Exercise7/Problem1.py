"""
FYS-4096 Computational Physics
Author: Joonas Latukka
7th Excercise
"""

import numpy as np
import matplotlib.pyplot as plt

def solve_hartree():
    a = 1
    b = 1
    V1D = lambda x: a/np.sqrt(x**2 + b)
    Ne = 4


def main():
    solve_hartree()

if __name__ == "__main__":
    main()