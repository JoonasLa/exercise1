#! /usr/bin/env python3

"""
Intentionally unfinished Hartree code for 
   N-electron 1D harmonic quantum dot


- Related to FYS-4096 Computational Physics 
- Test case should give:
  
    Total energy      11.502873299181957
    Kinetic energy    3.622097184934205
    Potential energy  7.880776114247752

    Density integral  3.9999938742737164

- Job description: 
  -- Problem 1: add/fill needed functions and details
      --- some are marked with #FILL#
  -- Problem 2: Include input and output as text files
  -- Problem 3: Include input and output as DHF5 file 
"""



from numpy import *
from matplotlib.pyplot import *
import scipy.sparse as sp
import scipy.sparse.linalg as sla
from scipy.integrate import simps
import h5py
import os

def hartree_potential(ns,x):
    """ 
    Hartree potential using 'Rieman' integration 
    - assuming linear grid
    """
    Vhartree=0.0*ns
    dx=x[1]-x[0]
    for ix in range(len(x)):
        r = x[ix]
        for ix2 in range(len(x)):
            rp = x[ix2]
            Vhartree[ix]+=ns[ix2]*ee_potential(r-rp)
    return Vhartree*dx

def ee_potential(x):
    global ee_coef
    """ 1D electron-electron interaction """
    return ee_coef[0]/np.sqrt(x**2+ee_coef[1])

def ext_potential(x,m=1.0,omega=1.0):
    """ 1D harmonic quantum dot """
    return 0.5*m*omega**2*x**2

def density(orbitals):
    ns=zeros((len(orbitals[0]),))
    # Calculate total density by summing squares of orbital values in each point
    for i in range(len(ns)):
        totsum = 0
        for j in range(len(orbitals)):
            totsum += np.abs(orbitals[j][i])**2
        ns[i] = totsum
    return ns
    
def initialize_density(x,dx,normalization=1):
    rho=exp(-x**2)
    A=sum(rho[:-1])*dx
    return normalization/A*rho

def check_convergence(Vold,Vnew,threshold):
    difference_ = amax(abs(Vold-Vnew))
    print('  Convergence check:', difference_)
    converged=False
    if difference_ <threshold:
        converged=True
    return converged

def diagonal_energy(T,orbitals,dx):
    """ 
    Calculate diagonal energy
    (using Rieman sum)
    """
    Tt=sp.csr_matrix(T)
    E_diag=0.0
    for i in range(len(orbitals)):
        evec=orbitals[i]
        E_diag+=dot(evec.conj().T,Tt.dot(evec))
    return E_diag*dx

def offdiag_potential_energy(orbitals,x):
    """ 
    Calculate off-diagonal energy
    (using Rieman sum)
    """
    U = 0.0
    dx=x[1]-x[0]
    for i in range(len(orbitals)-1):
        for j in range(i+1,len(orbitals)):
            for i1 in range(len(x)):
                for j1 in range(len(x)):
                    U+=abs(orbitals[i][i1])**2*abs(orbitals[j][j1])**2*ee_potential(x[i1]-x[j1])
    return U*dx**2

def save_all_in_ascii(ns, orbitals, E_kin, E_pot, treshold, filename):
    s1=shape(ns)
    f=open(filename+'.txt','w')
    # Write density to first column and orbitals to next columns
    for ix in range(s1[0]):
        f.write('{0:12.8f}'.format(ns[ix]))
        for j in range(len(orbitals)):
            f.write('\t{0:12.8f}'.format(orbitals[j][ix]))
        f.write('\n')
    f.close()

    f = open(filename+'_energies.txt', 'w')
    f.write('{0:12.8f}\n'.format(E_kin))
    f.write('{0:12.8f}\n'.format(E_pot))
    f.write(str(treshold))
    f.close()

    f=open(filename+'_shape.txt','w')
    f.write('{0:5}\t'.format(s1[0]))
    f.write('{0:5}'.format(len(orbitals)))
    f.close()
    
def load_all_from_ascii(filename):
    f=open(filename+'_shape.txt','r')
    for line in f:
        s=array(line.split(),dtype=int)
    f.close()
    ns=zeros((s[0],))
    d=loadtxt(filename+'.txt')
    orbitals = []
    for n in range(s[1]):
        orbitals.append(np.zeros((s[0], )))

    for ix in range(s[0]):
        ns[ix]=d[ix, 0]
        for j in range(1, len(d[ix])):
            orbitals[j-1][ix] = d[ix, j]
    f = open(filename+'_energies.txt', 'r')
    E_kin = float(f.readline())
    E_pot = float(f.readline())
    f.close()
    return ns, orbitals, E_kin, E_pot

def calculate_SIC(orbitals,x):
    V_SIC = []
    for i in range(len(orbitals)):
        V_SIC.append(-hartree_potential(abs(orbitals[i])**2,x))
    return V_SIC
            
def normalize_orbital(single_orb,dx):
    # Calculate propability function by multiplying orbital with its complex conjugate
    orb2 = conj(single_orb)*single_orb
    # Integral produces C^2, so sqrt must be taken
    C = np.sqrt(simps(orb2, dx=dx))
    # Return initial orbital scaled by solved scaling coefficienst
    return 1/C*single_orb

def kinetic_hamiltonian(x):
    grid_size = x.shape[0]
    dx = x[1] - x[0]
    dx2 = dx**2
    
    H0 = sp.diags(
        [
            -0.5 / dx2 * np.ones(grid_size - 1),
            1.0 / dx2 * np.ones(grid_size),
            -0.5 / dx2 * np.ones(grid_size - 1)
        ],
        [-1, 0, 1])
    return H0


def create_hdf5_file(fname, x, Ne, ns, orbitals, E_kin, E_pot):
    #Open file
    f = h5py.File(fname+".hdf5", "w")
    # Save x-grid to file
    gset = f.create_dataset("grid", data=x, dtype='d')
    gset.attrs["info"] = '1D grid'
    # Save orbitals to dataset
    oset = f.create_dataset("orbitals", shape=(len(x), Ne), dtype='d')
    oset.attrs["info"] = '1D orbitals as (len(grid),N_electrons)'
    for i in range(len(orbitals)):
        oset[:, i] = orbitals[i]
    # Save electrond densities
    nsset = f.create_dataset("ns", data = ns, dtype='d')
    nsset.attrs["info"] = 'Electron densities'
    Ekset = f.create_dataset('E_kin', data=E_kin, dtype='d')
    Epset = f.create_dataset('E_pot', data=E_pot, dtype='d')
    f.close()


def read_hdf5_file(fname):
    f = h5py.File(fname+".hdf5", "r")
    x = array(f["grid"])
    orbs = array(f["orbitals"])
    orbitals = []
    for i in range(len(orbs[0, :])):
        orbitals.append(orbs[:, i])
    ns = np.array(f["ns"])
    E_kin = np.double(f["E_kin"])
    E_pot = np.double(f["E_pot"])
    f.close()
    return x, ns, orbitals, E_kin, E_pot

def main(asciifname=None, hdf5name=None):
    # --- Setting up the system etc. ---
    global ee_coef
    # e-e potential parameters [strenght, smoothness]=[a,b]
    # IN NON-INTERACTING CASE only one iteration is enough
    ee_coef = [1.0, 1.0]
    # number of electrons
    N_e = 6
    # 1D occupations each orbital can have max 2, i.e., spin up and spin down
    # e.g., occ = [0,0,1,1] means lowest up and down, next lowest up and down
    #       occ = [0,1,2,3] means all spin up
    occ = [0, 0, 1, 1, 2, 2]
    #occ = [0, 0, 1, 1, 2, 2]
    # grid
    x=linspace(-4,4,120)
    # threshold
    threshold=1.0e-5
    # mixing value
    mix=0.2
    # maximum number of iterations
    maxiters = 100
    # --- End setting up the system etc. ---




    # Initialization and calculations start here

    dx = x[1]-x[0]
    T = kinetic_hamiltonian(x)
    Vext = ext_potential(x)

    # READ in density / orbitals / etc.
    # If filenames are given, load data from them. Otherwise initialize densities with function.
    if asciifname != None:
        ns, orbitals, E_kin, E_pot =load_all_from_ascii(asciifname)
    elif hdf5name != None:
        x, ns, orbitals, E_kin, E_pot = read_hdf5_file(hdf5name)
    else:
        ns=initialize_density(x,dx,N_e)
        orbitals = None

    print('Density integral        ', sum(ns[:-1])*dx)
    print(' -- should be close to  ', N_e)
    
    print('\nCalculating initial state')
    Vhartree=hartree_potential(ns,x)
    VSIC=[]
    if orbitals == None:
        for i in range(N_e):
            # Initialized with zeros if orbitals don't exist yet
            VSIC.append(ns*0.0)
    else:
        VSIC = calculate_SIC(orbitals, x)
    Veff=sp.diags(Vext+Vhartree,0)
    H=T+Veff
    for i in range(maxiters):
        print('\n\nIteration #{0}'.format(i))
        orbitals=[]
        for i in range(N_e):
            print('  Calculating orbitals for electron ', i+1)
            eigs, evecs = sla.eigs(H+sp.diags(VSIC[i],0), k=N_e, which='SR')
            eigs=real(eigs)
            evecs=real(evecs)
            print('    eigenvalues', eigs)
            evecs[:,occ[i]]=normalize_orbital(evecs[:,occ[i]],dx)
            orbitals.append(evecs[:,occ[i]])
        Veff_old = Veff
        ns=density(orbitals)
        Vhartree=hartree_potential(ns,x)
        VSIC=calculate_SIC(orbitals,x)
        Veff_new=sp.diags(Vext+Vhartree,0)
        if check_convergence(Veff_old,Veff_new,threshold):
            break
        else:
            """ Mixing the potential """
            # Mix potentials according to mix-ratio
            Veff= Veff_old*(mix) + Veff_new*(1-mix)
            H = T+Veff

    print('\n\n')
    off = offdiag_potential_energy(orbitals,x)
    E_kin = diagonal_energy(T,orbitals,dx)
    E_pot = diagonal_energy(sp.diags(Vext,0),orbitals,dx) + off
    E_tot = E_kin + E_pot
    print('Total energy     ', E_tot)
    print('Kinetic energy   ', E_kin)
    print('Potential energy ', E_pot) 
    print('\nDensity integral ', sum(ns[:-1])*dx)

    # WRITE OUT density / orbitals / energetics / etc.
    #save_all_in_ascii(ns, orbitals, E_kin, E_pot, threshold, 'density')
    create_hdf5_file('Hartree', x, N_e, ns, orbitals, E_kin, E_pot)
    
    plot(x,abs(ns))
    xlabel(r'$x$ (a.u.)')
    ylabel(r'$n(x)$ (1/a.u.)')
    title('N-electron density for N={0}'.format(N_e))
    show()

def problem4():
    x, ns1, orbitals1, E_kin1, E_pot1 = read_hdf5_file('S0')
    x, ns2, orbitals2, E_kin2, E_pot2 = read_hdf5_file('S3')
    x, ns3, orbitals3, E_kin3, E_pot3 = read_hdf5_file('S0noninter')
    x, ns4, orbitals4, E_kin4, E_pot4 = read_hdf5_file('S3noninter')
    E_tot1 = E_kin1 + E_pot1
    E_tot2 = E_kin2 + E_pot2
    E_tot3 = E_kin3 + E_pot3
    E_tot4 = E_kin4 + E_pot4
    f = figure()
    plot(x, ns1, 'r-', label='S=0, E_tot=%2.2f' % (E_tot1))
    plot(x, ns2, 'b-', label='S=3, E_tot=%2.2f' % (E_tot2))
    legend()
    show()
    print('Energy differences:\n S0(interacting)-S0(non-interacting)=%2.2f'
          '\n S3(interacting)-S3(non-interacting)=%2.2f' % (E_tot1-E_tot3, E_tot2-E_tot4))

if __name__=="__main__":
    main(asciifname=None, hdf5name=None)
    #problem4()