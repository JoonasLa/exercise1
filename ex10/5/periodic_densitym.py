from numpy import *
from matplotlib.pyplot import *
from mpl_toolkits.mplot3d import Axes3D


def rho_from_expansion(x,L,n,lambda1,tau):
    rho = ones((len(x),))/L
    zk=pi*x/L
    t=tau*lambda1*(2.0*pi/L)**2
    for i in range(1,n):
        rho += 2.0*exp(-t*i**2)*cos(2.0*i*zk)/L

    return rho

def rho_from_gaussian1(x,L,n,lambda1,tau):
    rho = zeros((len(x),))
    x2 = x**2
    rho = exp(-x**2/4/lambda1/tau)
    for i in range(1,n):
        rho += exp(-(x+i*L)**2/4/lambda1/tau)+exp(-(x-i*L)**2/4/lambda1/tau)

    return 1.0/sqrt(4*pi*lambda1*tau)*rho

def rho_from_gaussian2(x,L,n,lambda1,tau):
    rho = ones((len(x),))
    x2 = x**2
    for i in range(1,n):
        rho += exp(-((x+i*L)**2-x2)/4/lambda1/tau)+exp(-((x-i*L)**2-x2)/4/lambda1/tau)

    return 1.0/sqrt(4*pi*lambda1*tau)*exp(-x2/4/lambda1/tau)*rho

def tau_surface(func,x,L,lambda1,tau):
    N = len(tau)
    s = zeros((N,len(tau)))
    for n in range(N):
        s[n,:] = func(x,L,200,lambda1,tau[n])
    X,Y = meshgrid(arange(N),tau)
    fig=figure()
    ax=fig.add_subplot(111,projection='3d')
    ax.plot_surface(X,Y,transpose(s))
    show()
    
def main():
    lambda1 = 0.5
    tau = 0.01
    n = 200
    L = 2.0
    x = linspace(-L/2,L/2,100)
    beta = linspace(0.001,0.1,100)
    #tau_surface(rho_from_gaussian1,x,L,lambda1,beta)
    #exit()

    rho1 = rho_from_expansion(x,L,n,lambda1,tau)
    rho2 = rho_from_gaussian1(x,L,n,lambda1,tau)
    rho3 = rho_from_gaussian2(x,L,n,lambda1,tau)
    print(sum(rho1[:-1]*(x[1]-x[0])))
    print(sum(rho2[:-1]*(x[1]-x[0])))
    print(sum(rho3[:-1]*(x[1]-x[0])))
    plot(x,rho1,label='expansion')
    plot(x,rho2,'--',label='G1')
    plot(x,rho3,'r-.',label='G2')
    show()

if __name__=="__main__":
    main()
