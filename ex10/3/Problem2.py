import numpy as np
from matplotlib.pyplot import *

def eigenfunctions(L,d,n,lambda1,b,x):
    sum1 = 0
    phi = []
    for xi in range(len(x)):

        dx = x[xi]
        for i in range(n):
            kn = 2 * np.pi * i / L
            sum1 += L**(-d/2)*L**(-d/2)*np.exp(-1j*kn*dx)*np.exp(-b*lambda1*kn**2)
            #sum1 += L**(-d)*(np.cos(-kn*dx)+1j*np.sin(-kn*dx))*np.exp(-b*lambda1*kn**2)
        phi.append(sum1)
        sum1 = 0
    return phi
#end eigenfunctions

def summing_gaussian_density(L,d,n,lambda1,b,x,tau):
    phi = []
    N = 1
    for xi in range(len(x)):
        sum1 = 0
        for i in range(n):
            sum1 += (4*np.pi*lambda1*tau)**(-d*N/2)*np.exp(-(x[xi]+i*L)**2/(4*lambda1*tau))
        phi.append(sum1)
    return phi
#end summing_gaussian_density

def main():
    L = 2
    d = 1

    lambda1 = 0.5
    b = 0.1
    x = np.linspace(-L/2,L/2,100)
    n1 = 1
    phi1 = eigenfunctions(L,d,n1,lambda1,b,x)
    n10 = 10
    phi10 = eigenfunctions(L, d, n10, lambda1, b, x)
    n100 = 100
    phi100 = eigenfunctions(L, d, n100, lambda1, b, x)

    n10 = 10
    b = 0.01
    phi10_001 = eigenfunctions(L, d, n10, lambda1, b, x)
    b = 0.1
    phi10_01 = eigenfunctions(L, d, n10, lambda1, b, x)
    b = 1
    phi10_1 = eigenfunctions(L, d, n10, lambda1, b, x)

    tau = 0.1
    n = 1
    gaus_phi1 = summing_gaussian_density(L, d, n, lambda1, b, x,tau)
    n = 10
    gaus_phi10 = summing_gaussian_density(L, d, n, lambda1, b, x,tau)
    n = 100
    gaus_phi100 = summing_gaussian_density(L, d, n, lambda1, b, x,tau)

    n = 10
    tau = 0.01
    gaus_phi10_001 = summing_gaussian_density(L, d, n, lambda1, b, x, tau)
    tau = 0.1
    gaus_phi10_01 = summing_gaussian_density(L, d, n, lambda1, b, x, tau)
    tau = 1
    gaus_phi10_1 = summing_gaussian_density(L, d, n, lambda1, b, x, tau)

    plot(x,np.abs(phi1),label = 'n = 1')
    plot(x,np.abs(phi10),label = 'n = 10')
    plot(x, np.abs(phi100), label='n = 100')
    title('eigenfunctions')
    legend(loc=0)

    figure()
    plot(x,np.abs(phi10_001),label = 'b = 0.01')
    plot(x,np.abs(phi10_01),label = 'b = 0.1')
    plot(x, np.abs(phi10_1), label='b = 1')
    title('eigenfunctions')
    legend(loc=0)

    figure()
    plot(x,np.abs(gaus_phi1),label = 'n = 1')
    plot(x, np.abs(gaus_phi10),label = 'n = 10')
    plot(x, np.abs(gaus_phi100),label = 'n = 100')
    title('Gaussian')
    legend(loc=0)

    figure()
    plot(x, np.abs(gaus_phi10_001), label='tau = 0.01')
    plot(x, np.abs(gaus_phi10_01), label='tau = 0.1')
    plot(x, np.abs(gaus_phi10_1), label='tau = 1')
    title('Gaussian')
    legend(loc=0)

    show()
#end main



if __name__=="__main__":
    main()