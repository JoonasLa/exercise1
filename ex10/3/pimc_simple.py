from numpy import *
from matplotlib.pyplot import *
from scipy.special import erf

class Walker:
    def __init__(self,*args,**kwargs):
        self.Ne = kwargs['Ne'] #electron
        self.Re = kwargs['Re']
        self.spins = kwargs['spins'] #spins
        self.Nn = kwargs['Nn'] #neutrons
        self.Rn = kwargs['Rn'] #
        self.Zn = kwargs['Zn'] #atomic number
        self.tau = kwargs['tau'] #time step
        self.sys_dim = kwargs['dim'] #dimensions

    def w_copy(self):
        return Walker(Ne=self.Ne,
                      Re=self.Re.copy(),
                      spins=self.spins.copy(),
                      Nn=self.Nn,
                      Rn=self.Rn.copy(),
                      Zn=self.Zn,
                      tau=self.tau,
                      dim=self.sys_dim)
    

def kinetic_action(r1,r2,tau,lambda1):
    #kinectic action, lecture 10 equation 15
    return sum((r1-r2)**2)/lambda1/tau/4

def potential_action(Walkers,time_slice1,time_slice2,tau):
    #potential action, lecture 10 equation 16, error term zero
    return 0.5*tau*(potential(Walkers[time_slice1]) \
                    +potential(Walkers[time_slice2]))

def pimc(Nblocks,Niters,Walkers):

    M = len(Walkers) #steps
    Ne = Walkers[0].Ne*1 # Number of electrons
    sys_dim = 1*Walkers[0].sys_dim #dimension of system
    tau = 1.0*Walkers[0].tau # time step
    lambda1 = 0.5 #lambda
    Eb = zeros((Nblocks,)) #energies
    Accept=zeros((Nblocks,)) #accept percentage
    AccCount=zeros((Nblocks,)) #counting accepts
    sigma2 = lambda1*tau
    sigma = sqrt(sigma2) #sigma for sampling

    obs_interval = 5
    for i in range(Nblocks):
        EbCount = 0
        for j in range(Niters):
            #choosing random time_slice
            time_slice0 = int(random.rand()*M)
            time_slice1 = (time_slice0+1)%M
            time_slice2 = (time_slice1+1)%M
            ptcl_index = int(random.rand()*Ne)

            #computes position of time_slices
            r0 = Walkers[time_slice0].Re[ptcl_index]
            r1 = 1.0*Walkers[time_slice1].Re[ptcl_index]
            r2 = Walkers[time_slice2].Re[ptcl_index]

            #computes kineticaction and potentialaction for three time_slices and position of time_slices
            KineticActionOld = kinetic_action(r0,r1,tau,lambda1) +\
                kinetic_action(r1,r2,tau,lambda1)
            PotentialActionOld = potential_action(Walkers,time_slice0,time_slice1,tau)+potential_action(Walkers,time_slice1,time_slice2,tau)

            # bisection sampling
            #r02_ave,log_S_Rp_R,Rp,log_S_R_Rp = bisection_sampling(r0,r1,r2,sigma,sigma2,sys_dim)
            r02_ave, log_S_Rp_R, Rp, log_S_R_Rp = uniform_sampling(r0, r1, r2, sigma, sigma2, sys_dim)

            #computes new kinectic action and potential action for sampled position
            Walkers[time_slice1].Re[ptcl_index] = 1.0*Rp
            KineticActionNew = kinetic_action(r0,Rp,tau,lambda1) +\
                kinetic_action(Rp,r2,tau,lambda1)
            PotentialActionNew = potential_action(Walkers,time_slice0,time_slice1,tau)+potential_action(Walkers,time_slice1,time_slice2,tau)

            deltaK = KineticActionNew-KineticActionOld
            deltaU = PotentialActionNew-PotentialActionOld
            #print('delta K', deltaK)
            #print('delta logS', log_S_R_Rp-log_S_Rp_R)
            #print('exp(dS-dK)', exp(log_S_Rp_R-log_S_R_Rp-deltaK))
            #print('deltaU', deltaU)
            q_R_Rp = exp(log_S_Rp_R-log_S_R_Rp-deltaK-deltaU) #q, lecture 10 equation 20
            A_RtoRp = min(1.0,q_R_Rp)

            #choosing next step
            if (A_RtoRp > random.rand()):
                Accept[i] += 1.0
            else:
                #if fails, next step isn't accepted
                Walkers[time_slice1].Re[ptcl_index]=1.0*r1
            AccCount[i] += 1
            if j % obs_interval == 0:
                E_kin, E_pot = Energy(Walkers)
                #print(E_kin,E_pot)
                Eb[i] += E_kin + E_pot
                EbCount += 1
            #exit()
            
        Eb[i] /= EbCount
        Accept[i] /= AccCount[i]
        print('Block {0}/{1}'.format(i+1,Nblocks))
        print('    E   = {0:.5f}'.format(Eb[i]))
        print('    Acc = {0:.5f}'.format(Accept[i]))


    return Walkers, Eb, Accept

def bisection_sampling(r0,r1,r2,sigma,sigma2,sys_dim):
    # bisection sampling
    r02_ave = (r0 + r2) / 2  # computes average of two positions
    log_S_Rp_R = -sum((r1 - r02_ave) ** 2) / 2 / sigma2  # old circle

    Rp = r02_ave + random.randn(sys_dim) * sigma  # sampling


    log_S_R_Rp = -sum((Rp - r02_ave) ** 2) / 2 / sigma2  # circle of sampled bisection

    return r02_ave,log_S_Rp_R,Rp,log_S_R_Rp

def uniform_sampling(r0,r1,r2,sigma,sigma2,sys_dim):
    # bisection sampling
    r02_ave = (r0 + r2) / 2  # computes average of two positions
    log_S_Rp_R = 1

    #Rp = r02_ave + random.randn(sys_dim) * sigma  # sampling

    Rp = r1 + random.uniform(-1,1,sys_dim)* sigma

    log_S_R_Rp = 1

    return r02_ave,log_S_Rp_R,Rp,log_S_R_Rp
#end sampling

def Energy(Walkers):
    M = len(Walkers)
    d = 1.0*Walkers[0].sys_dim
    tau = Walkers[0].tau
    lambda1 = 0.5
    U = 0.0
    K = 0.0
    for i in range(M):
        U += potential(Walkers[i])
        for j in range(Walkers[i].Ne):
            if (i<M-1):
                K += d/2/tau-sum((Walkers[i].Re[j]-Walkers[i+1].Re[j])**2)/4/lambda1/tau**2
            else:
                K += d/2/tau-sum((Walkers[i].Re[j]-Walkers[0].Re[j])**2)/4/lambda1/tau**2    
    return K/M,U/M
        
    

def potential(Walker):
    V = 0.0
    r_cut = 1.0e-12
    # e-e
    for i in range(Walker.Ne-1):
        for j in range(i+1,Walker.Ne):
            r = sqrt(sum((Walker.Re[i]-Walker.Re[j])**2))
            V += 1.0/max(r_cut,r)




    Vext = external_potential(Walker)
    
    return V+Vext

def external_potential(Walker):

    sigma = 0.1

    V = 0.0
    r_cut = 1.0e-12

    #2D quantum dot, comment out if not using this
    for i in range(Walker.Ne):
        V += 0.5*sum(Walker.Re[i]**2)


    #3D H2
    """
    # Ion-Ion
    for i in range(Walker.Nn - 1):
        for j in range(i + 1, Walker.Nn):
            r = sqrt(sum((Walker.Rn[i] - Walker.Rn[j]) ** 2))
            V += 1.0 / max(r_cut, r)

    # e-Ion, using given pot of problem 1
    for i in range(Walker.Ne):
        for j in range(Walker.Nn):
            r = sqrt(sum((Walker.Re[i] - Walker.Rn[j]) ** 2))
            V -= Walker.Zn[j] / max(r_cut, r)*erf(max(r_cut, r)/sqrt(2*sigma))
    """
    return V

def main():
    Walkers=[]

    """
    # For H2
    Walkers.append(Walker(Ne=2,
                          Re=[array([0.5,0,0]),array([-0.5,0,0])],
                          spins=[0,1],
                          Nn=2,
                          Rn=[array([-0.7,0,0]),array([0.7,0,0])],
                          Zn=[1.0,1.0],
                          tau = 0.1,
                          dim=3))
    """

    #"""
    # For 2D quantum dot
    Walkers.append(Walker(Ne=2,
                          Re=[array([0.5,0]),array([-0.5,0])],
                          spins=[0,1],
                          Nn=2, # not used
                          Rn=[array([-0.7,0]),array([0.7,0])], # not used
                          Zn=[1.0,1.0], # not used
                          tau = 0.30, #previous 0.25
                          dim=2))
                          #"""
    
    #M=100 #with tau = 0.25
    M = 84 #with tau = 0.3
    for i in range(M-1):
         Walkers.append(Walkers[i].w_copy())
    Nblocks = 200
    Niters = 100
    
    Walkers, Eb, Acc = pimc(Nblocks,Niters,Walkers)

    plot(Eb)
    Eb = Eb[20:]
    print('PIMC total energy: {0:.5f} +/- {1:0.5f}'.format(mean(Eb), std(Eb)/sqrt(len(Eb))))
    print('Variance to energy ratio: {0:.5f}'.format(abs(std(Eb)/mean(Eb)))) 
    show()

if __name__=="__main__":
    main()

#system is 2D guantum dot where is two electrons, spin up and spin down
#tau = 0.25, M = 100. tau = b/M = 1/(kTM)
# T = 1/(kM tau) = 12631 K
# T = 12631*3.175 = 40105 K

#problem1_H2:
#PIMC total energy: -1.01214 +/- 0.11113
#Variance to energy ratio: 1.47311

#problem1_2D_quantum_dot
#PIMC total energy: 3.01992 +/- 0.03138
#Variance to energy ratio: 0.13939

#problem1_d uniform sampling
#PIMC total energy: 2.89048 +/- 0.03645
#Variance to energy ratio: 0.16917