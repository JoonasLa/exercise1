from numpy import*
from matplotlib.pyplot import *

def openfile_0():
    grid = []
    dens = []
    f = open('ag_density_0.dat', 'r')
    for line in f:

        g = line.split()
        if (size(g) != 0):
            grid.append(g[1])
            dens.append(g[2])


    f.close()

    e = open('E_tot_0.dat','r')
    E = zeros(100)
    i = 0
    for line in e:

        e2 = line.split()
        if (size(e2) != 0):
            #E.append(e2[0])
            E[i] = e2[0]
            i +=1

    E = array(E)
    e.close()
    return grid,dens,array(E)

def openfile_3():
    grid = []
    dens = []
    f = open('ag_density.dat', 'r')
    for line in f:

        g = line.split()
        if (size(g) != 0):
            grid.append(g[1])
            dens.append(g[2])


    f.close()
    e = open('E_tot.dat','r')
    E = zeros(109)
    i = 0
    for line in e:

        e2 = line.split()
        if (size(e2) != 0):
            #E.append(e2[0])
            E[i] = e2[0]
            i +=1

    E = array(E)
    e.close()

    return grid,dens,E

def main():
    grid,dens,E = openfile_0()
    grid3,dens3,E3 = openfile_3()
    x_grid = linspace(-4.4060346,4.4060346, 100)

    plot(x_grid,dens[9900:10000])
    title('Density S = 0')

    figure()
    plot(x_grid,dens3[9900:10000])
    title('Density S = 3')

    figure()
    l = len(E)
    x = linspace(1,l,l)
    print('Energy(S=0): ',mean(E))
    print('Error: ',std(E)/sqrt(len(E)))
    plot(x,E)
    title('Energy S = 0')

    figure()
    l = len(E3)
    print('Energy(S=3): ',mean(E3))
    print('Error: ',std(E3) / sqrt(len(E3)))
    x = linspace(1,l,l)

    plot(x,E3)
    title('Energy S = 3')

    show()

if __name__=="__main__":
    main()
