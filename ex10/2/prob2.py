"""
Problem 2 in exercises 10. Answers below

I have plotted to absolute value of density matrix by different cutoffs 'n' and
time steps 'β'. See images "prob2_beta=0.1.png" and "prob2_beta=4.0.png"

Gaussian form seems to bee adequate when timestep is small and box length is
high.

Three dimensional cubic box can be implemented similarly. (This code has a
dim=1 as a varible, so it can be changed to three. Plotting though only
works with dim=1)

"""


import numpy as np
import scipy
import matplotlib.pyplot as plt
import typing as tp
import copy

def ρ_infinite_box(r: np.ndarray, rp: np.ndarray,
                    constants: tp.Tuple[float, int, float, int]) -> float:
    """
    Free particle density matrix. Should work with any dimension.

    :param r:           start position
                            np.ndarray[float], size: (dim)
    :param rp:          end position "r-prime"
                            np.ndarray[float], size: (dim)
    :param constants:   constants: λ,
                                   d=dimension,
                                   β,
                                   N=number of particles
                            float, int, float
    :return:
    """
    λ, d, β, N = constants
    return (4 * np.pi * λ * β) ** (-d*N/2) * np.exp(-(r-rp)@(r-rp) / (4*λ * β))


def ρ_perioidic_box(r: tp.Tuple[float], rp: np.ndarray,
                    constants: tp.Tuple[float, int, float, int],
                    mode="eigenstate_sum",
                    n_max = (10,)) -> float:
    """
    Free particle density matrix in perioidic box. Uses energy eigenstate
    approach. Should work with any dimension.

    :param r:           start position
                            Tuple[float], size: (dim)
                            (This will be converted to ndarray. Numpy has a
                            limitation with arrays of numpy arrays.)
    :param rp:          end position "r-prime"
                            np.ndarray[float], size: (dim)
    :param constants:   tuple:     λ,
                                   d=dimension,
                                   β,
                                   N=number of particles
                                   L=size of perioidic box
                            float, int, float
    :param mode:        "eigenstate_sum" or "spatial_sum"
    :param n_max:       Number of eigenstate_sum or neighbouring boxes taken into
                        account.
    :return:
    """
    λ, d, β, N, L = constants
    k = lambda n: 2 * np.pi * n / L
    ψ = lambda r, n: L**(-d/2) * np.exp(-1j * k(n) @ r)
    E = lambda n: λ * k(n) @ k(n)

    ρ = 0.0
    if mode == "eigenstate_sum":
        for nd_idx in np.ndindex(*n_max):
            n = np.array(nd_idx, dtype=np.int_)
            ρ += ψ(r, n) * np.conjugate(ψ(rp, n)) * np.exp(-β * E(n))
    elif mode == "spatial_sum":
        offset = np.array(n_max, dtype=np.int_) // 2
        for nd_idx in np.ndindex(*n_max):
            n = np.array(nd_idx, dtype=np.int_)
            ρ += ρ_infinite_box(r + (n-offset)*L, rp, constants[:-1])
    else:
        raise ValueError("Unknown mode given as parameter.")

    return ρ


def main():
    # To calculate in N dimensions, mofidy 'dim' and 'repeats'
    λ, d, N, L = 0.5, 1, 1, 2.0
    repeats = [(1,), (3,), (15,)]
    assert (len(repeats[0]) == d)

    box_len = 50
    resolution = np.repeat([box_len], d)
    x_range = np.linspace(-L/2, L/2, box_len)
    # Following one-liner creates all r-indices in 1d-array that enumerate
    # through every grid point in N-d box.
    r_range = np.array([x_range[list(r_idx)] for r_idx in np.ndindex(*resolution)])


    ρ_vec = np.vectorize(ρ_perioidic_box, excluded=(1,2,3,4), signature='(n)->()')

    # Modify this to change dimension (plotting is not done right for d != 1)

    for β in [0.1, 4.0]:
        constants = (λ, d, β, N, L)
        fig, axes = plt.subplots(2, 3, figsize=(15, 10), num="beta="+str(β))
        for mode_idx, mode in enumerate(("eigenstate_sum", "spatial_sum")):
            for n_max_idx, n_max in enumerate(repeats):
                ρ_R_Rp = np.zeros((*resolution, *resolution,))
                for rp_idx, rp in enumerate(r_range):
                    ρ = ρ_vec(r_range, rp, constants, mode, n_max)
                    if d == 1:
                        # TODO fix n-dimensional indexing in ρ_R_Rp "nd_idx = np.unravel_index(rp_idx, resolution)"
                        ρ_R_Rp[:,rp_idx] = ρ

                if d == 1:
                    ax = axes[mode_idx][n_max_idx]
                    cnt = ax.contour(np.abs(ρ_R_Rp)**2, colors="white", extent=(*x_range[[0,-1,0,-1]],))
                    plt.clabel(cnt, inline=1, fmt='%1.2f', fontsize=10)
                    ax.imshow(ρ_R_Rp,
                              interpolation='bilinear',
                              origin='lower',
                              extent=(*x_range[[0,-1,0,-1]],)
                              )

                    ax.set_xlabel("$R$")
                    ax.set_ylabel("$R'$")
                    ax.set_title("$n_{{max}}$ = {} ({})".format(n_max[0], mode))
        fig.tight_layout()

    if d == 1:
        plt.show()




if __name__ == "__main__":
    main()


