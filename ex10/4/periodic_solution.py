""" Ilkka's model solution

Copied manually
"""

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


def rho_from_expansion(x, L, n, lambda1, tau):
    rho = np.ones((len(x), )) / L
    zk = np.pi*x/L
    t = tau * lambda1 * (2*np.pi/L)**2
    for i in range(1, n):
        rho += 2*np.exp(-t*i**2)*np.cos(2*i*zk)/L


def rho_from_gaussian1(x, L, n, lambda1, tau):
    rho = np.zeros((len(x),))
    x2 = x**2
    rho = np.exp(-x**2/4/lambda1/tau)
    for i in range(1, n):
        rho += np.exp(-(x+i*L)**2/4/lambda1/tau) + np.exp(-(x-i*L)**2/4/lambda1/tau)

    return 1/np.sqrt(4*np.pi*lambda1*tau)*rho


def rho_from_gaussian2(x, L, n, lambda1, tau):
    rho = np.ones((len(x),))
    x2 = x**2
    for i in range(1, n):
        rho += np.exp(-((x+i*L)**2-x2)/4/lambda1/tau) + np.exp(-((x-i*L)**2-x2)/4/lambda1/tau)

    return 1/np.sqrt(4*np.pi*lambda1*tau) * np.exp(-x2/4/lambda1/tau)*rho


def tau_surface(func, x, L, lambda1, tau):
    N = len(tau)
    s = np.zeros((N, len(tau)))
    for n in range(N):
        s[n, :] = func(x, L, 200, lambda1, tau[n])
    X,Y = np.meshgrid(np.arange(N), tau)
    fig = plt.figure()
    ax = fig.add_subplot(111, projection="3d")
    ax.plot_surface(X, Y, np.transpose(s))


def main():
    lambda1 = 0.5

    # TODO


if __name__ == "__main__":
    main()
