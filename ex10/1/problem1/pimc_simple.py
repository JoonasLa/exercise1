from numpy import *
from matplotlib.pyplot import *
from scipy.special import erf


class Walker:
    """ Same walker class as in previous exercises. """
    def __init__(self,*args,**kwargs):
        self.Ne = kwargs['Ne']
        self.Re = kwargs['Re']
        self.spins = kwargs['spins']
        self.Nn = kwargs['Nn']
        self.Rn = kwargs['Rn']
        self.Zn = kwargs['Zn']
        self.tau = kwargs['tau']
        self.sys_dim = kwargs['dim']

    def w_copy(self):
        return Walker(Ne=self.Ne,
                      Re=self.Re.copy(),
                      spins=self.spins.copy(),
                      Nn=self.Nn,
                      Rn=self.Rn.copy(),
                      Zn=self.Zn,
                      tau=self.tau,
                      dim=self.sys_dim)
    

def kinetic_action(r1,r2,tau,lambda1):
    """ Kinetic part of the action function for the used Gauss
        distributed random action. """
    return sum((r1-r2)**2)/lambda1/tau/4


def potential_action(Walkers,time_slice1,time_slice2,tau):
    """ Potential part of the action function as approximated in the
        lec slides. """
    return 0.5*tau*(potential(Walkers[time_slice1]) \
                    +potential(Walkers[time_slice2]))


def pimc(Nblocks,Niters,Walkers):
    """ Path integral Monte Carlo with self explanatory input variables
        (similar to previous weeks exercise. """
    M = len(Walkers)  # Number of points the imaginary "Path" is divided into
    Ne = Walkers[0].Ne*1
    sys_dim = 1*Walkers[0].sys_dim
    tau = 1.0*Walkers[0].tau  # Imaginary time step size
    lambda1 = 0.5  # Set constant for the kinetic terms throughout
    Eb = zeros((Nblocks,))
    Accept=zeros((Nblocks,))
    AccCount=zeros((Nblocks,))
    sigma2 = lambda1*tau  # Chosen variance for our Gauss distributed random movement
    sigma = sqrt(sigma2)  # Corresponding Standard deviation

    obs_interval = 5
    for i in range(Nblocks):
        EbCount = 0
        for j in range(Niters):
            # Starting iteration:
            # 1st we choose a random time point Ri to change in our range [0,M]
            time_slice0 = int(random.rand()*M)
            time_slice1 = (time_slice0+1)%M  # Modulo by M if time_slice0 = 0 or M
            time_slice2 = (time_slice1+1)%M
            # Also, randomly choose which electron we move:
            ptcl_index = int(random.rand()*Ne)

            # Reference positions and position we are going to move:
            r0 = Walkers[time_slice0].Re[ptcl_index]
            r1 = 1.0*Walkers[time_slice1].Re[ptcl_index]
            r2 = Walkers[time_slice2].Re[ptcl_index]

            # Action before moving:
            KineticActionOld = kinetic_action(r0,r1,tau,lambda1) +\
                kinetic_action(r1,r2,tau,lambda1)
            PotentialActionOld = potential_action(Walkers,time_slice0,time_slice1,tau)+potential_action(Walkers,time_slice1,time_slice2,tau)

            Rp, log_S_Rp_R, log_S_R_Rp = uniform_move(r1,sys_dim,sigma)
            #Rp, log_S_Rp_R, log_S_R_Rp = bisection_gauss_move(r0, r1, r2, sys_dim, sigma, sigma2)

            # Save this new position and calculate the new action function values:
            Walkers[time_slice1].Re[ptcl_index] = 1.0*Rp
            KineticActionNew = kinetic_action(r0,Rp,tau,lambda1) +\
                kinetic_action(Rp,r2,tau,lambda1)
            PotentialActionNew = potential_action(Walkers,time_slice0,time_slice1,tau)+potential_action(Walkers,time_slice1,time_slice2,tau)

            # Calculate the "improvement ratio" and acceptance for the
            # system energy after this iteration using the differnce in action:
            deltaK = KineticActionNew-KineticActionOld
            deltaU = PotentialActionNew-PotentialActionOld
            #print('delta K', deltaK)
            #print('delta logS', log_S_R_Rp-log_S_Rp_R)
            #print('exp(dS-dK)', exp(log_S_Rp_R-log_S_R_Rp-deltaK))
            #print('deltaU', deltaU)
            q_R_Rp = exp(log_S_Rp_R-log_S_R_Rp-deltaK-deltaU)
            A_RtoRp = min(1.0,q_R_Rp)

            # Check acceptance using a uniform random number
            # (if action reduced acceptance probability is bigger):
            if (A_RtoRp > random.rand()):
                Accept[i] += 1.0
            else:  # If not accepted return the electron to previous position:
                Walkers[time_slice1].Re[ptcl_index]=1.0*r1
            AccCount[i] += 1  # Acceptance count for block acceptance ratio

            # Calculate energy every "observation interval"
            if j % obs_interval == 0:
                E_kin, E_pot = Energy(Walkers)
                #print(E_kin,E_pot)
                Eb[i] += E_kin + E_pot
                EbCount += 1
            #exit()

        # Final energies and acceptances:
        Eb[i] /= EbCount
        Accept[i] /= AccCount[i]
        print('Block {0}/{1}'.format(i+1,Nblocks))
        print('    E   = {0:.5f}'.format(Eb[i]))
        print('    Acc = {0:.5f}'.format(Accept[i]))


    return Walkers, Eb, Accept


def Energy(Walkers):
    """ Total energy of electrons by averaging over all the walkers. """
    M = len(Walkers)
    d = 1.0*Walkers[0].sys_dim
    tau = Walkers[0].tau
    lambda1 = 0.5
    U = 0.0
    K = 0.0
    for i in range(M):
        U += potential(Walkers[i])
        for j in range(Walkers[i].Ne):
            # Kinetic energy calculated from the partition function derivative formula
            if (i<M-1):
                K += d/2/tau-sum((Walkers[i].Re[j]-Walkers[i+1].Re[j])**2)/4/lambda1/tau**2
            else:  # To include case R0,RM:
                K += d/2/tau-sum((Walkers[i].Re[j]-Walkers[0].Re[j])**2)/4/lambda1/tau**2    
    return K/M,U/M


def uniform_move(r1, sys_dim, sigma):

    log_S_Rp_R = 0
    Rp = r1 + (random.rand(sys_dim)-1/2)*2*sigma
    log_S_R_Rp = 0

    return Rp, log_S_Rp_R, log_S_R_Rp

def bisection_gauss_move(r0,r1, r2, sys_dim, sigma, sigma2):
    # bisection sampling (as in dissertation)
    r02_ave = (r0 + r2) / 2
    log_S_Rp_R = -sum((r1 - r02_ave) ** 2) / 2 / sigma2  # Gauss sampling probability
    # Gauss distributed random move of r1 in regards to the reference
    # "bisection" of r0 and r2:
    Rp = r02_ave + random.randn(sys_dim) * sigma
    log_S_R_Rp = -sum((Rp - r02_ave) ** 2) / 2 / sigma2  # Gauss sampling probability

    return Rp, log_S_Rp_R, log_S_R_Rp


def potential(Walker):
    """ Total potential energy of molecule external + between electrons. """
    V = 0.0
    r_cut = 1.0e-12
    # e-e
    for i in range(Walker.Ne-1):
        for j in range(i+1,Walker.Ne):
            r = sqrt(sum((Walker.Re[i]-Walker.Re[j])**2))
            V += 1.0/max(r_cut,r)


    Vext = external_potential_QD(Walker)
    #Vext = external_potential_H2(Walker)

    return V+Vext


def external_potential_H2(Walker):
    """ Calculates the electron nucleus interaction energy for
        the system specified in the Walker class. """
    V = 0.0
    r_cut = 1.0e-12
    sigma = 0.1  # Smoothing parameter

    # e-Ion
    for i in range(Walker.Nn-1):
        for j in range(Walker.Ne-1):
            r = sqrt(sum((Walker.Rn[i]-Walker.Re[j])**2))
            V += -Walker.Zn[i]*erf(max(r_cut, r)/sqrt(2*sigma))/max(r_cut, r)

    # Ion-Ion
    for i in range(Walker.Nn-1):
        for j in range(i+1,Walker.Nn):
            r = sqrt(sum((Walker.Rn[i]-Walker.Rn[j])**2))
            V += 1.0 / max(r_cut, r)

    return V



def external_potential_QD(Walker):
    """ Paraboloid external potential of the quantum dot. """
    V = 0.0
    for i in range(Walker.Ne):
        V += 0.5*sum(Walker.Re[i]**2)
        
    return V


def main():
    Walkers=[]

    """
    # For H2
    Walkers.append(Walker(Ne=2,
                          Re=[array([0.5,0,0]),array([-0.5,0,0])],
                          spins=[0,1],
                          Nn=2,
                          Rn=[array([-0.7,0,0]),array([0.7,0,0])],
                          Zn=[1.0,1.0],
                          tau = 0.1,
                          dim=3))

    """
    # For 2D quantum dot
    Walkers.append(Walker(Ne=2,
                          Re=[array([0.5,0]),array([-0.5,0])],
                          spins=[0,1],
                          Nn=2, # not used
                          Rn=[array([-0.7,0]),array([0.7,0])], # not used
                          Zn=[1.0,1.0], # not used
                          tau = 0.25,
                          dim=2))


    M = 100
    for i in range(M - 1):
        Walkers.append(Walkers[i].w_copy())
    Nblocks = 200
    Niters = 100

    Walkers, Eb, Acc = pimc(Nblocks, Niters, Walkers)
    """


    # Time step extrapolation:
    E = []
    i = 0
    steps = [0.05, 0.1, 0.25, 0.3]
    for step in steps:
        # For 2D quantum dot
        Walkers.append(Walker(Ne=2,
                              Re=[array([0.5, 0]), array([-0.5, 0])],
                              spins=[0, 1],
                              Nn=2,  # not used
                              Rn=[array([-0.7, 0]), array([0.7, 0])],  # not used
                              Zn=[1.0, 1.0],  # not used
                              tau=step,
                              dim=2))

        M=100
        for i in range(M-1):
             Walkers.append(Walkers[i].w_copy())
        Nblocks = 200
        Niters = 100

        Walkers, Eb, Acc = pimc(Nblocks,Niters,Walkers)

        E.append(mean(Eb))
        i += 1
        print('PIMC total energy: {0:.5f} +/- {1:0.5f}'.format(mean(Eb), std(Eb) / sqrt(len(Eb))))
        print('Variance to energy ratio: {0:.5f}'.format(abs(std(Eb) / mean(Eb))))
        print('Temperature: {0:.5f} Kelvin'.format(1 / (Walkers[0].tau * M * 3.16681e-6)))

    z = polyfit(steps,E, 1)
    p = poly1d(z)
    xp = linspace(0, 1)
    plot(steps, E, '.', xp, p(xp), '-')
    grid(True)
    xlabel('Time step')
    ylabel('Energy')
    title('')
    print('Extrapolated energy at tau = 0: {0:.5f}'.format(p(0)))


    """
    plot(Eb, color='black')
    Eb = Eb[20:]
    grid(True)
    ylabel('Energy')
    xlabel('Block')
    title('Energy convergence')
    plot(array([1, Nblocks]), array([mean(Eb), mean(Eb)]),
                linestyle='-', color='blue', label='Average energy')
    legend()
    print('PIMC total energy: {0:.5f} +/- {1:0.5f}'.format(mean(Eb), std(Eb)/sqrt(len(Eb))))
    print('Variance to energy ratio: {0:.5f}'.format(abs(std(Eb)/mean(Eb))))
    print('Temperature: {0:.5f} Kelvin'.format(1/(Walkers[0].tau*M*3.16681e-6)))


    show()


if __name__=="__main__":
    main()
        
