from numpy import *



def main():
    E0 = loadtxt("s0_E_tot.dat", unpack=True)
    E3 = loadtxt("s3_E_tot.dat", unpack=True)
    print('S=0 total energy: {0:.5f} +/- {1:0.5f}'.format(mean(E0)
                                        ,std(E0)/sqrt(len(E0))))
    print('S=3 total energy: {0:.5f} +/- {1:0.5f}'.format(mean(E3)
                                        , std(E3) / sqrt(len(E3))))


if __name__ == '__main__':
    main()