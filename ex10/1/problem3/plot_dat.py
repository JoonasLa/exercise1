from matplotlib.pyplot import *
from numpy import *
from mpl_toolkits.mplot3d import Axes3D


def main():
    file = loadtxt("s0_ag_density.dat", unpack=True)
    fig = figure()
    ax = Axes3D(fig)

    ax.plot_trisurf(file[0,:], file[1,:], file[2,:], cmap='inferno')
    title('S=0 case')
    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel(r'$\rho$')
    show()

if __name__ == '__main__':
    main()