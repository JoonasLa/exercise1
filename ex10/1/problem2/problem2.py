from numpy import *
from matplotlib.pyplot import *

def main():
    L = 2
    nmax = 1000
    lam = 0.5
    d = 1
    b = 0.001 #1/(1.38e-23*4)
    x = linspace(-L / 2, L / 2)
    R, Rp = meshgrid(x, x)

    # Gaussian
    rho0 = (4*pi*lam*b)**(-d)*exp(-(R-Rp)**2/(4*lam*b))
    f1 = figure
    ax = subplot(331)
    ax.contourf(R,Rp,rho0)
    title('Gaussian, beta = {}'.format( b))

    # 1
    n = arange(0, nmax)
    k = 2 * pi * n / L
    E = lam * k ** 2

    rho = zeros(R.shape)
    for i in range(len(x)):
        for j in range(len(x)):
            rho[i, j] = sum(L**(-d/2)*2*cos(-b*E*(R[i,j]-Rp[i,j])))

    ax = subplot(332)
    ax.contourf(R, Rp, rho)
    title('Option 1, n_max = {}, beta = {}'.format(nmax, b))

    # 2
    n = arange(-nmax, nmax)

    rho2 = zeros(R.shape)
    for i in range(len(x)):
        for j in range(len(x)):
            rho2[i, j] = sum((4*pi*lam*b)**(-d)*exp(-(R[i,j]+n*L-Rp[i,j])**2/(4*lam*b)))
    ax = subplot(333)
    ax.contourf(R, Rp, rho2)
    title('Option 2, n_max = {}, beta = {}'.format(nmax, b))


    nmax = 10
    b = 0.1

    # Gaussian
    rho0 = (4 * pi * lam * b) ** (-d) * exp(-(R - Rp) ** 2 / (4 * lam * b))
    f1 = figure
    ax = subplot(334)
    ax.contourf(R, Rp, rho0)
    title('Gaussian, beta = {}'.format(b))
    # 1
    n = arange(0, nmax)
    k = 2 * pi * n / L
    E = lam * k ** 2

    rho = zeros(R.shape)
    for i in range(len(x)):
        for j in range(len(x)):
            rho[i, j] = sum(L ** (-d / 2) * 2 * cos(-b * E * (R[i, j] - Rp[i, j])))

    ax = subplot(335)
    ax.contourf(R, Rp, rho)
    title('Option 1, n_max = {}, beta = {}'.format(nmax, b))

    # 2
    n = arange(-nmax, nmax)

    rho2 = zeros(R.shape)
    for i in range(len(x)):
        for j in range(len(x)):
            rho2[i, j] = sum((4 * pi * lam * b) ** (-d) * exp(-(R[i, j] + n * L - Rp[i, j]) ** 2 / (4 * lam * b)))
    ax = subplot(336)
    ax.contourf(R, Rp, rho2)
    title('Option 2, n_max = {}, beta = {}'.format(nmax, b))

    nmax = 100
    b = 1

    # Gaussian
    rho0 = (4 * pi * lam * b) ** (-d) * exp(-(R - Rp) ** 2 / (4 * lam * b))
    f1 = figure
    ax = subplot(337)
    ax.contourf(R, Rp, rho0)
    title('Gaussian, beta = {}'.format(b))

    # 1
    n = arange(0, nmax)
    k = 2 * pi * n / L
    E = lam * k ** 2

    rho = zeros(R.shape)
    for i in range(len(x)):
        for j in range(len(x)):
            rho[i, j] = sum(L ** (-d / 2) * 2 * cos(-b * E * (R[i, j] - Rp[i, j])))

    ax = subplot(338)
    ax.contourf(R, Rp, rho)
    title('Option 1, n_max = {}, beta = {}'.format(nmax, b))

    # 2
    n = arange(-nmax, nmax)

    rho2 = zeros(R.shape)
    for i in range(len(x)):
        for j in range(len(x)):
            rho2[i, j] = sum((4 * pi * lam * b) ** (-d) * exp(-(R[i, j] + n * L - Rp[i, j]) ** 2 / (4 * lam * b)))
    ax = subplot(339)
    ax.contourf(R, Rp, rho2)
    title('Option 2, n_max = {}, beta = {}'.format(nmax, b))

    show()


if __name__ == '__main__':
    main()