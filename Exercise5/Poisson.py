import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt


def Jacobi_update(phi, rho, h, N):
    phi_new = 1.0*phi
    for i in range(1,N-1):
        for j in range(1, N-1):
            phi_new[i, j] = 1.0/4.0*(phi[i+1, j] + phi[i-1, j] + phi[i, j+1] + phi[i, j-1] + h**2*rho[i, j])
    return phi_new

def GaussSeidel_update(phi, rho, h, N):
    phi2 = 1.0*phi
    for i in range(1, N - 1):
        for j in range(1, N - 1):
            phi2[i, j] = 1.0 / 4.0 * (
                        phi2[i + 1, j] + phi2[i - 1, j] + phi2[i, j + 1] + phi2[i, j - 1] + h ** 2 * rho[i, j])
    return phi2

def SOR_update(phi, rho, h, N, omega=1.8):
    phi2 = 1.0 * phi
    for i in range(1, N - 1):
        for j in range(1, N - 1):
            phi2[i, j] = (1-omega)*phi2[i, j] + omega / 4.0 * (
                    phi2[i + 1, j] + phi2[i - 1, j] + phi2[i, j + 1] + phi2[i, j - 1] + h ** 2 * rho[i, j])
    return phi2

def SolvePartialEquation(tolerance=1e-3):
    N = 21
    phi = np.zeros((N, N))
    x = np.linspace(0, 1, N)
    y = np.linspace(0, 1, N)
    rho = 0*np.ones((N, N))
    h = x[1]-x[0]
    phi[-1, :] = 1

    updateEq = SOR_update
    #updateEq = GaussSeidel_update
    #updateEq = Jacobi_update
    loops = 0
    while True:
        loops += 1
        phi2 = updateEq(phi, rho, h, N)
        if np.max(np.abs(phi2-phi)) < tolerance:
            break
        phi = phi2
    print("Times looped", loops)
    return phi, x, y

def main():
    phi, x, y = SolvePartialEquation()
    X, Y = np.meshgrid(x, y)
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot_wireframe(X, Y, phi, rstride=1, cstride=1)
    plt.show()

if __name__ == "__main__":
    main()