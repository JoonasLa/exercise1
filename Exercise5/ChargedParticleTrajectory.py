import numpy as np
import runge_kutta as rk
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt



def F(x, t, E, B):
    # x[0:2] : coordinates
    # x[3:5] : velocities
    q = 1
    x2 = np.zeros(6)
    x2[0:3] = x[3:]
    x2[3:] = q*(E+np.cross(x[3:], B))
    return x2


def main():
    m = 1
    t0 = 0
    x0 = np.array([0.0, 0.0, 0.0, 0.1, 0.1, 0.1])
    t = np.linspace(0, 5, 101)
    dt = t[1]-t[0]
    E = np.array([0.05, 0, 0])
    B = np.array([0, 4, 0])
    sol = []
    x = x0
    for i in range(len(t)):
        sol.append(x)
        x, tp = rk.runge_kutta4(x, t[i], dt, F, args=(E, B))
    sol = np.array(sol)

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot(sol[:, 0], sol[:, 1], sol[:, 2])
    plt.show()

    print("Coordinates at t = 5 s: (%2.2f, %2.2f, %2.2f)\n"
          "velocities at t = 5 s: (%2.2f, %2.2f, %2.2f)"
          % (sol[-1, 0], sol[-1, 1], sol[-1, 2], sol[-1, 3], sol[-1, 4], sol[-1, 5]))
if __name__ == "__main__":
    main()