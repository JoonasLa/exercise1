import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


def SOR_update(phi, rho, h, N, X, Y, omega=1.7):
    phi2 = 1.0 * phi
    for i in range(1, N - 1):
        for j in range(1, N - 1):
            ## Do not update values that are at capacitor plates
            if((X[i, j] < -0.29) & (-0.31 < X[i, j]) & (-0.51 <= Y[i, j]) & (Y[i, j] <= 0.51)) or \
                (X[i, j] < 0.31) & (0.29 < X[i, j]) & (-0.51 <= Y[i, j]) & (Y[i, j] <= 0.51):
                continue
            phi2[i, j] = (1-omega)*phi2[i, j] + omega / 4.0 * (
                    phi2[i + 1, j] + phi2[i - 1, j] + phi2[i, j + 1] + phi2[i, j - 1] + h ** 2 * rho[i, j])
    return phi2

def gradient(phi, h, N):
    Ex = np.zeros((N, N))
    Ey = np.zeros((N, N))
    for i in range(1,N-1):
        for j in range(1, N-1):
            Ex[i, j] = (phi[i, j+1] - phi[i, j-1])/(2*h)
            Ey[i, j] = (phi[i+1, j] - phi[i-1, j])/(2*h)
        Ex[i, 0] = (phi[i, 1] - phi[i, 0]) / h
        Ex[i, -1] = (phi[i, -1] - phi[i, -2]) / h
    for j in range(1, N-1):
        Ey[0, j] = (phi[1, j] - phi[0, j])/h
        Ey[-1, j] = (phi[-1, j] - phi[-2, j])/h
    return -Ex, -Ey

def main():
    N = 21
    x = np.linspace(-1, 1, N)
    y = np.linspace(-1, 1, N)
    phi = np.zeros((N, N))
    X, Y = np.meshgrid(x, y)
    phi[ (X < -0.29) & (-0.31 < X) & (-0.51 <= Y) & (Y <= 0.51)] = -1
    phi[(X < 0.31) & (0.29 < X) & (-0.51 <= Y) & (Y <= 0.51)] = 1

    rho = np.zeros((N, N))
    h = x[1]-x[0]

    tolerance = 1e-8
    phi2 = SOR_update(phi, rho, h, N, X, Y)
    error = np.max(np.abs(phi2-phi))
    loops = 1
    while error > tolerance:
        loops += 1
        phi = phi2
        phi2 = SOR_update(phi, rho, h, N, X, Y)
        error = np.max(np.abs(phi2-phi))
    print("Times looped:", loops)

    Ex, Ey = gradient(phi, h, N)

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot_wireframe(X, Y, phi, rstride=1, cstride=1)
    ax.set_title("Potential in a box")
    fig2 = plt.figure()
    ax2 = fig2.add_subplot(111)
    ax2.quiver(X, Y, Ex, Ey)
    ax2.axis('equal')
    ax2.hold(True)
    ax2.plot([-0.3, -0.3], [-0.5, 0.5], 'b-', linewidth=3)
    ax2.plot((0.3, 0.3), (-0.5, 0.5), 'r-', linewidth=3)
    ax2.set_title("Electric field in a box")

    plt.show()

if __name__ == "__main__":
    main()