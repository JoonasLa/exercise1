T=300K, Morse:
tau=1/(300*M*kb), Hartree units: kb=3.16681e-6


M=1:
PIMC total energy: -0.17141 +/- 0.00001
Variance to energy ratio: 0.00062
Average nuclei distance: 1.40863 +/- 0.00190

M=8:
PIMC total energy: -0.14854 +/- 0.00006
Variance to energy ratio: 0.00549
Average nuclei distance: 1.43111 +/- 0.00399


M=16:
PIMC total energy: -0.12463 +/- 0.00009
Variance to energy ratio: 0.00960
Average nuclei distance: 1.43871 +/- 0.00369
