"""
Simple Monte Carlo for Ising model

Related to course FYS-4096 Computational Physics

Problem 2:
- Add observables: heat capacity, magnetization, magnetic susceptibility
- Follow the assignment from ex11.pdf.

Problem 3:
- Look at the temperature effects and locate the phase transition temperature.
- Follow the assignment from ex11.pdf.

"""


from numpy import *
from matplotlib.pyplot import *
from scipy.special import erf
import h5py

class Walker:
    def __init__(self,*args,**kwargs):
        self.spin = kwargs['spin']
        self.nearest_neighbors = kwargs['nn']
        self.sys_dim = kwargs['dim']
        self.coords = kwargs['coords']

    def w_copy(self):
        return Walker(spin=self.spin.copy(),
                      nn=self.nearest_neighbors.copy(),
                      dim=self.sys_dim,
                      coords=self.coords.copy())
    

def Energy(Walkers):
    E = 0.0
    J = 4.0 # given in units of k_B
    for i in range(len(Walkers)):
        for k in range(len(Walkers[i].nearest_neighbors)):
            j = Walkers[i].nearest_neighbors[k]
            E += -J * Walkers[i].spin * Walkers[j].spin
    return E/2


def site_Energy(Walkers,Walker):
    E = 0.0
    J = 4.0 # given in units of k_B
    for k in range(len(Walker.nearest_neighbors)):
        j = Walker.nearest_neighbors[k]
        E += -J*Walker.spin*Walkers[j].spin
    return E


def ising(Nblocks,Niters,Walkers,beta):
    M = len(Walkers)
    Eb = zeros((Nblocks,))
    Accept=zeros((Nblocks,))
    AccCount=zeros((Nblocks,))
    Ms = zeros((Nblocks,))
    Xis = zeros((Nblocks,))
    Cs = zeros((Nblocks,))

    # Randomly change the spin orientation of a random atom in the lattice
    # then record the energy changes and perform metropolis MC
    obs_interval = 5
    for i in range(Nblocks):
        EbCount = 0
        Eb2 = 0
        Ms2 = 0
        for k in range(len(Walkers)):
            Ms[i] += Walkers[k].spin
            Ms2 += (Walkers[k].spin) ** 2
        for j in range(Niters):
            site = int(random.rand()*M)

            s_old = 1.0*Walkers[site].spin
 
            E_old = site_Energy(Walkers, Walkers[site])

            # Choose randomly from the 2 spin choices:
            s_new = random.choice([-abs(s_old), abs(s_old)])

            Walkers[site].spin = 1.0*s_new

            E_new = site_Energy(Walkers,Walkers[site])

            # Metropolis Monte Carlo:
            q_s = exp(-beta*(E_new-E_old))
            A_s = min(1.0, q_s)
            if (A_s > random.rand()):
                Accept[i] += 1.0
            else:
                Walkers[site].spin = 1.0*s_old
            AccCount[i] += 1.0

            if j % obs_interval == 0:
                E_tot = Energy(Walkers)/M # energy per spin
                Eb[i] += E_tot
                EbCount += 1
                Eb2 += E_tot**2
                Ms[i] += -s_old + Walkers[site].spin
                Ms2 += -s_old**2 + Walkers[site].spin**2


        Eb[i] /= EbCount
        Eb2 /= EbCount
        # Heat capacity:
        Cs[i] = (Eb2-Eb[i]**2)*beta
        # Magnetization:
        Ms[i] /= EbCount
        Ms2 /= EbCount
        # Magnetic susceptibility:
        Xis[i] = (Ms2 - Ms[i]**2)*beta

        Accept[i] /= AccCount[i]
        print('Block {0}/{1}'.format(i+1,Nblocks))
        print('    E   = {0:.5f}'.format(Eb[i]))
        print('    Acc = {0:.5f}'.format(Accept[i]))


    return Walkers, Eb, Accept, Cs, Ms, Xis

def save_h5(fname, C, M, Xi):
    f = h5py.File(fname, "w")
    gset = f.create_dataset("C", data=C, dtype='f')
    gset.attrs["info"] = 'Heat capacities'

    hset = f.create_dataset("M", data=M, dtype='f')
    hset.attrs["info"] = 'Magnetizations'

    iset = f.create_dataset("Xi", data=Xi, dtype='f')
    iset.attrs["info"] = 'Magnetic susceptibilities'

    f.close()


def main():
    Walkers=[]

    dim = 2
    grid_side = 10
    grid_size = grid_side**dim
    
    # Ising model nearest neighbors only
    mapping = zeros((grid_side,grid_side),dtype=int) # mapping
    inv_map = [] # inverse mapping
    ii = 0
    for i in range(grid_side):
        for j in range(grid_side):
            mapping[i,j]=ii
            inv_map.append([i,j])
            ii += 1

    # Create a walker for each atom containing its coordinates, spin value and
    # its nearest neighbor mappings.
    for i in range(grid_side):
        for j in range(grid_side):
            j1=mapping[i,(j-1) % grid_side]
            j2=mapping[i,(j+1) % grid_side]
            i1=mapping[(i-1) % grid_side,j]
            i2=mapping[(i+1) % grid_side,j]
            Walkers.append(Walker(spin=0.5,
                                  nn=[j1,j2,i1,i2],
                                  dim=dim,
                                  coords = [i,j]))
 
    
    Nblocks = 200
    Niters = 1000
    eq = 20 # equilibration "time"
    T = 3.0
    beta = 1.0 / T
    """
    Notice: Energy is measured in units of k_B, which is why
            beta = 1/T instead of 1/(k_B T)
    """
    # Perform classical monte carlo integration to get the ground state spin structure
    # and energy at given values.
    Walkers, Eb, Acc, Cs, Mss, Xis = ising(Nblocks, Niters, Walkers, beta)

    Cs /= T  # Temperature not in ising function

    M = mean(Mss)
    C = mean(Cs)
    Xi = mean(Xis)

    figure
    subplot(221)
    plot(Eb)
    title('Calculated energies')
    xlabel('Block number')

    subplot(222)
    plot(Cs)
    title('Heat capacity')
    xlabel('Block number')

    subplot(223)
    plot(Mss)
    title('Magnetization (Bohr magneton unit)')
    xlabel('Block number')

    subplot(224)
    plot(Xis)
    title('Magnetic susceptibility')
    xlabel('Block number')

    Eb = Eb[eq:]
    print('Ising total energy: {0:.5f} +/- {1:0.5f}'.format(mean(Eb), std(Eb) / sqrt(len(Eb))))
    print('Variance to energy ratio: {0:.5f}'.format(abs(std(Eb) / mean(Eb))))
    print('\nHeat capacity: {0:.5f}'.format(C))
    print('Magnetization: {0:.5f}'.format(M))
    print('Magnetic susceptibility: {0:.5f}'.format(Xi))
    show()

    save_h5('Observables', Cs, Mss, Xis)

if __name__=="__main__":
    main()
        
