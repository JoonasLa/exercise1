"""
Problem 4
Main modifications:
    - potential function to lennard jones
    - removal of quantum dot external potential
    - mass of proton
    - also added simulated annealing to speed up equilibration
    
- The energy values and radiai are weird, but I don't know what's wrong. Also 
the very long equilibration time makes matters a bit harder.
"""

from numpy import *
from matplotlib.pyplot import *
from scipy.special import erf
import copy
from matplotlib.ticker import ScalarFormatter

class Walker:
    def __init__(self,*args,**kwargs):
        for k, v in kwargs.items():
             setattr(self, k, v)
        # self.Ne = kwargs['Ne']
        # self.Re = kwargs['Re']
        # self.spins = kwargs['spins']
        # self.Nn = kwargs['Nn']
        # self.Rn = kwargs['Rn']
        # self.Zn = kwargs['Zn']
        # self.tau = kwargs['tau']
        # self.sys_dim = kwargs['dim']

    def w_copy(self):
        return copy.deepcopy(self)
        # return Walker(Ne=self.Ne,
        #               Re=self.Re.copy(),
        #               spins=self.spins.copy(),
        #               Nn=self.Nn,
        #               Rn=self.Rn.copy(),
        #               Zn=self.Zn,
        #               tau=self.tau,
        #               dim=self.sys_dim)
        #

def kinetic_action(r1,r2,tau,lambda1):
    return sum((r1-r2)**2)/lambda1/tau/4

def potential_action(Walkers,time_slice1,time_slice2,tau):
    return 0.5*tau*(potential(Walkers[time_slice1]) \
                    +potential(Walkers[time_slice2]))

def lennard_jones_potential(r, ϵ, σ):
    return 4 * ϵ * ((σ/r)**12 - (σ/r)**6)

def pimc(Nblocks,Niters,Walkers):
    M = len(Walkers)
    Ne = Walkers[0].Ne*1
    sys_dim = 1*Walkers[0].sys_dim
    tau = 1.0*Walkers[0].tau
    lambda1 = 0.5 / (1842+1)  # Mass of hydrogen: proton + electron
    Eb = zeros((Nblocks,))
    dst = zeros((Nblocks,))
    Accept=zeros((Nblocks,))
    AccCount=zeros((Nblocks,))
    sigma2 = lambda1*tau
    sigma = sqrt(sigma2)

    obs_interval = 5
    for i in range(Nblocks):
        EbCount = 0
        for j in range(Niters):
            time_slice0 = int(random.rand()*M)
            time_slice1 = (time_slice0+1)%M
            time_slice2 = (time_slice1+1)%M
            ptcl_index = int(random.rand()*Ne)

            r0 = Walkers[time_slice0].Re[ptcl_index]
            r1 = 1.0*Walkers[time_slice1].Re[ptcl_index]
            r2 = Walkers[time_slice2].Re[ptcl_index]
 
            KineticActionOld = kinetic_action(r0,r1,tau,lambda1) +\
                kinetic_action(r1,r2,tau,lambda1)
            PotentialActionOld = potential_action(Walkers,time_slice0,time_slice1,tau)+potential_action(Walkers,time_slice1,time_slice2,tau)

            # bisection sampling
            r02_ave = (r0+r2)/2
            log_S_Rp_R = -sum((r1-r02_ave)**2)/2/sigma2             
            Rp = r02_ave + random.randn(sys_dim)*sigma
            log_S_R_Rp = -sum((Rp - r02_ave)**2)/2/sigma2

            Walkers[time_slice1].Re[ptcl_index] = 1.0*Rp
            KineticActionNew = kinetic_action(r0,Rp,tau,lambda1) +\
                kinetic_action(Rp,r2,tau,lambda1)
            PotentialActionNew = potential_action(Walkers,time_slice0,time_slice1,tau)+potential_action(Walkers,time_slice1,time_slice2,tau)

            deltaK = KineticActionNew-KineticActionOld
            deltaU = PotentialActionNew-PotentialActionOld
            #print('delta K', deltaK)
            #print('delta logS', log_S_R_Rp-log_S_Rp_R)
            #print('exp(dS-dK)', exp(log_S_Rp_R-log_S_R_Rp-deltaK))
            #print('deltaU', deltaU)
            q_R_Rp = exp(log_S_Rp_R-log_S_R_Rp-deltaK-deltaU)
            A_RtoRp = min(1.0,q_R_Rp)
            if (A_RtoRp > random.rand()):
                Accept[i] += 1.0
            else:
                Walkers[time_slice1].Re[ptcl_index]=1.0*r1
            AccCount[i] += 1
            if j % obs_interval == 0:
                E_kin, E_pot = Energy(Walkers)
                dst[i] += distance(Walkers)
                #print(E_kin,E_pot)
                Eb[i] += E_kin + E_pot
                EbCount += 1
            #exit()
            
        Eb[i] /= EbCount
        dst[i] /= EbCount
        Accept[i] /= AccCount[i]
        print('Block {0}/{1}'.format(i+1,Nblocks))
        print('    E   = {0:.5f}'.format(Eb[i]))
        print('    Acc = {0:.5f}'.format(Accept[i]))

    return Walkers, Eb, Accept, dst


def Energy(Walkers):
    M = len(Walkers)
    d = 1.0*Walkers[0].sys_dim
    tau = Walkers[0].tau
    lambda1 = 0.5
    U = 0.0
    K = 0.0
    for i in range(M):
        U += potential(Walkers[i])
        for j in range(Walkers[i].Ne):
            if (i<M-1):
                K += d/2/tau-sum((Walkers[i].Re[j]-Walkers[i+1].Re[j])**2)/4/lambda1/tau**2
            else:
                K += d/2/tau-sum((Walkers[i].Re[j]-Walkers[0].Re[j])**2)/4/lambda1/tau**2    
    return K/M,U/M
        
    

def potential(Walker):
    V = 0.0
    r_cut = 1.0e-12
    # e-e
    for i in range(Walker.Ne-1):
        for j in range(i+1,Walker.Ne):
            r = sqrt(sum((Walker.Re[i]-Walker.Re[j])**2))

            ϵ, σ = Walker.lennard_jones_coefficients_ϵ_σ
            V += lennard_jones_potential(r, ϵ, σ)
    
    return V

def distance(Walkers):
    d = 0.0
    for walker in Walkers:
        assert(walker.Ne == 2)
        d += sqrt(sum((walker.Re[0]-walker.Re[1])**2))
    return d / len(Walkers)

def external_potential(Walker):
    V = 0.0
    
    for i in range(Walker.Ne):
        V += 0.5*sum(Walker.Re[i]**2)

    """
    r_cut = 1.0e-12
    # e-Ion
    sigma = 0.05
    for i in range(Walker.Ne):
        for j in range(Walker.Nn):
            r = max(r_cut,sqrt(sum((Walker.Re[i]-Walker.Rn[j])**2)))
            #V -= Walker.Zn[j]/max(r_cut,r)
            V -= Walker.Zn[j]*erf(r/sqrt(2.0*sigma))/r

    # Ion-Ion
    for i in range(Walker.Nn-1):
        for j in range(i+1,Walker.Nn):
            r = sqrt(sum((Walker.Rn[i]-Walker.Rn[j])**2))
            V += Walker.Zn[i]*Walker.Zn[j]/max(r_cut,r)
    """
    
    return V


def calculate_with_all_trotter_numbers(Nblocks, Niters, taus, Ms, walker,
                                       burnout):
    burn_tails = [[] for i in range(len(taus))]
    dst_tails = [[] for i in range(len(taus))]
    Ebs = [None for i in range(len(taus))]
    dsts = [None for i in range(len(taus))]


    # Calculate
    for i, (tau, M) in enumerate(zip(taus, Ms)):
        walkers = []
        walker.tau = tau
        for i_ in range(M):
            walkers.append(copy.deepcopy(walker))

        # Simulated annealing: decrease temperature from high value
        annealing_steps = 30
        for step in range(annealing_steps, 0, -1):
            for w in walkers:
                w.tau = tau * (2**(step*4/annealing_steps))
            walkers, Eb_tail, _, dst_tail = pimc(burnout//annealing_steps,
                                        Niters, walkers)
            burn_tails[i].extend(Eb_tail)
            dst_tails[i].extend(dst_tail)

        walkers, Ebs[i], Acc, dsts[i] = pimc(Nblocks, Niters, walkers)
        Ebs[i] = np.hstack((burn_tails[i], Ebs[i]))
        dsts[i] = np.hstack((dst_tails[i], dsts[i]))
    return Ebs, dsts

def plot_block_convergence(ax1, Ebs, Ms, burnout, titles=("Energy", "E_{tot}")):
    """
    Plot energy by block count for Largest M value.
    """
    pick_idx = -1
    Eb = Ebs[pick_idx][burnout:]
    ax1.plot(np.arange(burnout+1), Ebs[pick_idx][:burnout+1], ls=":", c="C0")
    ax1.plot(np.arange(burnout, burnout + len(Eb)), Eb, c="C0")
    ax1.set_xlabel("blocks")
    ax1.set_ylabel("${}$".format(titles[1]))
    txt = "At $M = {}$ : ${}$ : {:.3f} ± {:0.3f}".format( titles[1],
        Ms[pick_idx], mean(Eb), std(Eb) / sqrt(len(Eb)))
    print(txt)
    ax1.text(0.05, 0.3, txt, transform=ax1.transAxes)
    ax1.text(0.05, 0.7, "Simulated\nannealing part", transform=ax1.transAxes,
             color="r")
    print(
        'Variance to energy ratio: {0:.5f}'.format(abs(std(Eb) / mean(Eb))))
    ax1.set_title(
        "{} convergence by block, $M = {}$".format(titles[0], Ms[pick_idx]))
    ax1.set_ylim((0, 1.5*Eb[0]))


def plot_with_all_trotter_numbers(ax2, Ebs, Ms, burnout,
                                  titles=("Energy", "E_{tot}")):
    """
    Plot energy by time step (tau).
    """
    # Energies (and errorbars) by tau
    energies_by_M = np.array(
        [mean(Ebs[i][burnout:]) for i in range(len(Ms))])
    errors_by_M = np.array(
        [std(Ebs[i][burnout:]) / sqrt(len(Ebs[i][burnout:])) for i in
         range(len(Ms))])

    ax2.errorbar(Ms, energies_by_M, errors_by_M)

    # # Polynomial fit (impractical with logarithmic scale)
    # n = len(Ebs) - 4
    # mat = np.zeros((len(Ms), n))
    # for i in range(n):
    #     mat[:, i] = Ms ** i
    # coeff = linalg.lstsq(mat, energies_by_M)[0]
    #
    # def fit(x, cs):
    #     sum = 0.0
    #     for i, c in enumerate(cs):
    #         sum += c * x **i
    #     return sum
    # x_range = np.linspace(Ms[0], Ms[-1])
    # ax2.plot(x_range, fit(x_range, coeff), ls=":", c="k")

    # extrapolated_energy = fit(1, coeff)
    ax2.text(0.05, 0.25 - 1 * 0.1,
             "At $M = 1$ : ${} = {:.3f} ({:.3f})\\;$Ha"
             .format(titles[1], energies_by_M[0], errors_by_M[0]), transform=ax2.transAxes)

    ax2.set_title("{} by Trotter number $M$".format(titles[0]))
    ax2.set_xlim(1, Ms[-1])
    ax2.set_ylabel("${}$".format(titles[1]))
    ax2.set_xlabel("$M$")
    ax2.set_xscale("log")
    ax2.xaxis.set_major_formatter(ScalarFormatter())


def main():

    """
    # For H2
    Walkers.append(Walker(Ne=2,
                          Re=[array([0.5,0,0]),array([-0.5,0,0])],
                          spins=[0,1],
                          Nn=2,
                          Rn=[array([-0.7,0,0]),array([0.7,0,0])],
                          Zn=[1.0,1.0],
                          tau = 0.5,
                          dim=3))
    """

    # M*tau = 1/(k*T)
    # tau = 1/(T*k*M)

    T = 300.00
    Ms = 2**np.arange(6)  # [1,2,4,8...]
    taus = 1 / (T*Ms)
    ϵ, σ = 8.6, 5.3101

    # For 2D quantum dot
    walker = Walker(Ne=2,
                    Re=[array([0.7,0]),array([-0.7,0])],
                    spins=[0,1],
                    Nn=0, # not used
                    Rn=[], # not used
                    Zn=[], # not used
                    tau = 0.1,
                    sys_dim=2,
                    lennard_jones_coefficients_ϵ_σ=(ϵ, σ))

    Nblocks = 200
    Niters = 1000
    burnout = 800

    fig, ((ax1, ax2), (ax3, ax4)) = subplots(2, 2, figsize=(10, 10))
    Ebs, dsts = calculate_with_all_trotter_numbers(Nblocks, Niters, taus, Ms, walker,
                                             burnout)
    plot_block_convergence(ax1, Ebs, Ms, burnout, titles=("Energy", "E_{tot}"))
    plot_with_all_trotter_numbers(ax2, Ebs, Ms, burnout,
                                  titles=("Energy", "E_{tot}"))

    plot_block_convergence(ax3, dsts, Ms, burnout, titles=("Bond length", "r_{H-H}"))
    plot_with_all_trotter_numbers(ax4, dsts, Ms, burnout,
                                  titles=("Bond length", "r_{H-H}"))

    show()

if __name__=="__main__":
    main()
        
