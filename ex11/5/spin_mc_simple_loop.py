from numpy import *
from matplotlib.pyplot import *
from scipy.special import erf

class Walker:
    def __init__(self,*args,**kwargs):
        self.spin = kwargs['spin']
        self.nearest_neighbors = kwargs['nn']
        self.sys_dim = kwargs['dim']
        self.coords = kwargs['coords']

    def w_copy(self):
        return Walker(spin=self.spin.copy(),
                      nn=self.nearest_neighbors.copy(),
                      dim=self.sys_dim,
                      coords=self.coords.copy())
    

def Energy(Walkers):
    E = 0.0
    J = 4.0
    for i in range(len(Walkers)):
        for k in range(len(Walkers[i].nearest_neighbors)):
            j = Walkers[i].nearest_neighbors[k]
            E += -J*Walkers[i].spin*Walkers[j].spin
    return E/2

def site_Energy(Walkers,Walker):
    E = 0.0
    J = 4.0
    for k in range(len(Walker.nearest_neighbors)):
        j = Walker.nearest_neighbors[k]
        E += -J*Walker.spin*Walkers[j].spin
    return E

def magnetization(Walkers):
    m = 0.0
    for i in range(len(Walkers)):
        m += Walkers[i].spin
    return m

def ising(Nblocks,Niters,Walkers,beta):
    M = len(Walkers)
    Eb = zeros((Nblocks,))
    Eb2 = zeros((Nblocks,))
    mb = zeros((Nblocks,))
    mb2 = zeros((Nblocks,))
    Accept=zeros((Nblocks,))
    AccCount=zeros((Nblocks,))

    obs_interval = 5
    for i in range(Nblocks):
        EbCount = 0
        for j in range(Niters):
            site = int(random.rand()*M)

            s_old = 1.0*Walkers[site].spin
 
            E_old = site_Energy(Walkers,Walkers[site])

            if random.rand()>0.5:
                s_new = 0.5
            else:
                s_new = -0.5
            Walkers[site].spin = 1.0*s_new

            E_new = site_Energy(Walkers,Walkers[site])

            deltaE = E_new-E_old
            q_s_sp = exp(-beta*deltaE)
            A_s_sp = min(1.0,q_s_sp)
            if (A_s_sp > random.rand()):
                Accept[i] += 1.0
            else:
                Walkers[site].spin=1.0*s_old
            AccCount[i] += 1

            if j % obs_interval == 0:
                E_tot = Energy(Walkers)
                Eb[i] += E_tot
                Eb2[i] += E_tot**2
                mag = magnetization(Walkers)
                mb[i] += mag
                mb2[i] += mag**2
                EbCount += 1
            
        Eb[i] /= EbCount
        Eb2[i] /= EbCount
        mb[i] /= EbCount
        mb2[i] /= EbCount
        Accept[i] /= AccCount[i]
        print('Block {0}/{1}'.format(i+1,Nblocks))
        print('    E   = {0:.5f}'.format(Eb[i]))
        print('    Acc = {0:.5f}'.format(Accept[i]))


    return Walkers, Eb, Eb2, mb, mb2, Accept


def main():
    Walkers=[]

    
    dim = 2
    grid_side = 10
    grid_size = grid_side**dim
    
    # Ising model nearest neighbors only
    mapping = zeros((grid_side,grid_side),dtype=int)
    inv_map = []
    ii = 0
    for i in range(grid_side):
        for j in range(grid_side):
            mapping[i,j]=ii
            inv_map.append([i,j])
            ii += 1
 
    for i in range(grid_side):
        for j in range(grid_side):
            j1=mapping[i,(j-1) % grid_side]
            j2=mapping[i,(j+1) % grid_side]
            i1=mapping[(i-1) % grid_side,j]
            i2=mapping[(i+1) % grid_side,j]
            Walkers.append(Walker(spin=0.5,
                                  nn=[j1,j2,i1,i2],
                                  dim=dim,
                                  coords = [i,j]))
 
    
    #Nblocks = [100, 200, 400, 400, 500, 500, 400, 200, 200, 200, 200, 200]
    Nblocks = 200
    Niters = 2000
    #T = array([0.5, 0.75, 1, 2, 2.1, 2.2, 2.3, 2.4, 2.5, 2.6, 3, 4, 6])
    T = linspace(0.5,6,20)
    #T = array([3.0])
    Edata = []
    Edata2 = []
    Mdata = []
    Mdata2 = []
    eq = 20
    for i in range(len(T)):
        beta = 1.0/T[i]
        Walkers, Eb, Eb2, mb, mb2, Acc = ising(Nblocks,Niters,Walkers,beta)
        
        E_and_err = array([mean(Eb[eq:]), std(Eb[eq:])/sqrt(len(Eb[eq:]))])
        E2_and_err = array([mean(Eb2[eq:]), std(Eb2[eq:])/sqrt(len(Eb2[eq:]))])
        Edata.append(E_and_err)
        Edata2.append(E2_and_err)
        M_and_err = array([mean(mb[eq:]), std(mb[eq:])/sqrt(len(mb[eq:]))])
        M2_and_err = array([mean(mb2[eq:]), std(mb2[eq:])/sqrt(len(mb2[eq:]))])
        Mdata.append(M_and_err)
        Mdata2.append(M2_and_err)
    Edata=array(Edata)
    Edata2=array(Edata2)
    Mdata=array(Mdata)
    Mdata2=array(Mdata2)
    # Notice: T_c_exact = 2.27
    figure()
    errorbar(T,Edata[:,0]/grid_size,Edata[:,1]/grid_size)
    ylabel('Energy per spin')
    xlabel('Temperature')
    figure()
    Cv = (Edata2[:,0]-Edata[:,0]**2)/T**2/grid_size
    plot(T,Cv,'-o')
    plot([2.27, 2.27],[0.0, 1.03*amax(Cv)],'k--')
    ylabel('Heat Capacity per spin')
    xlabel('Temperature')
    figure()
    errorbar(T,Mdata[:,0]/grid_size,Mdata[:,1]/grid_size)
    ylabel('Magnetization per spin')
    xlabel('Temperature')
    figure()
    chi = (Mdata2[:,0]-Mdata[:,0]**2)/T**2/grid_size
    plot(T,chi,'-o')
    plot([2.27, 2.27],[0.0, 1.03*amax(chi)],'k--')
    ylabel('Susceptibility per spin')
    xlabel('Temperature')
    #print('Ising total energy: {0:.5f} +/- {1:0.5f}'.format(mean(Eb), std(Eb)/sqrt(len(Eb))))
    #print('Variance to energy ratio: {0:.5f}'.format(abs(std(Eb)/mean(Eb)))) 
    show()

if __name__=="__main__":
    main()
        
