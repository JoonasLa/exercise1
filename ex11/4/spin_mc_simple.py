# Problems 1, 2 and 3
# Jesse Saari
# 229027

"""
Simple Monte Carlo for Ising model

Related to course FYS-4096 Computational Physics

Problem 1:
- Make the code to work, that is, include code to where it reads "# ADD"
- Comment the parts with "# ADD" and make any additional comments you 
  think could be useful for yourself later.
- Follow the assignment from ex11.pdf.

Problem 2:
- Add observables: heat capacity, magnetization, magnetic susceptibility
- Follow the assignment from ex11.pdf.

Problem 3:
- Look at the temperature effects and locate the phase transition temperature.
- Follow the assignment from ex11.pdf.

"""


from numpy import *
from matplotlib.pyplot import *
from scipy.special import erf

class Walker:
    def __init__(self,*args,**kwargs):
        self.spin = kwargs['spin']
        self.nearest_neighbors = kwargs['nn']
        self.sys_dim = kwargs['dim']
        self.coords = kwargs['coords']

    def w_copy(self):
        return Walker(spin=self.spin.copy(),
                      nn=self.nearest_neighbors.copy(),
                      dim=self.sys_dim,
                      coords=self.coords.copy())
    
# Function calculates the total energy of the system (over all the walkers and spins of the system) by using Ising model.
def Energy(Walkers):
    E = 0.0
    # Exchange constant
    J = 4.0 # given in units of k_B
    # Calculation of the total energy of the system (over all the walkers and spins of the system) using Ising model.
    for i in range(len(Walkers)):
        for k in range(len(Walkers[i].nearest_neighbors)):
                   j = Walkers[i].nearest_neighbors[k]
                   E += -J*Walkers[i].spin*Walkers[j].spin
    return E/2

# Function calculates the magnetization of the system (over all the walkers and spins of the system)
def Magnetization(Walkers):
    M = 0.0
    for i in range(len(Walkers)):
            M += Walkers[i].spin
    return M

# Function calculates the energy of the certain site (over all the nearest neighbours) by using Ising model.
def site_Energy(Walkers,Walker):
    E = 0.0
    # Exchange constant
    J = 4.0 # given in units of k_B
    for k in range(len(Walker.nearest_neighbors)):
        j = Walker.nearest_neighbors[k]
        E += -J*Walker.spin*Walkers[j].spin
    return E

# Function calculates the energy and magnetization of the system by using classical Monte Carlo.
def ising(Nblocks,Niters,Walkers,beta):
    M = len(Walkers)
    Eb = zeros((Nblocks,))
    Eb2 = zeros((Nblocks,))
    Mag = zeros((Nblocks,))
    Mag2 = zeros((Nblocks,))
    Accept=zeros((Nblocks,))
    AccCount=zeros((Nblocks,))

    obs_interval = 5
    for i in range(Nblocks):
        EbCount = 0
        for j in range(Niters):
            site = int(random.rand()*M)

            s_old = 1.0*Walkers[site].spin
 
            E_old = site_Energy(Walkers,Walkers[site])

            # Selecting randomly the new spin for the walker (-1/2 or 1/2)
            if random.random() < 0.5:
                s_new = 1.0*s_old
            else:
                s_new = -1.0*s_old
            
            Walkers[site].spin = 1.0*s_new

            E_new = site_Energy(Walkers,Walkers[site])

            # Metropolis Monte Carlo algorithm to sample the spin according to the probability density function.
            q_R_Rp = exp(-beta*(E_new-E_old))
            A_RtoRp = min(1.0,q_R_Rp)
            if (A_RtoRp > random.rand()):
                Accept[i] += 1.0
            else:
                Walkers[site].spin=1.0*s_old
            AccCount[i] += 1
            if j % obs_interval == 0:
                E_tot = Energy(Walkers)/M # energy per spin
                Eb[i] += E_tot
                Eb2[i] += E_tot**2
                Mag[i] += Magnetization(Walkers)
                Mag2[i] += Magnetization(Walkers)**2
                EbCount += 1
            
        Eb[i] /= EbCount
        Eb2[i] /= EbCount
        Mag[i] /= EbCount
        Mag2[i] /= EbCount
        Accept[i] /= AccCount[i]
        print('Block {0}/{1}'.format(i+1,Nblocks))
        print('    E   = {0:.5f}'.format(Eb[i]))
        print('    Acc = {0:.5f}'.format(Accept[i]))


    return Walkers, Eb, Accept, Eb2, Mag, Mag2


def main():

    Walkers=[]

    dim = 2 # Dimension of the system
    grid_side = 10
    grid_size = grid_side**dim # Grid size
    
    # Ising model nearest neighbors only
    mapping = zeros((grid_side,grid_side),dtype=int) # mapping
    inv_map = [] # inverse mapping
    ii = 0
    for i in range(grid_side):
        for j in range(grid_side):
            mapping[i,j]=ii
            inv_map.append([i,j])
            ii += 1

    # Mapping the walkers in the grid and initalizing the spins to be 1/2.
    for i in range(grid_side):
        for j in range(grid_side):
            j1=mapping[i,(j-1) % grid_side]
            j2=mapping[i,(j+1) % grid_side]
            i1=mapping[(i-1) % grid_side,j]
            i2=mapping[(i+1) % grid_side,j]
            Walkers.append(Walker(spin=0.5,
                                  nn=[j1,j2,i1,i2],
                                  dim=dim,
                                  coords = [i,j]))

    # Number of blocks and iterations.
    Nblocks = 200
    Niters = 1000
    eq = 20 # equilibration "time"
    T = 3.0 # Temperature
    beta = 1.0/T # beta value
    """
         Notice: Energy is measured in units of k_B, which is why
            beta = 1/T instead of 1/(k_B T)
    """

    # Calculating the energy and magnetization of the system by using classic Monte Carlo in "ising" function
    Walkers, Eb, Acc, Eb2, Mag, Mag2 = ising(Nblocks,Niters,Walkers,beta)

    # Calculating the magnetic susceptilibity and heat capacity of the system
    MagneticSusceptibility = (Mag2-Mag**2)/T**2
    HeatCapacity = (Eb2-Eb**2)/T

    # Plotting the total energy, magnetization, heat capacity and magnetic susceptibility as a function of blocks.
    plot(Eb)
    title('Ising total energy')
    xlabel('Block number')
    ylabel('Total energy')
    show()
    plot(Mag)
    title('Magnetization')
    xlabel('Block number')
    ylabel('Magnetization')
    show()
    plot(HeatCapacity)
    title('Heat capacity')
    xlabel('Block number')
    ylabel('Heat capacity')
    show()
    plot(MagneticSusceptibility)
    title('Magnetic susceptibility')
    xlabel('Block number')
    ylabel('Magnetic susceptibility')
    show()
    Eb = Eb[eq:]

    # Printing the average of the results.
    print('Ising total energy: {0:.5f} +/- {1:0.5f}'.format(mean(Eb), std(Eb)/sqrt(len(Eb))))
    print('Variance to energy ratio: {0:.5f}'.format(abs(std(Eb)/mean(Eb))))
    print('Magnetization:', mean(Mag))
    print('Heat capacity:', mean(HeatCapacity))
    print('Magnetic susceptibility:', mean(MagneticSusceptibility))
    show()
    
    # the total energy, magnetization, heat capacity and magnetic susceptibility as a function of temperature from 0.5 K to 6.0 K.

    # Number of blocks and iterations.
    Nblocks2 = 50
    Niters2 = 1000
    T2 = linspace(0.5, 6.0, 50) # Temperature
    beta2 = linspace(1/0.5, 1/6.0, 50) # beta value

    Etot = []
    Magn = []
    HeatCap = []
    MagSus = []

    # Calculating the average total energy, magnetization, heat capacity and magnetic susceptibility in different temperature of the system.
    for i in range(len(beta2)):
        Walkers, Eb, Acc, Eb2, Mag, Mag2 = ising(Nblocks2,Niters2,Walkers,beta2[i])
        MagneticSusceptibility = (Mag2-Mag**2)/T**2
        HeatCapacity = (Eb2-Eb**2)/T
        Etot.append(mean(Eb))
        Magn.append(mean(Mag))
        HeatCap.append(mean(HeatCapacity))
        MagSus.append(mean(MagneticSusceptibility))

    Etot = np.array(Etot)
    Magn = np.array(Magn)
    HeatCap = np.array(HeatCap)
    MagSus = np.array(MagSus)

    # Plotting the average total energy, magnetization, heat capacity and magnetic susceptibility as a function of temperature of the system.
    plot(T2, Etot)
    title('Ising total energy as a function of T')
    xlabel('Temperature')
    ylabel('Total energy')
    show()
    plot(T2, Magn)
    title('Magnetization as a function of T')
    xlabel('Temperature')
    ylabel('Magnetization')
    show()
    plot(T2, HeatCap)
    title('Heat capacity as a function of T')
    xlabel('Temperature')
    ylabel('Heat capacity')
    show()
    plot(T2, MagSus)
    title('Magnetic susceptibility as a function of T')
    xlabel('Temperature')
    ylabel('Magnetic susceptibility')
    show()

if __name__=="__main__":
    main()
        
