import read_xsf_example as rxsf
import numpy as np

def main():
    filename = 'dft_chargedensity1.xsf'
    rho, lattice, grid, shift = rxsf.read_example_xsf_density(filename)

    # Volume of one differential block is it lattice vectors is calculated with equation V = (a x b) dot c
    blockV = np.dot(np.cross(lattice[0, :]/grid[0], lattice[1, :]/grid[1]), lattice[2, :]/grid[2])
    # Each point has rho*volume electorns in it, so total electron amount is equal to
    # sum of rhos times volume of single block
    totale = np.sum(rho)*blockV
    print("Total electrons in %s: %4.1f" % (filename, totale))
    print("Reciprocal lattice vectors", lattice[0, :], lattice[1, :], lattice[2, :])

    filename = 'dft_chargedensity2.xsf'
    rho, lattice, grid, shift = rxsf.read_example_xsf_density(filename)
    blockV = np.dot(np.cross(lattice[0, :] / grid[0], lattice[1, :] / grid[1]), lattice[2, :] / grid[2])
    totale = np.sum(rho) * blockV
    print("Total electrons in %s: %4.1f" % (filename, totale))
    print("Reciprocal lattice vectors", lattice[0, :], lattice[1, :], lattice[2, :])


if __name__ == "__main__":
    main()