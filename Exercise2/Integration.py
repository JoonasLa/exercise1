""" Exercise 2 for Computational Physics"""
from scipy import integrate
import numpy as np
from matplotlib import pyplot as plt

def integrate1d():
    f1 = lambda r: r**2*np.exp(-2*r)
    f2 = lambda x: np.sin(x)/x
    f3 = lambda x: np.exp(np.sin(x**3))

    int1 = []
    int2 = []
    int3 = []

    pointlist = range(10, 200, 10)
    for points in pointlist:
        x1 = np.linspace(0, 5, points)
        x2 = np.linspace(0.001, 1, points)
        x3 = np.linspace(0, 5, points)

        y1 = f1(x1)
        y2 = f2(x2)
        y3 = f3(x3)

        int1.append(integrate.simps(y1, x1))
        int2.append(integrate.simps(y2, x2))
        int3.append(integrate.simps(y3, x3))

    fig = plt.figure()
    plt.ticklabel_format(style='plain', axis='y')
    ax1 = plt.subplot(311)
    ax1.plot(pointlist, int1)

    ax2 = plt.subplot(312)
    ax2.plot(pointlist, int2)

    ax3 = plt.subplot(313)
    ax3.plot(pointlist, int3)
    ax3.set_xlabel("Grid points")
    print(int2)
    plt.show()

def integrate2d():
    f = lambda x, y: x*np.exp(-(x**2+y**2))
    pointlist = range(10, 200, 10)
    ints = []
    for points in pointlist:
        x = np.linspace(0, 2, points)
        y = np.linspace(-2, 2, points)
        dx = x[1]-x[0]
        int = 0
        for xx in x:
            fvals = f(xx, y)
            int += integrate.simps(fvals, y)*dx
        ints.append(int)

    fig = plt.figure()
    plt.plot(pointlist, ints)
    plt.xlabel("Grid points")
    plt.ylabel("Integral value")
    plt.show()

def integrate3d():
    ra = (1, 1, 1)
    rb = (2, 2, 2)
    r = lambda r1, r2: np.sqrt((r1[0]-r2[0])**2+(r1[1]-r2[1])**2+(r1[2]-r2[2])**2)

    f = lambda ri: abs(np.exp(-r(ri, ra))/np.sqrt(np.pi))**2/abs(r(ri, rb))

    x = np.linspace(-1, 1, 5)
    y = np.linspace(-1, 1, 5)
    z = np.linspace(-1, 1, 5)
    dx = x[1]-x[0]
    dy = y[1]-y[0]
    dz = z[1]-z[0]
    int = 0
    for xx in x:
        for yy in y:
            for zz in z:
                ri = (xx, yy, zz)
                int += f(ri)*dx*dy*dz
    analyticint = (1-(1+np.sqrt(3))*np.exp(-2*np.sqrt(3)))/np.sqrt(3)
    print("Evaluated integral:", int, "Analytical integral:", analyticint)
def main():
    integrate1d()
    integrate2d()
    integrate3d()


main()