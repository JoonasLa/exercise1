import numpy as np

def linear_root_search(value, x):
    # Test that data is in given range
    if not (x[0] <= value <= x[-1]):
        print("Value not in the given range")
        return False
    # Calculate length of the series N and its value range R
    N = len(x)
    R = x[-1]-x[0]

    # Compare given value to starting value and calculate correct index assuming linear values in x
    startindex = int(np.floor((value-x[0])/R*(N-1)))
    return startindex

def exponential_root_search(value, x, r_0, h):
    # Test that data is in given range
    if not (x[0] <= value <= x[-1]):
        print("Value not in the given range")
        return False
    # Calculate starting index by solving i from the equation
    # used to make x
    startindex = int(np.floor(1/h*np.log(value/r_0+1)))
    return startindex

def test_lin_root_search():
    x = np.linspace(0, 10, 20)
    i1 = linear_root_search(1.1, x)
    i2 = linear_root_search(7.7, x)
    i3 = linear_root_search(20, x)
    print(x, "\n", "1.1 index:", i1, "| 7.7 index:", i2)

def test_exp_root_search():
    r0 = 1e-3
    rmax = 10
    dim = 10
    h = np.log(rmax/r0+1)/(dim-1)

    x = np.zeros(dim)
    for i in range(1, dim):
        x[i] = r0*(np.exp(i*h)-1)

    i1 = exponential_root_search(1.1, x, r0, h)
    i2 = exponential_root_search(7.7, x, r0, h)
    i3 = exponential_root_search(200, x, r0, h)

    print(x, "\n 1.1 index:", i1, "| 7.7 index:", i2)

def main():
    test_lin_root_search()
    test_exp_root_search()

main()

