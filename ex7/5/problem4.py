#! /usr/bin/env python3

"""
Hartree code for N-electron 1D harmonic quantum dot

- Related to FYS-4096 Computational Physics 
- Test case should give:
  
    Total energy      11.502873299181957
    Kinetic energy    3.622097184934205
    Potential energy  7.880776114247752

    Density integral  3.9999938742737164

- Job description: 
  -- Problem 1: add/fill needed functions and details
  -- Problem 2: Include input and output as text file
  -- Problem 3: Include input and output as DHF5 file 
"""



from numpy import *
from matplotlib.pyplot import *
import scipy.sparse as sp
import scipy.sparse.linalg as sla
from scipy.integrate import simps
import h5py
import os

def hartree_potential(ns,x):
    """ 
    Hartree potential using 'Rieman' integration 
    - assuming linear grid
    """
    Vhartree=0.0*ns
    dx=(x[1]-x[0])
    for ix in range(len(x)):
        r = x[ix]
        for ix2 in range(len(x)):
            rp = x[ix2]
            Vhartree[ix]+=ns[ix2]*ee_potential(r-rp)
    return Vhartree*dx

def ee_potential(x):
    global ee_coef
    """ 1D electron-electron interaction """
    return ee_coef[0]/sqrt(x**2+ee_coef[1])

def ext_potential(x,m=1.0,omega=1.0):
    """ 1D harmonic quantum dot """
    return 0.5*m*omega**2*x**2

def density(psis):
    ns=zeros((len(psis[0]),))
    for i in range(len(psis)):
        ns+=abs(psis[i])**2
    return ns
    
def initialize_density(x,dx,normalization=1):
    rho=exp(-x**2)
    A=sum(rho[:-1])*dx
    return normalization/A*rho

def check_convergence(Vold,Vnew,threshold):
    difference_ = amax(abs(Vold-Vnew))
    print('  Convergence check:', difference_)
    converged=False
    if difference_ <threshold:
        converged=True
    return converged

def diagonal_energy(T,orbitals,dx):
    """ 
    Calculate diagonal energy
    (using Rieman sum)
    """
    Tt=sp.csr_matrix(T)
    E_diag=0.0
    for i in range(len(orbitals)):
        evec=orbitals[i]
        E_diag+=dot(evec.conj().T,Tt.dot(evec))
    return E_diag*dx

def offdiag_potential_energy(orbitals,x):
    """ 
    Calculate off-diagonal energy
    (using Rieman sum)
    """
    U = 0.0
    dx=x[1]-x[0]
    for i in range(len(orbitals)-1):
        for j in range(i+1,len(orbitals)):
            for i1 in range(len(x)):
                for j1 in range(len(x)):
                    U+=abs(orbitals[i][i1])**2*abs(orbitals[j][j1])**2*ee_potential(x[i1]-x[j1])
    return U*dx**2

def save_ns_in_ascii(ns,filename):
    s=shape(ns)
    f=open(filename+'.txt','w')
    for ix in range(s[0]):
        f.write('{0:12.8f}\n'.format(ns[ix]))
    f.close()
    f=open(filename+'_shape.txt','w')
    f.write('{0:5}'.format(s[0]))
    f.close()
    
def load_ns_from_ascii(filename):
    f=open(filename+'_shape.txt','r')
    for line in f:
        s=array(line.split(),dtype=int)
    f.close()
    ns=zeros((s[0],))
    d=loadtxt(filename+'.txt')
    k=0
    for ix in range(s[0]):
        ns[ix]=d[k]
        k+=1
    return ns

def save_data_to_hdf5_file(fname,orbitals,density,N_e,occ,grid,ee_coefs):
    return

def calculate_SIC(orbitals,x):
    V_SIC = []
    for i in range(len(orbitals)):
        V_SIC.append(-hartree_potential(abs(orbitals[i])**2,x))
    return V_SIC
            
def normalize_orbital(evec,dx):
    return evec/sqrt((sum(abs(evec)**2)*dx))

def kinetic_hamiltonian(x):
    grid_size = x.shape[0]
    dx = x[1] - x[0]
    dx2 = dx**2
    
    H0 = sp.diags(
        [
            -0.5 / dx2 * np.ones(grid_size - 1),
            1.0 / dx2 * np.ones(grid_size),
            -0.5 / dx2 * np.ones(grid_size - 1)
        ],
        [-1, 0, 1])
    return H0

def main():
    global ee_coef
    # e-e potential parameters [strenght, smoothness]
    ee_coef = [0.0, 1.0]
    # number of electrons
    N_e = 6
    # 1D occupations each orbital can have max 2, i.e., spin up and spin down
    # e.g., occ = [0,0,1,1] means lowest up and down, next lowest up and down
    #       occ = [0,1,2,3] means all spin up
    occ = [0,0,1,1,2,2]  

    # grid
    x=linspace(-4,4,120)
    # threshold
    threshold=1.0e-4
    # mixing value
    mix=0.2
    # maximum number of iterations
    maxiters = 100



    dx = x[1]-x[0]
    T = kinetic_hamiltonian(x)
    Vext = ext_potential(x)

    # READ in density / orbitals / etc.
    if os.path.isfile('density.txt'):
        ns=load_ns_from_ascii('density')
    else:
        ns=initialize_density(x,dx,N_e)

    print('Density integral        ', sum(ns[:-1])*dx)
    print(' -- should be close to  ', N_e)
    
    print('\nCalculating initial state')
    Vhartree=hartree_potential(ns,x)
    VSIC=[]
    for i in range(N_e):
        VSIC.append(ns*0.0)
    Veff=sp.diags(Vext+Vhartree,0)
    H=T+Veff
    for i in range(maxiters):
        print('\n\nIteration #{0}'.format(i))
        orbitals=[]
        for i in range(N_e):
            print('  Calculating orbitals for electron ', i+1)
            eigs, evecs = sla.eigs(H+sp.diags(VSIC[i],0), k=N_e, which='SR')
            eigs=real(eigs)
            evecs=real(evecs)
            print('    eigenvalues', eigs)
            evecs[:,occ[i]]=normalize_orbital(evecs[:,occ[i]],dx)
            orbitals.append(evecs[:,occ[i]])
        Veff_old = Veff
        ns=density(orbitals)
        Vhartree=hartree_potential(ns,x)
        VSIC=calculate_SIC(orbitals,x)
        Veff_new=sp.diags(Vext+Vhartree,0)
        if check_convergence(Veff_old,Veff_new,threshold):
            break
        else:
            Veff=(1.0-mix)*Veff_new+mix*Veff_old
            H = T+Veff

    print('\n\n')
    off = offdiag_potential_energy(orbitals,x)
    E_kin = diagonal_energy(T,orbitals,dx)
    E_pot = diagonal_energy(sp.diags(Vext,0),orbitals,dx) + off
    E_tot = E_kin + E_pot
    print('Total energy     ', E_tot)
    print('Kinetic energy   ', E_kin)
    print('Potential energy ', E_pot) 
    print('\nDensity integral ', sum(ns[:-1])*dx)

    # WRITE OUT density / orbitals / energetics / etc.
    #save_ns_in_ascii(ns,'density')
    
    plot(x,abs(ns))
    xlabel(r'$x$ (a.u.)')
    ylabel(r'$n(x)$ (1/a.u.)')
    title('N-electron density for N={0}'.format(N_e))
    show()

if __name__=="__main__":
    main()
