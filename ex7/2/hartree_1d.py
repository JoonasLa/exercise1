#! /usr/bin/env python3

"""
Intentionally unfinished Hartree code for 
   N-electron 1D harmonic quantum dot


- Related to FYS-4096 Computational Physics 
- Test case should give:
  
    Total energy      11.502873299181957
    Kinetic energy    3.622097184934205
    Potential energy  7.880776114247752

    Density integral  3.9999938742737164

- Job description: 
  -- Problem 1: add/fill needed functions and details
      --- some are marked with #FILL#
  -- Problem 2: Include input and output as text files
  -- Problem 3: Include input and output as DHF5 file 
"""



from numpy import *
from matplotlib.pyplot import *
import scipy.sparse as sp
import scipy.sparse.linalg as sla
from scipy.integrate import simps
import h5py
import os

def hartree_potential(ns,x):
    """ 
    Hartree potential using 'Rieman' integration 
    - assuming linear grid
    """
    Vhartree=0.0*ns
    dx=x[1]-x[0]
    for ix in range(len(x)):
        r = x[ix]
        for ix2 in range(len(x)):
            rp = x[ix2]
            Vhartree[ix]+=ns[ix2]*ee_potential(r-rp)
    return Vhartree*dx

def ee_potential(x):
    global ee_coef
    """ 1D electron-electron interaction """
    return ee_coef[0]/sqrt(x**2+ee_coef[1])     # approximate the Coulomb potential of electron-electron interaction

def ext_potential(x,m=1.0,omega=1.0):
    """ 1D harmonic quantum dot """
    return 0.5*m*omega**2*x**2

def density(orbitals):
    ns=zeros((len(orbitals[0]),))

    for i in range(len(orbitals)):
        ns = ns + orbitals[i]**2    # sum each orbital to get density
    return ns
    
def initialize_density(x,dx,normalization=1):
    rho=exp(-x**2)
    A=sum(rho[:-1])*dx
    return normalization/A*rho

def check_convergence(Vold,Vnew,threshold):
    difference_ = amax(abs(Vold-Vnew))
    print('  Convergence check:', difference_)
    converged=False
    if difference_ <threshold:
        converged=True
    return converged

def diagonal_energy(T,orbitals,dx):
    """ 
    Calculate diagonal energy
    (using Rieman sum)
    """
    Tt=sp.csr_matrix(T)
    E_diag=0.0
    for i in range(len(orbitals)):
        evec=orbitals[i]
        E_diag+=dot(evec.conj().T,Tt.dot(evec))
    return E_diag*dx

def offdiag_potential_energy(orbitals,x):
    """ 
    Calculate off-diagonal energy
    (using Rieman sum)
    """
    U = 0.0
    dx=x[1]-x[0]
    for i in range(len(orbitals)-1):
        for j in range(i+1,len(orbitals)):
            for i1 in range(len(x)):
                for j1 in range(len(x)):
                    U+=abs(orbitals[i][i1])**2*abs(orbitals[j][j1])**2*ee_potential(x[i1]-x[j1])
    return U*dx**2

# save 1d array data
def save_ns_in_ascii(ns,filename):
    s = shape(ns)
    f = open(filename + '.txt', 'w')
    for ix in range(s[0]):
        f.write('{0:12.8f}\n'.format(ns[ix]))
    f.close()
    f = open(filename + '_shape.txt', 'w')
    f.write('{0:5}'.format(s[0]))
    f.close()

# load 1d array data
def load_ns_from_ascii(filename):
    f = open(filename + '_shape.txt', 'r')
    for line in f:
        s = array(line.split(), dtype=int)
    f.close()
    ns = zeros((s[0],))
    d = loadtxt(filename + '.txt')
    k = 0
    for ix in range(s[0]):
        ns[ix] = d[k]
        k += 1
    return ns

# saving for integer type data
def save_occupations_in_ascii(occ,filename):
    s = shape(occ)

    f = open(filename + '.txt', 'w')
    if len(s) == 0:
        f.write('{0:5d}\n'.format(occ))  # if occ is not array
        f.close()
        f = open(filename+'_shape.txt','w')
        f.write('{0:5}'.format(0))
    else:
        for ix in range(s[0]):
            f.write('{0:5d}\n'.format(occ[ix]))
        f.close()
        f = open(filename + '_shape.txt', 'w')
        f.write('{0:5}'.format(s[0]))

    f.close()

# load integer type data
def load_occupations_from_ascii(filename):
    f = open(filename + '_shape.txt', 'r')
    for line in f:
        s = array(line.split(), dtype=int)
    f.close()
    if s == 0:
        occ = loadtxt(filename+'.txt',dtype=int)
        return occ
    occ = zeros((s[0],),dtype=int)
    d = loadtxt(filename + '.txt', dtype=int)
    k = 0
    for ix in range(s[0]):
        occ[ix] = d[k]
        k += 1
    return occ

# load 2d array data
def load_orbitals_from_ascii(filename):
    f = open(filename + '_shape.txt', 'r')
    for line in f:
        s = array(line.split(), dtype=int)
    f.close()

    orbs = zeros((s[0], s[1]))
    d = loadtxt(filename + '.txt')
    k = 0
    for ix in range(s[1]):  # row at a time
        for jx in range(s[0]):
            orbs[jx][ix] = d[k][jx]
        k += 1

    return orbs

# save 2d array data
def save_orbitals_in_ascii(orbs,filename):
    s = shape(orbs)
    f = open(filename + '.txt', 'w')

    for ix in range(s[1]):
        for jx in range(s[0]):
            f.write('{0:12.8f}'.format(orbs[jx][ix]))
        f.write('\n')

    f.close()

    # save the shape
    f = open(filename + '_shape.txt', 'w')
    f.write('{0:5} {1:5}'.format(s[0], s[1]))
    f.close()


def save_data_to_hdf5_file(fname,orbitals,density,N_e,occ,grid,ee_coefs):

    f  = h5py.File(fname,'w')

    gset = f.create_dataset("grid", data=grid, dtype='f')
    gset.attrs["info"] = '1D grid'

    oset = f.create_dataset("orbitals", shape=(N_e,len(grid)), dtype='f')
    oset.attrs["info"] = '1D orbitals as (len(grid),N_electrons)'
    for i in range(len(orbitals)):
        oset[i,:]=orbitals[i]

    dset = f.create_dataset("density",data=density, dtype='f')
    dset.attrs["info"] = '1D density'

    Ne = f.create_dataset("N_e",data=N_e, dtype='int32')
    Ne.attrs["info"] = 'Number of electrons'

    occset = f.create_dataset("occupation",data=occ,dtype='int32')
    occset.attrs["info"] = 'Electron occupations in orbitals'

    ee_coef_set = f.create_dataset("ee_coefficients",data=ee_coefs,dtype ='f')
    ee_coef_set.attrs["info"] = "Coefficients of electron-electron interaction"
    f.close()
    return

def calculate_SIC(orbitals,x):
    V_SIC = []
    for i in range(len(orbitals)):
        V_SIC.append(-hartree_potential(abs(orbitals[i])**2,x))
    return V_SIC
            
def normalize_orbital(single_orb,dx):
    """ Normalize the orbitals to one """
    norm = sqrt(simps(single_orb**2,dx=dx))  # calculate the integral
    # divide orbitals with integral value to get normalized orbitals
    return single_orb/norm


def kinetic_hamiltonian(x):
    grid_size = x.shape[0]
    dx = x[1] - x[0]
    dx2 = dx**2
    
    H0 = sp.diags(
        [
            -0.5 / dx2 * np.ones(grid_size - 1),
            1.0 / dx2 * np.ones(grid_size),
            -0.5 / dx2 * np.ones(grid_size - 1)
        ],
        [-1, 0, 1])
    return H0

def ascii_test(N_e,S,load = True,threshold = 1e-6,maxiters = 100):

    """Problem 2 code"""
    fname_density = 'density_'+str(N_e)+"_"+str(S)
    fname_grid = 'grid'+str(N_e)+"_"+str(S)
    fname_orbital = 'orbitals'+str(N_e)+"_"+str(S)
    fname_occ = 'occupations'+str(N_e)+"_"+str(S)
    fname_coeff = 'coefficients'+str(N_e)+"_"+str(S)
    fname_Ne ='N_e'+str(N_e)+"_"+str(S)
    global ee_coef
    # --- Setting up the system etc. ---


    # e-e potential parameters [strenght, smoothness]=[a,b]
    if os.path.isfile(fname_coeff+'.txt') and load:
        ee_coef = load_ns_from_ascii(fname_coeff)
    else:
        ee_coef = [1.0, 1.0]
    # number of electrons
    if os.path.isfile(fname_Ne+'.txt') and load:
        N_e = load_occupations_from_ascii(fname_Ne)

    # 1D occupations each orbital can have max 2, i.e., spin up and spin down
    # e.g., occ = [0,0,1,1] means lowest up and down, next lowest up and down
    #       occ = [0,1,2,3] means all spin up
    if os.path.isfile(fname_occ+'.txt') and load:
        occ = load_occupations_from_ascii(fname_occ)
    else:
        occ = zeros(N_e,dtype=int)
        if S == 0:
            k = 0
            for i in range(len(occ)):
                occ[i] = k          # for S = 0
                k += i%2
        else:
            for i in range(len(occ)):
                occ[i] = i           # for S = 3

    # grid
    if os.path.isfile(fname_grid+'.txt') and load:
        x = load_ns_from_ascii(fname_grid)
    else:
        x = linspace(-4, 4, 120)

    # mixing value
    mix = 0.2
    # maximum number of iterations

    # --- End setting up the system etc. ---

    # Initialization and calculations start here

    dx = x[1] - x[0]
    T = kinetic_hamiltonian(x)
    Vext = ext_potential(x)

    # READ in density / orbitals / etc.
    if os.path.isfile(fname_density+'.txt') and load:
        ns = load_ns_from_ascii(fname_density)
    else:
        ns = initialize_density(x, dx, N_e)

    if os.path.isfile(fname_orbital+'.txt') and load:
        orbitals = load_orbitals_from_ascii(fname_orbital)
        VSIC = calculate_SIC(orbitals,x)
    else:
        VSIC = []
        for i in range(N_e):
            VSIC.append(ns*0.0)

    print('Density integral        ', sum(ns[:-1]) * dx)
    print(' -- should be close to  ', N_e)

    print('\nCalculating initial state')
    Vhartree = hartree_potential(ns, x)



    Veff = sp.diags(Vext + Vhartree, 0)
    H = T + Veff
    for i in range(maxiters):
        print('\n\nIteration #{0}'.format(i))
        orbitals = []
        for i in range(N_e):
            print('  Calculating orbitals for electron ', i + 1)
            eigs, evecs = sla.eigs(H + sp.diags(VSIC[i], 0), k=N_e, which='SR')
            eigs = real(eigs)
            evecs = real(evecs)
            print('    eigenvalues', eigs)
            evecs[:, occ[i]] = normalize_orbital(evecs[:, occ[i]], dx)  # normalize the orbitals
            orbitals.append(evecs[:, occ[i]])
        Veff_old = Veff
        ns = density(orbitals)  # calculate the electron density
        Vhartree = hartree_potential(ns, x)
        VSIC = calculate_SIC(orbitals, x)
        Veff_new = sp.diags(Vext + Vhartree, 0)
        if check_convergence(Veff_old, Veff_new, threshold):
            break
        else:
            """ Mixing the potential """
            Veff = Veff_old * mix + (1 - mix) * Veff_new
            H = T + Veff

    print('\n\n')
    off = offdiag_potential_energy(orbitals, x)
    E_kin = diagonal_energy(T, orbitals, dx)
    E_pot = diagonal_energy(sp.diags(Vext, 0), orbitals, dx) + off
    E_tot = E_kin + E_pot
    print('Total energy     ', E_tot)
    print('Kinetic energy   ', E_kin)
    print('Potential energy ', E_pot)
    print('\nDensity integral ', sum(ns[:-1]) * dx)

    # WRITE OUT density / orbitals / energetics / etc.

    save_ns_in_ascii(ns,fname_density)
    save_ns_in_ascii(x,fname_grid)
    save_occupations_in_ascii(occ,fname_occ)
    save_occupations_in_ascii(N_e,fname_Ne)
    save_ns_in_ascii(ee_coef,fname_coeff)

    save_orbitals_in_ascii(orbitals,fname_orbital)


    plot(x, abs(ns))
    xlabel(r'$x$ (a.u.)')
    ylabel(r'$n(x)$ (1/a.u.)')
    title('N-electron density for N={0} and S={1} '.format(N_e,S))
    show()


def hdf5_test(N_e,S,load = True,threshold = 1.0e-6,maxiters = 100):
    """Problem 3 code"""
    fname = 'save_file_'+str(N_e)+"_"+str(S)+".hdf5"
    global ee_coef
    # --- Setting up the system etc. ---

    # if data is saved in hdf5 file
    if os.path.isfile(fname) and load:
        f = h5py.File(fname, 'r')

        ee_coef = array(f['ee_coefficients'])
        N_e = array(f['N_e'])
        occ = array(f['occupation'])
        x = array(f['grid'])
        f.close()
    # If no data is saved to hdf5 file, then generate new data
    else:
        # e-e potential parameters [strenght, smoothness]=[a,b]
        ee_coef = [1.0, 1.0]
        # number of electrons

        # 1D occupations each orbital can have max 2, i.e., spin up and spin down
        # e.g., occ = [0,0,1,1] means lowest up and down, next lowest up and down
        #       occ = [0,1,2,3] means all spin up
        occ = zeros(N_e, dtype=int)
        if S == 0:
            k = 0
            for i in range(len(occ)):
                occ[i] = k  # for S = 0
                k += i % 2
        else:
            for i in range(len(occ)):
                occ[i] = i  # for S = 3


        # grid
        x = linspace(-4, 4, 120)

    # mixing value
    mix = 0.2


    # --- End setting up the system etc. ---

    # Initialization and calculations start here

    dx = x[1] - x[0]
    T = kinetic_hamiltonian(x)
    Vext = ext_potential(x)

    # READ in density / orbitals / etc.

    # if data is saved, then load orbitals and density from file
    if os.path.isfile(fname) and load:
        f = h5py.File(fname,"r")
        ns = array(f['density'])
        orbitals = array(f['orbitals'])
        f.close()
    # if not, generate new data
    else:
        ns = initialize_density(x, dx, N_e)
        orbitals = []
        for i in range(N_e):
            orbitals.append(ns * 0.0)

    print('Density integral        ', sum(ns[:-1]) * dx)
    print(' -- should be close to  ', N_e)

    print('\nCalculating initial state')
    Vhartree = hartree_potential(ns, x)
    VSIC = []
    # If save file is found, then calculate VSIC from that data
    if os.path.isfile(fname) and load:
        VSIC = calculate_SIC(orbitals, x)
    else:
        for i in range(N_e):
            # initialized as zero, since no orbitals yet
            VSIC.append(orbitals[i]*0.0)

    Veff = sp.diags(Vext + Vhartree, 0)
    H = T + Veff
    for i in range(maxiters):
        print('\n\nIteration #{0}'.format(i))
        orbitals = []
        for i in range(N_e):
            print('  Calculating orbitals for electron ', i + 1)
            eigs, evecs = sla.eigs(H + sp.diags(VSIC[i], 0), k=N_e, which='SR')
            eigs = real(eigs)
            evecs = real(evecs)
            print('    eigenvalues', eigs)
            evecs[:, occ[i]] = normalize_orbital(evecs[:, occ[i]], dx)  # normalize the orbitals
            orbitals.append(evecs[:, occ[i]])
        Veff_old = Veff
        ns = density(orbitals)  # calculate the electron density
        Vhartree = hartree_potential(ns, x)
        VSIC = calculate_SIC(orbitals, x)
        Veff_new = sp.diags(Vext + Vhartree, 0)
        if check_convergence(Veff_old, Veff_new, threshold):
            break
        else:
            """ Mixing the potential """
            Veff = Veff_old * mix + (1 - mix) * Veff_new
            H = T + Veff

    print('\n\n')
    off = offdiag_potential_energy(orbitals, x)
    E_kin = diagonal_energy(T, orbitals, dx)
    E_pot = diagonal_energy(sp.diags(Vext, 0), orbitals, dx) + off
    E_tot = E_kin + E_pot
    print('Total energy     ', E_tot)
    print('Kinetic energy   ', E_kin)
    print('Potential energy ', E_pot)
    print('\nDensity integral ', sum(ns[:-1]) * dx)

    # WRITE OUT density / orbitals / energetics / etc.

    save_data_to_hdf5_file(fname, orbitals, ns, N_e, occ, x, ee_coef)

    f = h5py.File(fname,"r")

    plot(x, abs(ns))
    xlabel(r'$x$ (a.u.)')
    ylabel(r'$n(x)$ (1/a.u.)')
    title('N-electron density for N={0} and S={1} '.format(N_e,S))
    show()



def main():
    load = True  # set to false if want new data
    threshold = 1e-5
    maxiters = 100
    N = 6   # number of electrons
    S = 0   # total spin, change between N/2 and 0

    hdf5_test(N,S,load,threshold,maxiters)
    #ascii_test(N,S,load,threshold,maxiters)

if __name__=="__main__":
    main()
