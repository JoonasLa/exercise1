""" Creates figures for problem 4 of exercise 7. """

import h5py
from matplotlib.pyplot import *
import numpy as np

def main():
    f1 = h5py.File('density_6_ground' + '.hdf5', "r")
    f2 = h5py.File('density_6_S=3' + '.hdf5', "r")

    ns1 = np.array(f1["density"])
    ns2 = np.array(f2["density"])
    x1 = np.array(f1["grid"])
    x2 = np.array(f2["grid"])
    N_e1 = np.array(f1["N_e"])
    N_e2 = np.array(f2["N_e"])

    Etot1 = np.array(f1["Etot"])
    Epot1 = np.array(f1["Epot"])
    Ekin1 = np.array(f1["Ekin"])
    Etot2 = np.array(f2["Etot"])
    Epot2 = np.array(f2["Epot"])
    Ekin2 = np.array(f2["Ekin"])

    figure()

    subplot(1, 2, 1)
    plot(x1, np.abs(ns1))
    xlabel(r'$x$ (a.u.)')
    ylabel(r'$n(x)$ (1/a.u.)')
    title('N-electron density for N={0}, Ground'.format(N_e1))
    grid(True)
    text(-2, 0,
         "Total energy    {:.4f} \nKinetic energy    {:.4f} \nPotential energy  {:.4f}".format(Etot1, Ekin1, Epot1),
         fontsize=12)
    subplot(1, 2, 2)
    plot(x2, np.abs(ns2))
    xlabel(r'$x$ (a.u.)')
    ylabel(r'$n(x)$ (1/a.u.)')
    title('N-electron density for N={0}, S=3'.format(N_e2))
    grid(True)
    text(-2, 0,
         "Total energy    {:.4f} \nKinetic energy    {:.4f} \nPotential energy  {:.4f}".format(Etot2, Ekin2, Epot2),
         fontsize=12)
    show()


if __name__ == '__main__':
    main()