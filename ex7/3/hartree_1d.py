#! /usr/bin/env python3

"""
Intentionally unfinished Hartree code for 
   N-electron 1D harmonic quantum dot


- Related to FYS-4096 Computational Physics 
- Test case should give:
  
    Total energy      11.502873299181957
    Kinetic energy    3.622097184934205
    Potential energy  7.880776114247752

    Density integral  3.9999938742737164

- Job description: 
  -- Problem 1: add/fill needed functions and details
      --- some are marked with #FILL#
  -- Problem 2: Include input and output as text files
  -- Problem 3: Include input and output as DHF5 file 
"""



from numpy import *
from matplotlib.pyplot import *
from scipy.integrate import simps
import scipy.sparse as sp
import scipy.sparse.linalg as sla
from scipy.integrate import simps
import os

def hartree_potential(ns,x):
    """ 
    Hartree potential using 'Riemann' integration
    - assuming linear grid
    So, interaction between electrons by integrating electron density.
    """
    Vhartree=0.0*ns
    dx=x[1]-x[0]
    for ix in range(len(x)):
        r = x[ix]
        for ix2 in range(len(x)):
            rp = x[ix2]
            Vhartree[ix]+=ns[ix2]*ee_potential(r-rp)
    return Vhartree*dx

def ee_potential(x):
    global ee_coef
    """ 1D electron-electron interaction """
    V_ee = ee_coef[0]/sqrt(x**2 + ee_coef[1])
    return V_ee

def ext_potential(x,m=1.0,omega=1.0):
    """ 1D harmonic quantum dot """
    return 0.5*m*omega**2*x**2

def density(orbitals):
    """ Calculates the complete electron density from column vectors of orbital data."""
    ns=zeros((len(orbitals[0]),))
    for i in range(len(orbitals)):
        ns += abs(orbitals[i])**2
    return ns
    
def initialize_density(x,dx,normalization=1):
    rho=exp(-x**2)
    A=sum(rho[:-1])*dx
    return normalization/A*rho

def check_convergence(Vold,Vnew,threshold):
    difference_ = amax(abs(Vold-Vnew))
    print('  Convergence check:', difference_)
    converged=False
    if difference_ <threshold:
        converged=True
    return converged

def diagonal_energy(T,orbitals,dx):
    """ 
    Calculate diagonal energy
    (using Rieman sum)
    """
    Tt=sp.csr_matrix(T)
    E_diag=0.0
    for i in range(len(orbitals)):
        evec=orbitals[i]
        E_diag+=dot(evec.conj().T,Tt.dot(evec))
    return E_diag*dx

def offdiag_potential_energy(orbitals,x):
    """ 
    Calculate off-diagonal energy
    (using Rieman sum)
    """
    U = 0.0
    dx=x[1]-x[0]
    for i in range(len(orbitals)-1):
        for j in range(i+1,len(orbitals)):
            for i1 in range(len(x)):
                for j1 in range(len(x)):
                    U+=abs(orbitals[i][i1])**2*abs(orbitals[j][j1])**2*ee_potential(x[i1]-x[j1])
    return U*dx**2

def save_ns_in_ascii(ns,filename):
    s=shape(ns)
    f=open(filename+'.txt','w')
    for ix in range(s[0]):
        f.write('{0:12.8f}\n'.format(ns[ix]))
    f.close()
    f=open(filename+'_shape.txt','w')
    f.write('{0:5}'.format(s[0]))
    f.close()

def save_orbitals_in_ascii(orbitals,filename):
    f = open(filename + '_orbitals.txt', 'w')
    num_orb = len(orbitals)
    vec_length = len(orbitals[0])
    f.write("Number of orbitals = {}".format(num_orb))
    f.write("\nLength of vectors = {}".format(vec_length))

    for i in range(num_orb):
        f.write("\nOrb{}:".format(i+1))
        for ix in range(vec_length):
            f.write(" {0:12.8f}".format(orbitals[i][ix]))
    f.close()

def save_grid_in_ascii(x, filename):
    f = open(filename + '_x.txt', 'w')
    for i in range(len(x)):
        f.write('{0:12.8f} '.format(x[i]))
    f.close()

def save_misc_in_ascii(N_e, occ, ee_coefs, filename):
    f = open(filename + '_misc.txt', 'w')
    f.write('{}\n'.format(N_e))
    for i in range(len(occ)):
        f.write('{} '.format(occ[i]))
    f.write('\n')
    for i in range(len(ee_coefs)):
        f.write('{} '.format(ee_coefs[i]))
    f.close()

def load_misc_from_ascii(filename):
    f = open(filename + '_misc.txt', 'r')
    for i, line in enumerate(f):
        if i == 0:
            N_e = int(line)
        elif i == 1:
            occ = array(line.split(), dtype=int)
        elif i == 2:
            ee_coefs = array(line.split(), dtype=float)
    f.close()
    return N_e, occ, ee_coefs

def load_grid_from_ascii(filename):
    f = open(filename + '_x.txt', 'r')
    for line in f:
        x = array(line.split(),dtype=float)
    f.close()
    return x

def load_orbitals_from_ascii(filename):
    f = open(filename + '_orbitals.txt', 'r')
    orbitals = []
    for i, line in enumerate(f):
        if i == 0:
            num_orb = int(line[20:])
        elif i == 1:
            vec_length = len(line[19:])
        else:
            orb = array(line[5:].split(), dtype=float)
            orbitals.append(orb)
    f.close()
    return orbitals

def load_ns_from_ascii(filename):
    f=open(filename+'_shape.txt','r')
    for line in f:
        s=array(line.split(),dtype=int)
    f.close()
    ns=zeros((s[0],))
    d=loadtxt(filename+'.txt')
    k=0
    for ix in range(s[0]):
        ns[ix]=d[k]
        k+=1
    return ns

def save_data_to_hdf5_file(fname,orbitals,density,N_e,occ,grid,ee_coefs):
    return

def calculate_SIC(orbitals,x):
    """ Calculates the self interaction correction for all orbital vectors in list of orbitals."""
    V_SIC = []
    for i in range(len(orbitals)):
        V_SIC.append(-hartree_potential(abs(orbitals[i])**2,x))
    return V_SIC
            
def normalize_orbital(single_orb,dx):
    """ Normalize the orbital probability density to one """
    N_const = simps(abs(single_orb)**2, dx=dx)
    norm_orb = single_orb/np.sqrt(N_const)
    return norm_orb

def kinetic_hamiltonian(x):
    grid_size = x.shape[0]
    dx = x[1] - x[0]
    dx2 = dx**2
    
    H0 = sp.diags(
        [
            -0.5 / dx2 * np.ones(grid_size - 1),
            1.0 / dx2 * np.ones(grid_size),
            -0.5 / dx2 * np.ones(grid_size - 1)
        ],
        [-1, 0, 1])
    return H0

def main():
    # --- Setting up the system etc. ---
    global ee_coef
    # e-e potential parameters [strenght, smoothness]=[a,b]
    ee_coef = [1.0, 1.0]
    # number of electrons
    N_e = 4
    # 1D occupations each orbital can have max 2, i.e., spin up and spin down
    # e.g., occ = [0,0,1,1] means lowest up and down, next lowest up and down
    #       occ = [0,1,2,3] means all spin up
    occ = [0,1,2,3]
    # grid
    x=linspace(-4,4,120)
    # threshold
    threshold=1.0e-4
    # mixing value
    mix=0.2  #alpha
    # maximum number of iterations
    maxiters = 100
    filename = 'density'
    # --- End setting up the system etc. ---


    # Initialization and calculations start here

    dx = x[1]-x[0]
    T = kinetic_hamiltonian(x)
    Vext = ext_potential(x)

    # READ in density / orbitals / etc.
    if os.path.isfile(filename+ '.txt'):
        ns=load_ns_from_ascii(filename)
        N_e, occ, ee_coef = load_misc_from_ascii(filename)
    else:
        ns=initialize_density(x,dx,N_e)  # Initial guess of electron density

    print('Density integral        ', sum(ns[:-1])*dx)
    print(' -- should be close to  ', N_e)
    
    print('\nCalculating initial state')
    Vhartree=hartree_potential(ns,x)
    VSIC=[]
    if os.path.isfile(filename+ '_orbitals.txt'):
        orbitals = load_orbitals_from_ascii(filename)
        x = load_grid_from_ascii(filename)
        VSIC = calculate_SIC(orbitals, x)  # Calculate self interaction correction
    else:
        for i in range(N_e):
            # initialized as zero, since no orbitals yet
            VSIC.append(ns*0.0)
    Veff=sp.diags(Vext+Vhartree,0)  # Diagonal matrix of effective potential
    H=T+Veff  # Hamiltonian for the case

    # Creating the orbitals and changing them by iterating the potential with the new orbitals:
    for i in range(maxiters):
        print('\n\nIteration #{0}'.format(i))
        orbitals=[]

        # Orbitals for 'this' potential:
        for i in range(N_e):
            print('  Calculating orbitals for electron ', i+1)
            eigs, evecs = sla.eigs(H+sp.diags(VSIC[i],0), k=N_e, which='SR')  # SR = smallest real part
            eigs=real(eigs)
            evecs=real(evecs)
            print('    eigenvalues', eigs)
            evecs[:,occ[i]]=normalize_orbital(evecs[:,occ[i]],dx)  # occ chooses the lowest wanted "excited" orbital
            orbitals.append(evecs[:,occ[i]])

        # Update potential with these new orbitals:
        Veff_old = Veff
        ns=density(orbitals) # Calculate electron density from our new orbitals
        Vhartree=hartree_potential(ns,x)  # Calculate the potential with the new density
        VSIC=calculate_SIC(orbitals,x)  # Calculate self interaction correction
        Veff_new=sp.diags(Vext+Vhartree,0)  # Add everything to our new potential
        if check_convergence(Veff_old,Veff_new,threshold):
            break
        else:
            """ Mixing the potential, since no convergence was achieved """
            Veff = mix*Veff_old + (1-mix)*Veff_new
            H = T+Veff

    print('\n\n')
    off = offdiag_potential_energy(orbitals,x)
    E_kin = diagonal_energy(T,orbitals,dx)
    E_pot = diagonal_energy(sp.diags(Vext,0),orbitals,dx) + off
    E_tot = E_kin + E_pot
    print('Total energy     ', E_tot)
    print('Kinetic energy   ', E_kin)
    print('Potential energy ', E_pot) 
    print('\nDensity integral ', sum(ns[:-1])*dx)

    # WRITE OUT density / orbitals / energetics / etc.
    #save_ns_in_ascii(ns,'density')
    filename = 'density'
    save_ns_in_ascii(ns, filename)
    save_orbitals_in_ascii(orbitals, filename)
    save_grid_in_ascii(x, filename)
    save_misc_in_ascii(N_e, occ, ee_coef, filename)

    
    plot(x,abs(ns))
    xlabel(r'$x$ (a.u.)')
    ylabel(r'$n(x)$ (1/a.u.)')
    title('N-electron density for N={0}'.format(N_e))
    grid(True)
    text(-2, 0, "Total energy    {:.4f} \nKinetic energy    {:.4f} \nPotential energy  {:.4f}".format(E_tot, E_kin, E_pot), fontsize=12)
    show()

if __name__=="__main__":
    main()
