"""
Problems 1 2 3 and 4: Hartree simulation of N particles.

All problems are implemented with same base function but with a bit differing
parameters.

File is basing on original code made by Ilkka Kylänpää

"""

"""
Intentionally unfinished Hartree code for 
   N-electron 1D harmonic quantum dot


- Related to FYS-4096 Computational Physics 
- Test case should give:
  
    Total energy      11.502873299181957
    Kinetic energy    3.622097184934205
    Potential energy  7.880776114247752

    Density integral  3.9999938742737164

- Job description: 
  -- Problem 1: add/fill needed functions and details
      --- some are marked with #FILL#
  -- Problem 2: Include input and output as text files
  -- Problem 3: Include input and output as DHF5 file 
"""

import numpy as np
import matplotlib.pyplot as plt
import scipy.sparse as sp
import scipy.sparse.linalg as sla
from scipy.integrate import simps
import h5py
import os


def hartree_potential(ns, x):
    """ 
    Hartree potential using 'Rieman' integration 
    - assuming linear grid
    """
    Vhartree = 0.0 * ns
    dx = x[1] - x[0]
    for ix in range(len(x)):
        r = x[ix]
        for ix2 in range(len(x)):
            rp = x[ix2]
            Vhartree[ix] += ns[ix2] * ee_potential(r - rp)
    return Vhartree * dx


def ee_potential(x):
    global ee_coef
    """ 1D electron-electron interaction. Using formula give in problem 
    question. """
    if np.nonzero(x):
        return ee_coef[0] / np.sqrt(x**2 + ee_coef[1])
    else:
        return 0.0


def ext_potential(x, m=1.0, omega=1.0):
    """ 1D harmonic quantum dot. """
    return 0.5 * m * omega ** 2 * x ** 2


def density(orbitals):
    """ Produce electronic denstiy by summing squares of orbitals.
    Equation 18 in week 10 slides."""
    ns = np.zeros((len(orbitals[0]),))
    for i in range(len(orbitals)):
        ns += np.transpose(orbitals[i]) * orbitals[i]
    return ns


def initialize_density(x, dx, normalization=1):
    """
    Initialize initial guess for density by gaussian function
    (Single particle harmonic oscillator has gaussian density.)
    """
    rho = np.exp(-x ** 2)
    A = sum(rho[:-1]) * dx
    return normalization / A * rho


def check_convergence(Vold, Vnew, threshold):
    """Check if convergence of orbitals has happened."""
    difference_ = np.amax(abs(Vold - Vnew))
    print('    Convergence check:', difference_)
    converged = False
    if difference_ < threshold:
        converged = True
    return converged


def diagonal_energy(T, orbitals, dx):
    """ 
    Calculate diagonal energy
    (using Rieman sum)
    """
    Tt = sp.csr_matrix(T)
    E_diag = 0.0
    for i in range(len(orbitals)):
        evec = orbitals[i]
        E_diag += np.dot(evec.conj().T, Tt.dot(evec))
    return E_diag * dx


def offdiag_potential_energy(orbitals, x):
    """ 
    Calculate off-diagonal energy
    (using Rieman sum)
    """
    U = 0.0
    dx = x[1] - x[0]
    for i in range(len(orbitals) - 1):
        for j in range(i + 1, len(orbitals)):
            for i1 in range(len(x)):
                for j1 in range(len(x)):
                    U += abs(orbitals[i][i1]) ** 2 * abs(
                        orbitals[j][j1]) ** 2 * ee_potential(x[i1] - x[j1])
    return U * dx ** 2


def save_1d_array(ns, filename, fileformat):
    """ Save 1d array either to text file or from hdf5 file."""
    if (fileformat=="txt"):
        np.savetxt(filename + "." + fileformat, ns)
    elif (fileformat=="h5"):
        param_info = filename[filename.find("_N"):]
        h5f = h5py.File('data' + param_info + '.h5', 'a')
        # Create or overwrite
        if not filename in h5f.keys():
            dset = h5f.create_dataset(filename, data=ns)
        else:
            dset = h5f[filename]
            dset[...] = ns
        dset.attrs["info"] = '1d array, type: np.ndarray[float]'
        h5f.close()
    else:
        assert(False)


def save_orbitals(orbitals, filename, fileformat):
    """ Save orbitals either to text file or from hdf5 file.
    'orbitals' is list of 1d arrays.
    """
    orb = np.empty((len(orbitals), len(orbitals[0])))
    for i in range(len(orb)):
        orb[i] = orbitals[i]

    if (fileformat=="txt"):
        np.savetxt(filename + "." + fileformat, orb)
    elif (fileformat=="h5"):
        param_info = filename[filename.find("_N"):]
        h5f = h5py.File('data' + param_info + '.h5', 'a')
        if not filename in h5f.keys():
            dset = h5f.create_dataset(filename, data=orb)
        else:
            dset = h5f[filename]
            dset[...] = orb
        dset.attrs["info"] = 'Orbitals, type: List[np.ndarray[float]]'
        h5f.close()
    else:
        assert(False)

def load_1d_array(filename, fileformat):
    """ Load 1d array either from text file or from hdf5 file."""
    if (fileformat=="txt"):
        ns = np.loadtxt(filename + "." + fileformat)
    elif (fileformat=="h5"):
        param_info = filename[filename.find("_N"):]
        h5f = h5py.File('data' + param_info + '.h5', 'r')
        ns = h5f[filename][:]
        h5f.close()
    else:
        assert(False)

    return ns

def load_orbitals(filename, fileformat):
    """ Load orbitals either from text file or from hdf5 file.
    'orbitals' is list of 1d arrays.
    """
    if (fileformat=="txt"):
        orb = np.loadtxt(filename + "." + fileformat)
    elif (fileformat=="h5"):
        param_info = filename[filename.find("_N"):]
        h5f = h5py.File('data' + param_info + '.h5', 'r')
        orb = h5f[filename][:]
        h5f.close()
    else:
        assert(False)

    VSIC = []
    for i in range(len(orb)):
        VSIC.append(orb[i])
    return VSIC


def calculate_SIC(orbitals, x):
    """Calculate correction term that removes potential of electro with itself.
    """
    V_SIC = []
    for i in range(len(orbitals)):
        V_SIC.append(-hartree_potential(abs(orbitals[i]) ** 2, x))
    return V_SIC


def normalize_orbital(single_orb, dx):
    """ Normalize the orbitals to one. Equation 16 in week 10 slides."""

    return single_orb / np.sqrt(np.trapz(np.conjugate(single_orb)*single_orb,
                                         dx=dx))


def kinetic_hamiltonian(x):
    """ Kinetic part of hamiltonian. """
    grid_size = x.shape[0]
    dx = x[1] - x[0]
    dx2 = dx ** 2

    H0 = sp.diags(
        [
            -0.5 / dx2 * np.ones(grid_size - 1),
            1.0 / dx2 * np.ones(grid_size),
            -0.5 / dx2 * np.ones(grid_size - 1)
        ],
        [-1, 0, 1])
    return H0


def run(threshold=1.0e-4, fileformat="txt", plot=None, occ=(0, 1, 2, 3), a=1.0):
    """
    Hartree simulation of N particles.

    :param threshold:   Determines when wavefunction is converged
    :param fileformat:  Format in which data is saved ("txt" or "h5")
    :param plot:        Axes to which image is plotted. If None, do not plot .
    :param occ:         Determines number of electron and their spins
    # 1D occupations each orbital can have max 2, i.e., spin up and spin down
    # e.g., occ = [0,0,1,1] means lowest up and down, next lowest up and down
    # occ = [0,1,2,3] means all spin up
    :return: None
    """

    """ Runs Hartree simulation on four electrons in harmonic potential.
    Stroes data that can be recovered next time. With different parameters this
    function can be used as a answer to problems 1, 2 and 3."""
    assert (fileformat == "txt" or fileformat == "h5")

    # --- Setting up the system etc. ---
    global ee_coef
    # e-e potential parameters [strenght, smoothness]=[a,b]
    ee_coef = [a, 1.0]

    occ = occ
    # number of electrons
    N_e = len(occ)
    # grid
    x = np.linspace(-4, 4, 120)

    # mixing value
    mix = 0.2
    # maximum number of iterations
    maxiters = 100
    # --- End setting up the system etc. ---
    S = (occ[-1]+1) - N_e//2
    param_info = "_N{}_S{}_a{}".format(N_e, S, a)
    density_name = "data_density" + param_info
    grid_name = "data_grid" + param_info
    orbitals_name = "data_orbitals" + param_info


    # Initialization and calculations start here

    dx = x[1] - x[0]
    T = kinetic_hamiltonian(x)
    Vext = ext_potential(x)

    # Do not load files if not found or if x-grid differs
    load_files = False
    if fileformat == "txt" and os.path.isfile(grid_name + ".txt") and \
                               os.path.isfile(orbitals_name + ".txt") and \
                               os.path.isfile(density_name + ".txt"):
        x_saved = load_1d_array(grid_name, fileformat)
        load_files = np.isclose(x, x_saved).all()
    if fileformat == "h5" and os.path.isfile("data" + param_info + ".h5"):
        x_saved = load_1d_array(grid_name, fileformat)
        load_files = np.isclose(x, x_saved).all()

    # READ in density / orbitals / etc.
    if load_files:
        print("    Loading orbitals from text file")
        ns = load_1d_array(density_name, fileformat)
        orbitals = load_orbitals(orbitals_name, fileformat)
        VSIC = calculate_SIC(orbitals, x)
    else:
        print("    No valid stored orbitals found, generating all from beginning.")
        VSIC = []
        ns = initialize_density(x, dx, N_e)
        for i in range(N_e):
            # initialized as zero, since no orbitals yet
            VSIC.append(ns * 0.0)

    Vhartree = hartree_potential(ns, x)
    Veff = sp.diags(Vext + Vhartree, 0)
    H = T + Veff
    for i in range(maxiters):
        print('        Iteration #{0}'.format(i))
        orbitals = []
        for i in range(N_e):
            eigs, evecs = sla.eigs(H + sp.diags(VSIC[i], 0), k=N_e, which='SR')
            eigs = np.real(eigs)
            evecs = np.real(evecs)
            evecs[:, occ[i]] = normalize_orbital(evecs[:, occ[i]], dx)
            orbitals.append(evecs[:, occ[i]])
        Veff_old = Veff
        ns = density(orbitals)
        Vhartree = hartree_potential(ns, x)
        VSIC = calculate_SIC(orbitals, x)
        Veff_new = sp.diags(Vext + Vhartree, 0)
        if check_convergence(Veff_old, Veff_new, threshold):
            break
        else:
            """ Mixing the potential Eq. 26 in slides 7."""
            Veff = mix*Veff_old + (1-mix)*Veff_new
            H = T + Veff

    print('')
    off = offdiag_potential_energy(orbitals, x)
    E_kin = diagonal_energy(T, orbitals, dx)
    E_pot = diagonal_energy(sp.diags(Vext, 0), orbitals, dx) + off
    E_tot = E_kin + E_pot
    strs = [
        ('$E_{{tot}}$ {:.5f}'.format(E_tot)),
        ('$E_{{kin}}$ {:.5f}'.format(E_kin)),
        ('$E_{{pot}}$ {:.5f}'.format(E_pot)),
        ('$\\int \\rho$     {:.5f}'.format(sum(ns[:-1]) * dx))
    ]
    print("    " + "\n    ".join(strs))

    # WRITE OUT density / orbitals / energetics / etc.
    save_1d_array(ns, density_name, fileformat)
    save_1d_array(x, grid_name, fileformat)
    save_orbitals(orbitals, orbitals_name, fileformat)

    if plot is not None:
        ax = plot
        is_correlated = a**2 > 1e-9
        label = "$V_{{e-e}}" + (1-int(is_correlated)) * "=0" + "$"
        ax.plot(x, abs(ns), label=label)
        ax.set_xlabel(r'$x$ (a.u.)')
        ax.set_ylabel(r'$n(x)$ (1/a.u.)')
        ax.set_title('El. dens. N={0} and occ={1}'
                     .format(N_e, occ))
        ax.text(0.25, 0.01 + (1-int(is_correlated))*0.4,
                label + ":  " + ("\n{}:  ".format(label)).join(strs),
                verticalalignment="bottom", horizontalalignment="left",
                transform=ax.transAxes)
        ax.legend(loc=1)

def main():
    fig1, ax1 = plt.subplots(1, 1, figsize=(5,4), num="4 electron density")
    fig2, (ax2, ax3) = plt.subplots(1, 2, figsize=(10,4), num="6 electron density")

    print("\n\nProblem 1:")
    print("###########################################")
    print("Running with tolerance 1e-4")
    run(threshold=1.0e-4, fileformat="txt", plot=ax1)

    print("\n\nProblem 2: load orbitals from txt")
    print("###########################################")
    print("Running again with tolerance 1e-10")
    run(threshold=1.0e-10, fileformat="txt", plot=None)

    print("\n\nProblem 3: load orbitals from hdf5")
    print("###########################################")
    print("Running with tolerance 1e-4")
    run(threshold=1.0e-4, fileformat="h5", plot=None)
    print("\n\nRunning again with tolerance 1e-10")
    run(threshold=1.0e-10, fileformat="h5", plot=None)

    print("\n\nProblem 4: Six electrons")
    print("###########################################")
    print("Interacting electrons:")
    run(threshold=1.0e-4, fileformat="h5", plot=ax2, occ=(0, 0, 1, 1, 2, 2))
    run(threshold=1.0e-4, fileformat="h5", plot=ax3, occ=(0, 1, 2, 3, 4, 5))
    print("Energies raise a lot (18 vs 26) when spins are aligned.\n")
    print("Non-interacting electrons:")
    run(threshold=1.0e-4, fileformat="h5", plot=ax2, occ=(0, 0, 1, 1, 2, 2), a=0.0)
    run(threshold=1.0e-4, fileformat="h5", plot=ax3, occ=(0, 1, 2, 3, 4, 5), a=0.0)
    print("Energies decrease a lot  (18 vs 9 or 26 vs 18) when e-e interaction is taken off.")
    print("5-10 iterations seems to bee well enough for tresholds 1e-4 and 1e-10")

    plt.show()


if __name__ == "__main__":
    main()