from scipy.integrate import simps
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def calculate_integral():
    # Declare function and lattice vector a1, a2
    f = lambda x, y: (x + y) * np.exp(-0.5 * np.sqrt(x ** 2 + y ** 2))
    a1 = (1.1, 0)
    a2 = (0.5, 1)

    # Number of gridpoints used in integration
    N = 100
    # Grid spacing for lattice vectors
    A1, A2 = np.meshgrid(np.linspace(0, 1, N), np.linspace(0, 1, N))

    # Compute corresponging real-space points
    X = A1 * a1[0] + A2 * a2[0]
    Y = A1 * a1[1] + A2 * a2[1]
    # Calculate function in each point
    F = f(X, Y)

    # Calculate integrals first for each row
    int1 = simps(F, X)
    print(int1)
    #Final answer is integral from row integrals
    int2 = simps(int1, Y[:, 0])
    print("Value of the integral:", int2)

    # Make wireframe of integration area
    fig = plt.figure()
    ax = fig.add_subplot(121, projection='3d')
    ax.plot_wireframe(X, Y, F, rstride=10, cstride=10)
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('F(x, y)')

    # Make contour plot of integration area
    ax2 = fig.add_subplot(122)
    ax2.contourf(X, Y, F)
    ax2.axis('equal')
    ax2.set_xlabel('x')
    ax2.set_ylabel('y')

    # Show 'cool' animation
    for angle in range(0, 700, 5):
        ax.view_init(30, angle)
        plt.draw()
        plt.pause(.01)

    plt.show()

def main():
   calculate_integral()

if __name__ == "__main__":
    main()