"""
FYS-4096 Computational Physics
Author: Joonas Latukka
Project work 1
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import simps

def eval_derivative(f, x, dx=0.001):
    """
    Simple function to evaluate derivative
    :param f: function to be evaluated
    :param x: x-value where derivative is being evaluated
    :param dx: difference used to evaluate derivative
    :return: f'(x)
    """
    return (f(x+dx)-f(x-dx))/(2*dx)

def solve_zero_newton(f, x0=0, tolerance=1e-5):
    """
    Used to solve where function reaches zero using Newton's  method
    :param f: function that is being used
    :param x0: startig point for iteration
    :param tolerance: tolerance to determine zero point
    :return: x-value for which f(x) = 0
    """
    x = x0
    func_value = f(x0)
    while np.abs(func_value) > tolerance:
        x = x-func_value/eval_derivative(f, x, dx=0.001)
        func_value = f(x)
    return x

def Yukawa_potential(r: float, a:float = 1.0):
    """
    Yukawa's potential
    :param r: distance from potential center
    :param a: parameter defining the width of potential
    :return: potential at given distance
    """
    kappa = 1.0
    return kappa/r*np.exp(-r/a)

def solve_r_min(b, V, E=1.0):
    """
    Solve r_min using eq. (13) in project_work.pdf
    :param b: impact parameter
    :param V: Potential function used
    :param E: Total energy
    :return: r_min
    """
    f = lambda r: 1-b**2/r**2 - V(r)/E
    r_min = solve_zero_newton(f, x0=0.1, tolerance=1e-5)
    return r_min

def solve_theta(b, V, r_min, E=1.0):
    """
    Solve theta using eq. (14) in project_work.pdf
    :param b: impact parameter
    :param V: Potential function used
    :param r_min: r_min that has been solved using solve_r_min
    :param E: Total energy
    :return: Theta
    """
    # First integral can be solved analytically to equal pi/2b. Only the second integral in equation (14) must be solved
    # When r is close to rm, sqrt(1-b²/r²-V(r)/E) is close to zero and integral diverges. Approximation must be used
    # Itegral 1/(r^2*sqrt(f(r)))dr ~ 1/(r_min^2*sqrt(f'(rmin)*(r-rmin)))dr which can be solved analytically and show
    # that it converges
    f = lambda r: 1-b**2/r**2-V(r)/E
    epsilon = 0.1
    int1 = 2.0/r_min**2*np.sqrt(epsilon*r_min/eval_derivative(f, r_min))

    # When r >> r_min, f is close to 1. Define limit for that. Integral is approximated to be 1/r^2 in that region and
    # can be analytically solved as r belongs (uplim, inf)
    uplim = 10*r_min
    while(np.abs(f(uplim) - 1) > 0.01):
        uplim *= 2
    int2 = 1/uplim

    # Region between approximations is well-behaving and can be integrated numerically
    x = np.linspace((1+epsilon)*r_min, uplim, 10000)
    y = 1/(x**2*np.sqrt(f(x)))
    int3 = simps(y, x)

    # Add together all regions
    int_tot = int1+int2+int3

    # Calculate theta. First integral is 2b*pi/2b->pi and second has been calculated
    theta = np.pi-2*b*int_tot
    return theta


def impact_parameter(r, v):
    """
    Calculate impact parameter when initial position and velocity is known. Not used in this code
    :param r: Initial position
    :param v: Initial velocity
    :return: Impact parameter
    """
    b = np.linalg.norm(r - np.dot(r, v) / np.linalg.norm(v) ** 2 * v)
    return b

def cross_section(V=Yukawa_potential, num_points=100, db=0.001):
    """
    Main function to calculate scattering coefficients to multiple directions at once
    :param V: Funktion to calculate potential as V = V(r)
    :param num_points: How many points are calculated
    :param db: Differential for b used when calculating differential of db/dtheta
    :return: Scattering coefficients in sigmas and corresponding thetas
    """
    # Solves thetas and cross section using several values for b
    # Find upper limit for b that produces thetas < 0.1
    uplim = 0.1
    while True:
        r_min = solve_r_min(uplim, V)
        theta = solve_theta(uplim, V, r_min)
        if theta < 0.1:
            break
        uplim *= 2
    # Find lower limit that produces thetas > 3.1
    lowlim=uplim
    while True:
        r_min = solve_r_min(lowlim, V)
        theta = solve_theta(lowlim, V, r_min)
        if theta > 3.1:
            break
        lowlim /= 2

    # Thetas are more evenly distributed if b is distributed logarithmically between calculated limits
    b_list = np.logspace(np.log10(lowlim), np.log10(uplim), num_points)
    thetas = np.zeros(num_points)
    sigmas = np.zeros(num_points)
    dthetas = np.zeros(num_points)

    for i in range(num_points):
        # Solve theta that corresponds to b
        b = b_list[i]
        r_min = solve_r_min(b, V, E=1.0)
        assert r_min > 0
        thetas[i] = solve_theta(b, V, r_min, E=1.0)

        # Change b a bit and solve again theta. The difference between theta(b + db) and theta(b) is equal to dtheta, which is
        # being used later to solve sigma
        r_min = solve_r_min(b+db, V, E=1.0)
        dthetas[i] = solve_theta(b+db, V, r_min, E=1.0)-thetas[i]

    # Calculate sigmas using equation 12 in project work pdf
    for i in range(num_points):
        sigmas[i] = np.abs(b_list[i]/np.sin(thetas[i])*db/(dthetas[i]))

    # Values close to theta=0 diverge, so remove them from arrays
    sigmas = sigmas[thetas > 0.1]
    thetas = thetas[thetas > 0.1]
    return sigmas, thetas

def rutherford_scattering(theta, kappa=1.0, E=1.0):
    """
    Analytical solution to scattering when Coulomb potential is used
    :param theta:
    :param kappa:
    :param E:
    :return: Scattering coefficients
    """
    sigma = (kappa/(4*E))**2*1/np.sin(theta/2)**4
    return sigma

def test_functions():
    """
    Used to test different function while writing code
    :return:
    """
    BB = np.linspace(0, 2, 100)
    thetas = np.zeros(1000)
    V_pot = lambda r: Yukawa_potential(r, 1.0)
    for i in range(1000):
        b = BB[i]
        r_min = solve_r_min(b, V_pot)
        thetas[i] = solve_theta(b, V_pot, r_min)
    plt.plot(BB, thetas)
    plt.show()
    plt.plot(np.diff(thetas))
    plt.show()

def main():
    """
    Main function to be used to calculate and plot different scatterings
    :return:
    """
    # Make figure and draw plot of Rutherford's scattering to it
    f = plt.figure()
    thetas = np.linspace(0.1, np.pi, 100)
    sigmas = rutherford_scattering(thetas)
    plt.plot(thetas, np.log(sigmas), 'k--', label="Rutherford's scattering")

    # Solve cross_section of scattering with different values of a used in Yukawa's potential
    for a in (0.1, 1.0, 10.0, 100.0):
        V_pot = lambda r: Yukawa_potential(r, a)
        sigmas, thetas = cross_section(V_pot)
        plt.plot(thetas, np.log(sigmas), label='a=%3.2f' % (a))

    # Configure plot
    plt.legend()
    plt.xlabel(r'$\theta$ (rad)')
    plt.ylabel(r'ln$(\sigma(\theta))$')
    plt.title("Scattering using different Yukawa's potentials")
    plt.show()

if __name__ == "__main__":
    main()
    #test_functions()