from numpy import *
from matplotlib.pyplot import *

class Atom:
    def __init__(self,index,mass,dt,dims):
        self.index = index
        self.mass = mass
        self.dt = dt
        self.dims = dims
        self.LJ_epsilon = None
        self.LJ_sigma = None
        self.R = None
        self.v = None
        self.force = None

    def set_LJ_parameters(self,LJ_epsilon,LJ_sigma):
        self.LJ_epsilon = LJ_epsilon
        self.LJ_sigma = LJ_sigma

    def set_position(self,coordinates):
        self.R = coordinates
        
    def set_velocity(self,velocity):
        self.v = velocity

    def set_force(self,force):
        self.force = force

class Observables:
    def __init__(self):
        self.E_kin = []
        self.E_pot = []
        self.distance = []
        self.Temperature = []

def calculate_energetics(atoms):
    N = len(atoms)
    V = 0.0
    E_kin = 0.0
    for i in range(N):
        E_kin += 0.5*atoms[i].mass*dot(atoms[i].v,atoms[i].v)
        for j in range(i+1,N):
            V += pair_potential(atoms[i],atoms[j])
    return E_kin, V

def calculate_force(atoms):
    N = len(atoms)
    ij_map = zeros((N,N),dtype=int)
    Fs = []
    ind = 0
    for i in range(0,N-1):
        for j in range(i+1,N):
            Fs.append(pair_force(atoms[i],atoms[j]))
            ij_map[i,j] = ind
            ij_map[j,i] = ind
            ind += 1
    F = []
    for i in range(N):
        f = zeros(shape=shape(atoms[i].R))
        for j in range(N):
            ind = ij_map[i,j]
            if i<j:
                f += Fs[ind]
            elif i>j:
                f -= Fs[ind]
        F.append(f)
    #F = array(F)
    return F

def pair_force(atom1,atom2):
    return Morse_force(atom1,atom2)
    #return lennard_jones_force(atom1,atom2)

def pair_potential(atom1,atom2):
    return Morse_potential(atom1,atom2)
    #return lennard_jones_potential(atom1,atom2)

def Morse_potential(atom1,atom2):
    # H2 parameters
    De = 0.1745
    re = 1.40
    a = 1.0282
    r = atom1.R-atom2.R
    dr = sqrt(sum(r**2))
    return De*(1.0-exp(-a*(dr-re)))**2-De

def Morse_force(atom1,atom2):
    # H2 parameters
    De = 0.1745
    re = 1.40
    a = 1.0282
    r = atom1.R-atom2.R
    dr = sqrt(sum(r**2))
    f = exp(-a*(dr-re))
    return -2.0*De*a*f*(1.0-f)*r/dr

def lennard_jones_potential(atom1,atom2):
    epsilon = sqrt(atom1.LJ_epsilon*atom2.LJ_epsilon)
    sigma = (atom1.LJ_sigma+atom2.LJ_sigma)/2
    r = atom1.R-atom2.R
    dr = sqrt(sum(r**2))
    return 4.0*epsilon*((sigma/dr)**12-(sigma/dr)**6)

def lennard_jones_force(atom1,atom2):
    epsilon = sqrt(atom1.LJ_epsilon*atom2.LJ_epsilon)
    sigma = (atom1.LJ_sigma+atom2.LJ_sigma)/2
    r = atom1.R-atom2.R
    dr = sqrt(sum(r**2))
    return -24.0*epsilon/sigma*((sigma/dr)**7-2.0*(sigma/dr)**13)*r/dr

def velocity_verlet_update(atoms):
    dt = atoms[0].dt
    dt2 = dt**2
    for i in range(len(atoms)):
        atoms[i].R += dt*atoms[i].v + dt2*atoms[i].force/2/atoms[i].mass
    Fnew = calculate_force(atoms)
    for i in range(len(atoms)):
        atoms[i].v += dt*(atoms[i].force+Fnew[i])/2/atoms[i].mass
        atoms[i].force = Fnew[i]
    return atoms
    
def initialize_positions(atoms):
    # diatomic case
    atoms[0].set_position(array([-0.8,0.0,0.0]))
    atoms[1].set_position(array([0.7,0.0,0.0]))

def initialize_velocities(atoms,T):
    # ADD initial velocities
    # diatomic case
    dims = atoms[0].dims
    kB=3.16e-6 # in hartree/Kelvin
    for i in range(len(atoms)):
        v_max = sqrt(3.0/atoms[i].mass*kB*T)
        atoms[i].set_velocity(array([1.0,0.0,0.0])*v_max)
        #atoms[i].set_velocity((random.rand(dims)-0.5))
        #atoms[i].v = atoms[i].v*v_max/sqrt(sum(atoms[i].v**2))
    atoms[1].v = -1.0*atoms[0].v

def initialize_force(atoms):
    F=calculate_force(atoms)
    for i in range(len(atoms)):
        atoms[i].set_force(F[i])

def Temperature(average_kinetic_energy,atoms):
    # Energy in hartree units
    kB = 3.16e-6
    # ADD calculation of temperature
    d = atoms[0].dims
    N = len(atoms)
    return average_kinetic_energy*2.0/d/N/kB

def calculate_observables(atoms,observables):
    E_k, E_p = calculate_energetics(atoms)
    observables.E_kin.append(E_k)
    observables.E_pot.append(E_p)
    # ADD calculation of internuclear distance observable
    observables.distance.append(sqrt(sum((atoms[0].R-atoms[1].R)**2)))
    observables.Temperature.append(Temperature(E_k,atoms))
    return observables

def main():
    N_atoms = 2
    dims = 3
    dt = 0.1
    mass = 1860.0
    T = 10.0

    # Initialize atoms
    atoms = []    
    for i in range(N_atoms):
        atoms.append(Atom(i,mass,dt,dims))
        atoms[i].set_LJ_parameters(0.1745,1.25)
    # Initialize observables
    observables = Observables()

    # Initialize positions, velocities, and forces
    initialize_positions(atoms)
    initialize_velocities(atoms,T)
    initialize_force(atoms)

    E_k, E_p = calculate_energetics(atoms)
    print('T', Temperature(E_k,atoms))

    for i in range(100000):
        atoms = velocity_verlet_update(atoms)
        if ( i % 10 == 0):
            observables = calculate_observables(atoms,observables)            

    E_kin = array(observables.E_kin)
    E_pot = array(observables.E_pot)
    print('E_kin',mean(E_kin))
    print('E_pot',mean(E_pot))
    print('E_tot',mean(E_kin)+mean(E_pot))
    print('distance', mean(array(observables.distance)))
    print('T', Temperature(mean(E_kin),atoms))
    print('T', mean(observables.Temperature),std(observables.Temperature) )
    figure()
    plot(E_kin+E_pot)
    figure()
    plot(E_pot)
    figure()
    plot(array(observables.distance))
    show()

if __name__=="__main__":
    main()
        
