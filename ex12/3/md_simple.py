"""
Simple Molecular Dynamics code for course FYS-4096 Computational Physics

Problem 1:
- Make the code to work and solve H2 using the Morse potential.
- Modify and comment especially at parts where it reads "# ADD"
- Follow instructions on ex12.pdf

Problems 2:
- Add observables: temperature, distance, and heat capacity.
- Follow instructions on ex12.pdf

Problem 3:
- Add Lennard-Jones capability etc.
- Follow instructions on ex12.pdf

Problem 4:
- Temperature dependent data and analysis
- Follow instructions on ex12.pdf

"""




from numpy import *
from matplotlib.pyplot import *

class Atom:
    def __init__(self,index,mass,dt,dims):
        self.index = index
        self.mass = mass
        self.dt = dt
        self.dims = dims
        self.LJ_epsilon = None
        self.LJ_sigma = None
        self.R = None
        self.v = None
        self.force = None

    def set_LJ_parameters(self,LJ_epsilon,LJ_sigma):
        self.LJ_epsilon = LJ_epsilon
        self.LJ_sigma = LJ_sigma

    def set_position(self,coordinates):
        self.R = coordinates
        
    def set_velocity(self,velocity):
        self.v = velocity

    def set_force(self,force):
        self.force = force

class Observables:
    def __init__(self):
        self.E_kin = []
        self.E_pot = []
        self.distance = []
        self.Temperature = []

def calculate_energetics(atoms):
    N = len(atoms)
    V = 0.0
    E_kin = 0.0

    # computing kinectic energies
    for atom in atoms:
        E_kin += 1/2*atom.mass*(atom.v).dot(atom.v)

    #computing potential energies
    for i in range(N-1):
        atom1 = atoms[i]
        for j in range(i+1,N):
            atom2 = atoms[j]
            V += pair_potential(atom1,atom2)

    return E_kin, V

def calculate_force(atoms):
    N = len(atoms)
    ij_map = zeros((N,N),dtype=int)
    Fs = []
    ind = 0

    #iterates all possible combinations of atoms and calculates pair forces between atoms
    for i in range(0,N-1):
        for j in range(i+1,N):
            Fs.append(pair_force(atoms[i],atoms[j]))
            ij_map[i,j] = ind
            ij_map[j,i] = ind
            ind += 1
    F = []
    for i in range(N):
        f = zeros(shape=shape(atoms[i].R))
        for j in range(N):
            ind = ij_map[i,j]
            if i<j:
                f += Fs[ind]
            elif i>j:
                f -= Fs[ind]
        F.append(f)
    F = array(F)
    return F

def pair_force(atom1,atom2):
    #return lennard_jones_force(atom1,atom2)
    return Morse_force(atom1,atom2)

def pair_potential(atom1,atom2):
    #return lennard_jones_potential(atom1,atom2)
    return Morse_potential(atom1,atom2)

def Morse_potential(atom1,atom2):
    #morse potential for two atoms
    # H2 parameters given here
    De = 0.1745
    re = 1.40
    a = 1.0282
    r = linalg.norm(atom1.R-atom2.R)
    # ADD comments and calculation of Morse potential
    V = De*(1-exp(-1*a*(r-re)))**2 - De

    return V

def Morse_force(atom1,atom2):
    # computes force of Morse potential according to equation 3 of lecture 12
    # H2 parameters
    De = 0.1745
    re = 1.40
    a = 1.0282
    r = linalg.norm(atom1.R-atom2.R)

    F = -2*De*(1-exp(-1*a*(r-re)))*a*exp(-1*a*(r-re))

    return F*(atom1.R-atom2.R)/r

def lennard_jones_potential(atom1,atom2):
    #lennard jones potential between two atoms
    epsilon = sqrt(atom1.LJ_epsilon*atom2.LJ_epsilon)
    sigma = (atom1.LJ_sigma+atom2.LJ_sigma)/2
    r = linalg.norm(atom1.R-atom2.R)

    V = 4*epsilon*((sigma/r)**12-(sigma/r)**6)

    return V

def lennard_jones_force(atom1,atom2):
    #computes force of lennard jones potential according to equation 3 of lecture 12
    epsilon = sqrt(atom1.LJ_epsilon*atom2.LJ_epsilon)
    sigma = (atom1.LJ_sigma+atom2.LJ_sigma)/2

    r = linalg.norm(atom1.R - atom2.R)
    gradient = 4*epsilon*(6*sigma**6/(r**7)-12*sigma**12/(r**13))
    return -1*gradient*(atom1.R-atom2.R)/r

def velocity_verlet_update(atoms):
    #computes time evolution for position and velocity using verlet_algorithm
    #arrcorgin to lecture 12 equations 10 and 11
    dt = atoms[0].dt
    dt2 = dt**2
    for i in range(len(atoms)):
        #position update
        atoms[i].R +=  dt*atoms[i].v + dt2/2/atoms[i].mass*atoms[i].force
    Fnew = calculate_force(atoms)
    for i in range(len(atoms)):
        # velocity update
        atoms[i].v += dt/2/atoms[i].mass*(Fnew[i]+atoms[i].force)
        #force update
        atoms[i].force = Fnew[i] # update force
    return atoms
    
def initialize_positions(atoms):
    # diatomic case
    atoms[0].set_position(array([-0.8,0.0,0.0]))
    atoms[1].set_position(array([0.7,0.0,0.0]))

def initialize_velocities(atoms,T):
    # vmax is computed velocity of given temperature T
    # diatomic case
    dims = atoms[0].dims
    kB=3.16e-6 # in hartree/Kelvin
    for i in range(len(atoms)):
        v_max = sqrt(3.0/atoms[i].mass*kB*T)
        atoms[i].set_velocity(array([1.0,0.0,0.0])*v_max)
    atoms[1].v = -1.0*atoms[0].v

def initialize_force(atoms):
    F=calculate_force(atoms)
    for i in range(len(atoms)):
        atoms[i].set_force(F[i])

def Temperature(atoms,E_k):
    # Boltzmann constant in Hartree/Kelvin
    kB = 3.16e-6
    d = atoms[0].dims
    N = len(atoms)
    K = mean(E_k)
    T = K*2/d/kB/N
    return T

def calculate_observables(atoms,observables):
    E_k, E_p = calculate_energetics(atoms)
    observables.E_kin.append(E_k)
    observables.E_pot.append(E_p)

    r = linalg.norm(atoms[0].R - atoms[1].R)
    observables.distance.append(r)

    T = Temperature(atoms,E_k)
    observables.Temperature.append(T)

    return observables

def Morse_pot(r):
    De = 0.1745
    re = 1.40
    a = 1.0282
    return De*(1-exp(-1*a*(r-re)))**2 - De

def Morse_force_single_param(r):
    De = 0.1745
    re = 1.40
    a = 1.0282
    return -2*De*(1-exp(-1*a*(r-re)))*a*exp(-1*a*(r-re))

def lenard_jones_pot(r):
    epsilon = 0.1745
    sigma = 1.25
    return 4*epsilon*((sigma/r)**12-(sigma/r)**6)

def lenard_jones_force(r):
    epsilon = 0.1745
    sigma = 1.25
    return -4*epsilon*(6*sigma**6/(r**7)-12*sigma**12/(r**13))

def problem1_2_3():
    T = 10
    N_atoms = 2
    dims = 3
    dt = 0.1
    mass = 1860.0

    # Initialize atoms
    atoms = []
    for i in range(N_atoms):
        atoms.append(Atom(i, mass, dt, dims))
        atoms[i].set_LJ_parameters(0.1745, 1.25)
    # Initialize observables
    observables = Observables()

    # Initialize positions, velocities, and forces
    initialize_positions(atoms)
    initialize_velocities(atoms,T)
    initialize_force(atoms)

    # E_k, E_p = calculate_energetics(atoms)
    # print('T', Temperature(E_k,atoms))

    for i in range(100000):
        atoms = velocity_verlet_update(atoms)
        if (i % 10 == 0):
            observables = calculate_observables(atoms, observables)

            # Print energies
    E_kin = array(observables.E_kin)
    E_pot = array(observables.E_pot)
    T = array(observables.Temperature)
    distance = array(observables.distance)

    kB = 3.16e-6
    E_tot = E_kin + E_pot
    Cv = var(E_tot) / (kB * mean(T) ** 2)
    # Cv = (mean(E_tot**2)-mean(E_tot)**2)/(kB*mean(T)**2)

    print('E_kin', mean(E_kin))
    print('E_pot', mean(E_pot))
    print('E_tot', mean(E_kin) + mean(E_pot))
    print('Temperature: ', mean(T))
    print('Distance: ', mean(distance))
    print('Heat Capacity: ', Cv)
    print('Standard deviation of temperature: ', std(T))

    figure()
    plot(observables.E_kin,label = 'Kinectic energy')
    # title('Kinectic energy')

    plot(observables.E_pot,label = 'Potential energy')
    legend(loc = 0)
    title('Potential energy and kinectic energy')

    figure()
    plot(T)
    title('Temperature')

    figure()
    plot(distance)
    title('Distance')

    r = linspace(1.2, 1.8, 200)
    lenard_pot = lenard_jones_pot(r)
    morse_pot = Morse_pot(r)
    lenard_force = lenard_jones_force(r)
    morse_force = Morse_force_single_param(r)

    figure()
    plot(r, lenard_pot, label='Lenard Jones')
    plot(r, morse_pot, label='Morse')
    legend(loc=0)
    title('Potential')

    figure()
    plot(r, lenard_force, label='Lenard Jones')
    plot(r, morse_force, label='Morse')
    legend(loc=0)
    title('Force')

    show()

def problem4_diff_T(T):
    N_atoms = 2
    dims = 3
    dt = 0.1
    mass = 1860.0

    # Initialize atoms
    atoms = []
    for i in range(N_atoms):
        atoms.append(Atom(i, mass, dt, dims))
        atoms[i].set_LJ_parameters(0.1745, 1.25)
    # Initialize observables
    observables = Observables()

    # Initialize positions, velocities, and forces
    initialize_positions(atoms)
    initialize_velocities(atoms, T)
    initialize_force(atoms)

    # E_k, E_p = calculate_energetics(atoms)
    # print('T', Temperature(E_k,atoms))

    for i in range(100000):
        atoms = velocity_verlet_update(atoms)
        if (i % 10 == 0):
            observables = calculate_observables(atoms, observables)

            # Print energies
    E_kin = array(observables.E_kin)
    E_pot = array(observables.E_pot)
    T = array(observables.Temperature)
    distance = array(observables.distance)

    kB = 3.16e-6
    E_tot = E_kin + E_pot
    Cv = var(E_tot) / (kB * mean(T) ** 2)
    return mean(E_kin),mean(E_pot),mean(T),std(T),Cv,mean(distance)

def problem4():
    Temp = linspace(100,900,10)
    E_kins = []
    E_pots = []
    Temperatures = []
    Temperatures_std = []
    Heat_capacitys = []
    distances = []
    for T in Temp:
        print('Calculating temperature: ',T)
        E_kin,E_pot,Te,std_Te,Cv,distance = problem4_diff_T(T)
        E_kins.append(E_kin)
        E_pots.append(E_pot)
        Temperatures.append(Te)
        Temperatures_std.append(std_Te)
        Heat_capacitys.append(Cv)
        distances.append(distance)

    figure()
    plot(Temp,E_kins,label = 'Kinectic energy')
    plot(Temp,E_pots,label = 'Potential energy')
    legend(loc = 0)
    xlabel('Temperature')
    ylabel('Energy')
    title('Energies')

    figure()
    plot(Temp,Temperatures,label = 'Temperature')
    title('Temperatures')
    legend(loc = 0)
    ylabel('Calculated temperature')
    xlabel('Given temperature')

    figure()
    plot(Temp,Temperatures_std,label = 'Standard deviation of temperature')
    xlabel('Temperature')
    ylabel('Standard deviation')
    title('Standard deviation')
    legend(loc = 0)

    figure()
    plot(Temp,Heat_capacitys,label = 'Heat Capacity')
    xlabel('Temperature')
    ylabel('Heat Capacity')
    title('Heat capacity')
    legend(loc = 0)

    figure()
    plot(Temp,distances,label = 'distance')
    xlabel('Temperature')
    ylabel('Distance')
    legend(loc = 0)
    title('Distance between atoms')

    show()


def main():
    #problem1_2_3()
    problem4()

if __name__=="__main__":
    main()
        
