# Problems 1 and 2:
# Jesse Saari
# 229027


from numpy import *
from matplotlib.pyplot import *

class Atom:
    def __init__(self,index,mass,dt,dims):
        self.index = index
        self.mass = mass
        self.dt = dt
        self.dims = dims
        self.LJ_epsilon = None
        self.LJ_sigma = None
        self.R = None
        self.v = None
        self.force = None

    def set_LJ_parameters(self,LJ_epsilon,LJ_sigma):
        self.LJ_epsilon = LJ_epsilon
        self.LJ_sigma = LJ_sigma

    def set_position(self,coordinates):
        self.R = coordinates
        
    def set_velocity(self,velocity):
        self.v = velocity

    def set_force(self,force):
        self.force = force

class Observables:
    def __init__(self):
        self.E_kin = []
        self.E_pot = []
        self.distance = []
        self.Temperature = []

# Function calculates the total kinetic energy and total potential energy (Morse potential) of the atoms in the system.
def calculate_energetics(atoms):
    N = len(atoms)
    V = 0.0
    E_kin = 0.0
    for i in range(len(atoms)):
        # Total kinetic energy of the atoms in the system
        E_kin += 1/2*atoms[i].mass*dot(atoms[i].v, atoms[i].v)
        for j in range(len(atoms)):
            if i != j:
                # Total potential energy between all the atoms.
                V += pair_potential(atoms[i], atoms[j])
    return E_kin, V/2

# Function calculates the forces between the atoms. The F consists of all the forces between atoms in the system.
def calculate_force(atoms):
    N = len(atoms)
    ij_map = zeros((N,N),dtype=int)
    Fs = []
    ind = 0
    for i in range(0,N-1):
        for j in range(i+1,N):
            Fs.append(pair_force(atoms[i],atoms[j]))
            ij_map[i,j] = ind
            ij_map[j,i] = ind
            ind += 1
    F = []
    for i in range(N):
        f = zeros(shape=shape(atoms[i].R))
        for j in range(N):
            ind = ij_map[i,j]
            # The atom causes the force to other atom and the other way round but opposite direction.
            if i<j:
                f += Fs[ind]
            elif i>j:
                f -= Fs[ind]
        F.append(f)
    F = array(F)
    return F

# The force (Morse force) between two atoms.
def pair_force(atom1,atom2):
    return Morse_force(atom1,atom2)

# The potential (Morse potential) between two atoms.
def pair_potential(atom1,atom2):
    return Morse_potential(atom1,atom2)

# Function calculates the Morse potential for H2 molecule.
def Morse_potential(atom1,atom2):
    # H2 parameters given here
    De = 0.1745
    re = 1.40
    a = 1.0282
    # Morse potential calculated with the equation from the lecture slides.
    u = De*(1-exp(-a*(linalg.norm(atom1.R-atom2.R)-re)))**2-De
    return u

# Function calculates the Morse force for H2 molecule.
def Morse_force(atom1,atom2):
    # H2 parameters
    De = 0.1745
    re = 1.40
    a = 1.0282
    # The interacting potential between two atoms can be calculated with the negative gradient of the potential energy.
    F = -2*De*a*exp(-a*(linalg.norm(atom1.R-atom2.R)-re))*(1-exp(-a*(linalg.norm(atom1.R-atom2.R)-re)))*(atom1.R-atom2.R)/linalg.norm(atom1.R-atom2.R)
    return F

"""
def lennard_jones_potential(atom1,atom2):
    epsilon = sqrt(atom1.LJ_epsilon*atom2.LJ_epsilon)
    sigma = (atom1.LJ_sigma+atom2.LJ_sigma)/2
    # ADD --> in problem 3: comments and calculation of LJ potential
    #
    #
    return 

def lennard_jones_force(atom1,atom2):
    epsilon = sqrt(atom1.LJ_epsilon*atom2.LJ_epsilon)
    sigma = (atom1.LJ_sigma+atom2.LJ_sigma)/2
    # ADD --> in problem 3: comments and calculation of LJ force
    #
    #
    return 
"""

# Function uses the Velocity Verlet method to update the position and velocity of the atoms.
def velocity_verlet_update(atoms):
    # The time step dt from the "Atom" class.
    dt = atoms[0].dt
    dt2 = dt**2
    for i in range(len(atoms)):
        # For loop is going through all the atoms and updates the positions.
        atoms[i].R += dt*atoms[i].v + dt2/(2*atoms[i].mass)*atoms[i].force
    # Using the calculate_force function to calculate the new forces between the atoms.
    Fnew = calculate_force(atoms)
    for i in range(len(atoms)):
        # For loop is going through all the atoms and updates the velocities.
        atoms[i].v += dt/(2*atoms[i].mass)*(Fnew[i]+atoms[i].force)
        atoms[i].force = Fnew[i] # update force
    return atoms

# The function initializes the positions of the atoms.
def initialize_positions(atoms):
    # diatomic case
    atoms[0].set_position(array([-0.8,0.0,0.0]))
    atoms[1].set_position(array([0.7,0.0,0.0]))

# The function initializes the velocities of the atoms.
def initialize_velocities(atoms):
    # ADD --> in problem 4: comment on what is v_max
    # diatomic case
    dims = atoms[0].dims
    kB=3.16e-6 # in hartree/Kelvin
    for i in range(len(atoms)):
        v_max = sqrt(3.0/atoms[i].mass*kB*10.0)
        atoms[i].set_velocity(array([1.0,0.0,0.0])*v_max)
    atoms[1].v = -1.0*atoms[0].v

# The function initializes the forces between the atoms.
def initialize_force(atoms):
    F=calculate_force(atoms)
    for i in range(len(atoms)):
        atoms[i].set_force(F[i])

# The function calculates the temperature of the system.
def Temperature(atoms):
    # Boltzmann constant in Hartree/Kelvin
    kB = 3.16e-6
    E_kin = 0.0
    # For loob is going through all the atoms and calculates the total kinetic energy of the atoms.
    for i in range(len(atoms)):
        E_kin += 1/2*atoms[i].mass*dot(atoms[i].v, atoms[i].v)

    # Temperature calculated with the equation from the lecture slides.
    T = 2*mean(E_kin)/(kB*atoms[i].dims*len(atoms))
    return T

# The function calculates the observables from one "measurement" and returns them.
def calculate_observables(atoms,observables):
    E_k, E_p = calculate_energetics(atoms)# The kinetic and potential energy
    observables.E_kin.append(E_k)
    observables.E_pot.append(E_p)
    T = Temperature(atoms) # The temperature of the system.
    observables.Temperature.append(T)
    r = linalg.norm(atoms[0].R-atoms[1].R) # The interatomic distance.
    observables.distance.append(r)
    return observables

def main():
    N_atoms = 2
    dims = 3
    dt = 0.1
    mass = 1860.0
    kB = 3.16e-6

    # Initialize atoms
    atoms = []    
    for i in range(N_atoms):
        atoms.append(Atom(i,mass,dt,dims))
        atoms[i].set_LJ_parameters(0.1745,1.25)
    # Initialize observables
    observables = Observables()

    # Initialize positions, velocities, and forces
    initialize_positions(atoms)
    initialize_velocities(atoms)
    initialize_force(atoms)

    for i in range(100000):
        atoms = velocity_verlet_update(atoms)
        if ( i % 10 == 0):
            observables = calculate_observables(atoms,observables)            

    # Print energies
    E_kin = array(observables.E_kin)
    E_pot = array(observables.E_pot)
    E_tot = E_kin+E_pot
    T = array(observables.Temperature)
    r = array(observables.distance)
    # Calculating the heat capacity
    HeatCapacity =(mean(E_tot**2)-mean(E_tot)**2)/(kB*T**2)

    # Print energies, temperature (and standard deviation), interatomic distance and heat capacity
    print('E_kin',mean(E_kin))
    print('E_pot',mean(E_pot))
    print('E_tot',mean(E_kin)+mean(E_pot))
    print('Temperature', mean(T))
    print('Standard deviation of temperature', std(T))
    print('Interatomic distance', mean(r))
    print('Heat capacity', mean(HeatCapacity))
    show()

    # Plot total energy, temperature, interatomic distance and heat capacity as a function of time
    t = linspace(0, 1000, 10000)
    plot(t, E_tot)
    title('Simulation observables of total energy')
    xlabel('Time')
    ylabel('Total energy')
    show()
    plot(t, T)
    title('Simulation observables of temperature')
    xlabel('Time')
    ylabel('Temperature')
    show()
    plot(t, r)
    title('Simulation observables of interatomic distance')
    xlabel('Time')
    ylabel('Interatomic distance')
    show()
    plot(t, HeatCapacity)
    title('Simulation observables of heat capacity')
    xlabel('Time')
    ylabel('Heat capacity')
    show()

if __name__=="__main__":
    main()
        
