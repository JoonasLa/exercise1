"""
Simple Molecular Dynamics code for course FYS-4096 Computational Physics

Problem 1:
- Make the code to work and solve H2 using the Morse potential.
- Modify and comment especially at parts where it reads "# ADD"
- Follow instructions on ex12.pdf

Problems 2:
- Add observables: temperature, distance, and heat capacity.
- Follow instructions on ex12.pdf

Problem 3:
- Add Lennard-Jones capability etc.
- Follow instructions on ex12.pdf

Problem 4:
- Temperature dependent data and analysis
- Follow instructions on ex12.pdf

"""




from numpy import *
from matplotlib.pyplot import *

class Atom:
    """
    Class for storing info on system components. i.e. atom mass, location,
    interaction coefficients etc.
    """
    def __init__(self,index,mass,dt,dims):
        self.index = index
        self.mass = mass
        self.dt = dt
        self.dims = dims
        self.LJ_epsilon = None
        self.LJ_sigma = None
        self.R = None
        self.v = None
        self.force = None

    def set_LJ_parameters(self,LJ_epsilon,LJ_sigma):
        self.LJ_epsilon = LJ_epsilon
        self.LJ_sigma = LJ_sigma

    def set_position(self,coordinates):
        self.R = coordinates
        
    def set_velocity(self,velocity):
        self.v = velocity

    def set_force(self,force):
        self.force = force


class Observables:
    """
    Class for storing system observables.
    """
    def __init__(self):
        self.E_kin = []
        self.E_pot = []
        self.distance = []
        self.Temperature = []
        self.heat_capacity = []


def calculate_energetics(atoms):
    N = len(atoms)
    V = 0.0
    E_kin = 0.0
    # Using classical equations:
    # Kinetic:
    for i in range(N):
        E_kin += 1/2*atoms[i].mass*linalg.norm(atoms[i].v)**2

    #Potential:
    for i in range(0,N-1):
        for j in range(i+1,N):
            V += pair_potential(atoms[i], atoms[j])

    return E_kin, V


def calculate_force(atoms):
    """
    Calculates the forces between all system particles.
    :param atoms: list of system components as atom objects
    :return: An array of force vectors (a column is the force on a specific atom)
    """

    # ADD comments, e.g., what are the elements of the return function F
    # 
    # 
    N = len(atoms)
    ij_map = zeros((N,N),dtype=int)
    Fs = []
    ind = 0

    # Calculate force between a pair of 2 atoms:
    for i in range(0,N-1):
        for j in range(i+1,N):
            Fs.append(pair_force(atoms[i],atoms[j]))
            # Store the index in which the pair force is stored in Fs:
            ij_map[i,j] = ind
            ij_map[j,i] = ind
            ind += 1

    # Sum forces to specific atoms in to an array of force vectors:
    F = []
    for i in range(N):
        f = zeros(shape=shape(atoms[i].R))
        for j in range(N):
            ind = ij_map[i,j]
            if i<j:
                f += Fs[ind]
            elif i>j:
                f -= Fs[ind]
        F.append(f)
    F = array(F)
    return F


def pair_force(atom1,atom2):
    return Morse_force(atom1,atom2)


def pair_potential(atom1,atom2):
    return Morse_potential(atom1,atom2)


def Morse_potential(atom1,atom2):
    # H2 parameters given here
    De = 0.1745
    re = 1.40
    a = 1.0282
    r = sqrt(sum((atom1.R - atom2.R)**2))
    # Calculate pair potential using Morse potential with above values:
    V = De*(1 - exp(-a*(r-re)))**2 - De
    return V


def Morse_force(atom1,atom2):
    # H2 parameters
    De = 0.1745
    re = 1.40
    a = 1.0282
    R = atom1.R - atom2.R
    r = sqrt(sum((atom1.R - atom2.R) ** 2))

    # Calculate pair force using the force calculated from the
    # Morse potential with above values:
    F = -R/r*2*a*De * (exp(-a * (r - re)) - exp(-2*a * (r - re)))
    return F


def lennard_jones_potential(atom1,atom2):
    """ Lennard-Jones potential using values stored in atom objects. """
    epsilon = sqrt(atom1.LJ_epsilon*atom2.LJ_epsilon)
    sigma = (atom1.LJ_sigma+atom2.LJ_sigma)/2
    # Interatomic distance:
    r = linalg.norm(atom1.R - atom2.R)

    V = 4 * epsilon * ((sigma / r) ** 12 - (sigma / r) ** 6)
    return V


def lennard_jones_force(atom1,atom2):
    """ Conservative force calculated using Lennard-Jones potential. """
    epsilon = sqrt(atom1.LJ_epsilon*atom2.LJ_epsilon)
    sigma = (atom1.LJ_sigma+atom2.LJ_sigma)/2
    r = linalg.norm(atom1.R - atom2.R)  # Distance between atoms
    R = atom1.R - atom2.R  # Direction and distance between atoms

    # Calculate pair force using the force calculated from the
    # Lennard-Jones potential with above values:
    F = 24*epsilon*(2* sigma**12 / r**13 - sigma**6 / r**7)*R/r

    return F


def velocity_verlet_update(atoms):
    """
    Update the positions, forces and velocities after a time step dt.
    :param atoms: Input list of atom objects
    :return: updated atom objects
    """
    dt = atoms[0].dt
    dt2 = dt**2
    # Updating:
    for i in range(len(atoms)):
        # New position for all particles:
        atoms[i].R += dt*atoms[i].v + dt2/(2*atoms[i].mass)*\
                        atoms[i].force

    Fnew = calculate_force(atoms)  # New forces for these positions
    for i in range(len(atoms)):
        # New velocities for all particles:
        atoms[i].v += dt/(2*atoms[i].mass)*(Fnew[i] + atoms[i].force)
        atoms[i].force = Fnew[i]  # update force
    return atoms


def initialize_positions(atoms):
    # diatomic case
    atoms[0].set_position(array([-0.8,0.0,0.0]))
    atoms[1].set_position(array([0.7,0.0,0.0]))


def initialize_velocities(atoms, T):
    dims = atoms[0].dims
    N = len(atoms)
    kB=3.16e-6 # in hartree/Kelvin

    # Set a maximum velocity using classical kinetic energy
    # and the given Temperature to set the system temperature:
    for i in range(N):
        v_max = sqrt(dims*N/atoms[i].mass*kB*T/2)
        atoms[i].set_velocity(array([1.0,0.0,0.0])*v_max)
    atoms[1].v = -1.0*atoms[0].v


def initialize_force(atoms):
    F=calculate_force(atoms)
    for i in range(len(atoms)):
        atoms[i].set_force(F[i])


def Temperature(E_k, atoms):
    # Boltzmann constant in Hartree/Kelvin
    kB = 3.16e-6

    # Temperature as in slides:
    T = 2*E_k / (atoms[0].dims*len(atoms)*kB)
    return T


def calculate_distance(atoms):
    """
    Calculates the average distance between all the atoms
    """
    d = 0
    pairs = 0
    for i in range(len(atoms)):
        for j in range(i+1, len(atoms)):
            d += linalg.norm(atoms[i].R - atoms[j].R)
            pairs += 1
    return d/pairs


def calculate_observables(atoms,observables):
    E_k, E_p = calculate_energetics(atoms)
    T = Temperature(E_k, atoms)
    d = calculate_distance(atoms)


    observables.E_kin.append(E_k)
    observables.E_pot.append(E_p)
    observables.Temperature.append(T)
    observables.distance.append(d)

    # Heat capacity:
    kB = 3.16e-6
    c = (mean(array((observables.E_pot + observables.E_kin))**2) -
         mean(array(observables.E_pot + observables.E_kin))) / \
        (kB*T**2)
    observables.heat_capacity.append(c)

    return observables


def main():
    N_atoms = 2
    dims = 3
    dt = 0.1
    mass = 1860.0

    T_set = linspace(100, 4000, 10)
    Tobs = zeros(T_set.shape)
    Tstd = zeros(T_set.shape)
    E = zeros(T_set.shape)
    C = zeros(T_set.shape)
    D = zeros(T_set.shape)
    for k in range(len(T_set)):
        # Initialize atoms, with used intrinsic and potential parameters
        atoms = []
        for i in range(N_atoms):
            atoms.append(Atom(i, mass, dt, dims))
            atoms[i].set_LJ_parameters(0.1745, 1.25)
        # Initialize observables
        observables = Observables()

        # Initialize positions, velocities, and forces
        initialize_positions(atoms)
        initialize_velocities(atoms, T_set[k])
        initialize_force(atoms)

        E_k, E_p = calculate_energetics(atoms)
        print('\nT', Temperature(E_k, atoms))

        steps = 100000
        for i in range(steps):
            atoms = velocity_verlet_update(atoms)
            if ( i % 10 == 0):
                observables = calculate_observables(atoms,observables)

        # Print energies
        E_kin = array(observables.E_kin)
        E_pot = array(observables.E_pot)
        T = array(observables.Temperature)
        d = array(observables.distance)
        c = array(observables.heat_capacity)

        print('\nE_kin',mean(E_kin))
        print('E_pot',mean(E_pot))
        print('E_tot',mean(E_kin)+mean(E_pot))
        print('Temperature {:.3f} +/- {:.3f}'.format(mean(T), std(T, ddof=1)))
        print('Distance', mean(d))
        print('Heat capacity', c[-1])

        Tobs[k] = mean(T)
        Tstd[k] = std(T, ddof=1)
        E[k] = mean(E_kin)+mean(E_pot)
        D[k] = mean(d)
        C[k] = c[-1]

    # Plot observables:
    figure()
    subplot(221)
    plot(T_set, E, '-b', label='With set temperature values')
    plot(Tobs, E, '-k', label='With calculated temperature values')
    ylabel('Total energy (Hartree)')
    xlabel('Temperature (K)')
    legend()
    grid(True)

    subplot(222)
    plot(T_set, D, '-b', label='With set temperature values')
    plot(Tobs, D, '-k', label='With calculated temperature values')
    ylabel('Interatomic distance (Ä)')
    xlabel('Temperature (K)')
    legend()
    grid(True)

    subplot(223)
    plot(T_set, C, '-b', label='With set temperature values')
    plot(Tobs, C, '-k', label='With calculated temperature values')
    ylabel('Heat Capacity (Hartree/K)')
    xlabel('Temperature (K)')
    legend(loc='upper left')
    grid(True)

    subplot(224)
    plot(T_set, Tobs, '-b', label='Mean')
    plot(T_set, Tobs + Tstd, '-r', label='Mean +/- standard deviation')
    plot(T_set, Tobs - Tstd, '-r')
    ylabel('Observed Temperature (K)')
    xlabel('Temperature (K)')
    grid(True)
    legend()
    show()

if __name__=="__main__":
    main()
        
