import numpy as np
import matplotlib.pyplot as plt


def main():
    epsilon = 0.1745
    De = 0.1745
    sigma = 1.25
    re = 1.40
    a = 1.0282

    r1 = np.linspace(1, 5, 300)
    r2 = np.linspace(0.01, 5, 300)

    VLJ = 4*epsilon*((sigma/r1)**12 - (sigma/r1)**6)
    VM = De * (1 - np.exp(-a * (r2 - re))) ** 2 - De

    plt.figure()
    plt.subplot(121)
    plt.plot(r1, VLJ, label='Lennard-Jones')
    plt.xlabel('Interatomic distance (Ä)')
    plt.ylabel('Potential (Hartree)')
    plt.legend()
    plt.grid(True)
    plt.subplot(122)
    plt.plot(r2, VM, label='Morse')
    plt.xlabel('Interatomic distance (Ä)')
    plt.ylabel('Potential (Hartree)')
    plt.legend()
    plt.grid(True)
    plt.show()

if __name__ == '__main__':
    main()