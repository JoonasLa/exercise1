"""
Simple Molecular Dynamics code for course FYS-4096 Computational Physics

Problem 1:
- Make the code to work and solve H2 using the Morse potential.
- Modify and comment especially at parts where it reads "# ADD"
- Follow instructions on ex12.pdf

Problems 2:
- Add observables: temperature, distance, and heat capacity.
- Follow instructions on ex12.pdf

Problem 3:
- Add Lennard-Jones capability etc.
- Follow instructions on ex12.pdf

Problem 4:
- Temperature dependent data and analysis
- Follow instructions on ex12.pdf

"""




from numpy import *
from matplotlib.pyplot import *
import numpy as np

class Atom:
    def __init__(self,index,mass,dt,dims):
        self.index = index
        self.mass = mass
        self.dt = dt
        self.dims = dims
        self.LJ_epsilon = None
        self.LJ_sigma = None
        self.R = None
        self.v = None
        self.force = None

    def set_LJ_parameters(self,LJ_epsilon,LJ_sigma):
        self.LJ_epsilon = LJ_epsilon
        self.LJ_sigma = LJ_sigma

    def set_position(self,coordinates):
        self.R = coordinates
        
    def set_velocity(self,velocity):
        self.v = velocity

    def set_force(self,force):
        self.force = force

class Observables:
    def __init__(self):
        self.E_kin = []
        self.E_pot = []
        self.distance = []
        self.Temperature = []

def calculate_energetics(atoms):
    N = len(atoms)
    V = 0.0
    E_kin = 0.0
    # ADD calculation of kineti and potential energy
    for i in range(N):
        atom = atoms[i]
        E_kin += atom.mass * np.dot(atom.v, atom.v)/2

    for i in range(N-1):
        atom1 = atoms[i]
        for j in range(i+1, N):
            atom2 = atoms[j]
            V += pair_potential(atom1, atom2)
    return E_kin, V

def calculate_force(atoms):
    # ADD comments, e.g., what are the elements of the return function F

    N = len(atoms)
    ij_map = zeros((N,N),dtype=int)
    Fs = []
    ind = 0
    for i in range(0,N-1):
        for j in range(i+1,N):
            # Calculate force between ith and jth atom
            Fs.append(pair_force(atoms[i],atoms[j]))
            ij_map[i,j] = ind
            ij_map[j,i] = ind
            ind += 1
    F = []
    for i in range(N):
        f = zeros(shape=shape(atoms[i].R))
        for j in range(N):
            ind = ij_map[i,j]
            if i<j:
                f += Fs[ind]
            elif i>j:
                f -= Fs[ind]
        F.append(f)
    F = array(F)
    return F

def pair_force(atom1,atom2):
    return Morse_force(atom1,atom2)

def pair_potential(atom1,atom2):
    return Morse_potential(atom1,atom2)

def Morse_potential(atom1,atom2):
    # H2 parameters given here
    De = 0.1745
    re = 1.40
    a = 1.0282
    # ADD comments and calculation of Morse potential
    # Calculate distance between atoms
    r = np.linalg.norm(atom1.R-atom2.R)
    # Morse potential is calculated with equation
    Vmorse = De*(1-np.exp(-a*(r-re)))**2 - De
    return Vmorse

def Morse_force(atom1,atom2):
    # H2 parameters
    De = 0.1745
    re = 1.40
    a = 1.0282
    # ADD comments and calculation of Morse force
    r = np.linalg.norm(atom1.R-atom2.R)
    r_hat = (atom1.R-atom2.R)/r
    # Morse force is simply -dU/dr*r_hat
    F = -2*De*(1-np.exp(-a*(r-re)))*np.exp(-a*(r-re))*a*r_hat

    return F

def lennard_jones_potential(atom1,atom2):
    epsilon = sqrt(atom1.LJ_epsilon*atom2.LJ_epsilon)
    sigma = (atom1.LJ_sigma+atom2.LJ_sigma)/2
    # ADD --> in problem 3: comments and calculation of LJ potential
    r = np.linalg.norm(atom1.R-atom2.R)
    V = 4*epsilon*((sigma/r)**12-(sigma/r)**6)
    return V

def lennard_jones_force(atom1,atom2):

    epsilon = sqrt(atom1.LJ_epsilon*atom2.LJ_epsilon)
    sigma = (atom1.LJ_sigma+atom2.LJ_sigma)/2
    # ADD --> in problem 3: comments and calculation of LJ force
    r = np.linalg.norm(atom1.R-atom2.R)
    r_hat = (atom1.R-atom2.R)/r
    F = -4*epsilon*(-12*sigma**12/r**13+6*sigma**6/r**7)*r_hat

    return F

def velocity_verlet_update(atoms):
    # ADD comments and integration algoritm according to function name
    dt = atoms[0].dt
    dt2 = dt**2
    for i in range(len(atoms)):
        atoms[i].R += dt*atoms[i].v + dt2*atoms[i].force/(2*atoms[i].mass)
    Fnew = calculate_force(atoms)
    for i in range(len(atoms)):
        atoms[i].v += dt*(Fnew[i] + atoms[i].force) / (2*atoms[i].mass)
        atoms[i].force = Fnew[i] # update force
    return atoms
    
def initialize_positions(atoms):
    # diatomic case
    atoms[0].set_position(array([-0.8,0.0,0.0]))
    atoms[1].set_position(array([0.7,0.0,0.0]))

def initialize_velocities(atoms, T=10.0):
    # ADD --> in problem 4: comment on what is v_max
    # v_max is calculated from 1/2 m*v^2 = 3/2 kB*T
    # diatomic case
    dims = atoms[0].dims
    kB=3.16e-6 # in hartree/Kelvin
    for i in range(len(atoms)):
        v_max = sqrt(3.0/atoms[i].mass*kB*T)
        atoms[i].set_velocity(array([1.0,0.0,0.0])*v_max)
    atoms[1].v = -1.0*atoms[0].v

def initialize_force(atoms):
    F=calculate_force(atoms)
    for i in range(len(atoms)):
        atoms[i].set_force(F[i])

def Temperature(E_k, atoms):
    # Boltzmann constant in Hartree/Kelvin
    kB = 3.16e-6
    # ADD calculation of temperature in problem 2
    dims = atoms[0].dims
    N = len(atoms)
    T = E_k*2/(dims*N*kB)
    return T

def distance(atoms):
    return np.linalg.norm(atoms[0].R-atoms[1].R)

def calculate_observables(atoms,observables):
    E_k, E_p = calculate_energetics(atoms)
    observables.E_kin.append(E_k)
    observables.E_pot.append(E_p)
    # ADD calculation of observables in problem 2
    observables.Temperature.append(Temperature(E_k, atoms))
    observables.distance.append(distance(atoms))

    return observables

def plot_morse_lj():
    r = linspace(1.2, 1.8, 100)
    De = 0.1745
    sigma = 1.25
    re = 1.40
    a = 1.0282

    morseV = De*(1-np.exp(-a*(r-re)))**2-De
    ljV = 4*De*((sigma/r)**12-(sigma/r)**6)

    figure()
    plot(r, morseV, label='Morse')
    plot(r, ljV, label='Lennard-Jones')
    legend()
    title('Potential')

    morseF = -2*De*a*(1-np.exp(-a*(r-re)))*np.exp(-a*(r-re))
    ljF = -4*De*(-12*(sigma/r)**12/r+6*(sigma/r)**6/r)

    figure()
    plot(r, morseF, label='Morse')
    plot(r, ljF, label='Lennard-Jones')
    legend()
    title('Force')

    show()

def main():
    N_atoms = 2
    dims = 3
    dt = 0.1
    mass = 1860.0

    kB = 3.16e-6

    # Initialize atoms
    atoms = []    
    for i in range(N_atoms):
        atoms.append(Atom(i,mass,dt,dims))
        atoms[i].set_LJ_parameters(0.1745, 1.40*2**(-1/6))
    # Initialize observables
    observables = Observables()

    # Initialize positions, velocities, and forces
    initialize_positions(atoms)
    initialize_velocities(atoms)
    initialize_force(atoms)

    E_k, E_p = calculate_energetics(atoms)
    print('T', Temperature(E_k,atoms))

    for i in range(100000):
        atoms = velocity_verlet_update(atoms)
        if ( i % 10 == 0):
            observables = calculate_observables(atoms,observables)            

    # Print energies
    E_kin = array(observables.E_kin)
    E_pot = array(observables.E_pot)
    E_tot = E_kin+E_pot
    temp = array(observables.Temperature)
    dist = array(observables.distance)
    Cv = (var(E_tot))/(kB*mean(temp)**2)
    print('E_kin',mean(E_kin))
    print('E_pot',mean(E_pot))
    print('E_tot',mean(E_kin)+mean(E_pot))
    print('Temperature', mean(temp))
    print('Distance', mean(dist))
    print('Heat capacity', Cv)
    print('Standard deviation of temperature', std(temp))
    figure()
    plot(E_kin+E_pot)
    title('Total energy')
    figure()
    plot(temp)
    title('Temperature')
    figure()
    plot(dist)
    title('Distance')
    show()

def problem4():
    TT = linspace(10, 7000, 10)
    E_tots = []
    distances = []
    temperatures = []
    deviation = []

    for T in TT:
        N_atoms = 2
        dims = 3
        dt = 0.1
        mass = 1860.0

        kB = 3.16e-6

        # Initialize atoms
        atoms = []
        for i in range(N_atoms):
            atoms.append(Atom(i, mass, dt, dims))
            atoms[i].set_LJ_parameters(0.1745, 1.40 * 2 ** (-1 / 6))
        # Initialize observables
        observables = Observables()

        # Initialize positions, velocities, and forces
        initialize_positions(atoms)
        initialize_velocities(atoms, T)
        initialize_force(atoms)

        E_k, E_p = calculate_energetics(atoms)
        print('T', Temperature(E_k, atoms))

        for i in range(100000):
            atoms = velocity_verlet_update(atoms)
            if (i % 10 == 0):
                observables = calculate_observables(atoms, observables)

        E_kin = array(observables.E_kin)
        E_pot = array(observables.E_pot)
        E_tot = E_kin + E_pot
        temp = array(observables.Temperature)
        dist = array(observables.distance)
        E_tots.append(mean(E_tot))
        temperatures.append(mean(temp))
        distances.append(mean(dist))
        deviation.append(std(temperatures))

    figure()
    plot(temperatures, E_tots)
    ylabel('Energy')
    xlabel('Temperature')

    figure()
    plot(temperatures, distances)
    ylabel('Distance')
    xlabel('Temperature')

    figure()
    dE = diff(E_tots)
    dT = diff(temperatures)
    Cv = dE/dT
    plot(temperatures[0:-1], Cv)
    xlabel('Temperature')
    ylabel('Specific heat')

    figure()
    errorbar(temperatures, temperatures, yerr=deviation)
    title('Standard deviation of temperature')
    xlabel('Temperature')
    ylabel('Temperature')
    axis('equal')

    show()

if __name__=="__main__":
    plot_morse_lj()
    main()
    problem4()
        
"""
VALUES USING MORSE POTENTIAL
E_kin 0.000877447599544743
E_pot -0.17361663274447348
E_tot -0.17273918514492873
Temperature 92.55776366505728
Distance 1.407036871517909
Heat capacity 1.4255781565944494e-17
Standard deviation of temperature 65.6621119679301

VALUES USING LENNARD-JONES POTENTIAL
E_kin 0.009680041143969316
E_pot -0.16403508153348606
E_tot -0.15435504038951675
Temperature 1021.1013864946536
Distance 1.4254831856093777
Heat capacity 5.341152638011715e-15
Standard deviation of temperature 750.5029679291486
"""
