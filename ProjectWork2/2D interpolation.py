"""
Warm up problem for project work 2
FYS-4096 Computational Physics
Author: Joonas Latukka
Modified: 28.4.2019
"""

from numpy import *
import numpy as np
import h5py
import matplotlib.pyplot as plt

# USING SPLINE-class that was created in Exercise 2
def p1(t):
    return (1+2*t)*(t-1)**2

def p2(t):
    return t**2*(3-2*t)

def q1(t):
    return t*(t-1)**2

def q2(t):
    return t**2*(t-1)


def init_1d_spline(x, f, h):
    # now using complete boundary conditions
    # with forward/backward derivative
    # - natural boundary conditions commented
    a = zeros((len(x),))
    b = zeros((len(x),))
    c = zeros((len(x),))
    d = zeros((len(x),))
    fx = zeros((len(x),))

    # a[0]=1.0 # not needed
    b[0] = 1.0

    # natural boundary conditions
    # c[0]=0.5
    # d[0]=1.5*(f[1]-f[0])/(x[1]-x[0])

    # complete boundary conditions
    c[0] = 0.0
    d[0] = (f[1] - f[0]) / (x[1] - x[0])

    for i in range(1, len(x) - 1):
        d[i] = 6.0 * (h[i] / h[i - 1] - h[i - 1] / h[i]) * f[i] - 6.0 * h[i] / h[i - 1] * f[i - 1] + 6.0 * h[i - 1] / h[
            i] * f[i + 1]
        a[i] = 2.0 * h[i]
        b[i] = 4.0 * (h[i] + h[i - 1])
        c[i] = 2.0 * h[i - 1]
        # end for

    b[-1] = 1.0
    # c[-1]=1.0 # not needed

    # natural boundary conditions
    # a[-1]=0.5
    # d[-1]=1.5*(f[-1]-f[-2])/(x[-1]-x[-2])

    # complete boundary conditions
    a[-1] = 0.0
    d[-1] = (f[-1] - f[-2]) / (x[-1] - x[-2])

    # solve tridiagonal eq. A*f=d
    c[0] = c[0] / b[0]
    d[0] = d[0] / b[0]
    for i in range(1, len(x) - 1):
        temp = b[i] - c[i - 1] * a[i]
        c[i] = c[i] / temp
        d[i] = (d[i] - d[i - 1] * a[i]) / temp
    # end for

    fx[-1] = d[-1]
    for i in range(len(x) - 2, -1, -1):
        fx[i] = d[i] - c[i] * fx[i + 1]
    # end for

    return fx


# end function init_1d_spline

class spline(object):

    def __init__(self, *args, **kwargs):
        self.dims = kwargs['dims']
        if (self.dims == 2):
            self.x = kwargs['x']
            self.y = kwargs['y']
            self.f = kwargs['f']
            self.hx = np.diff(self.x)
            self.hy = np.diff(self.y)
            self.fx = np.zeros(np.shape(self.f))
            self.fy = np.zeros(np.shape(self.f))
            self.fxy = np.zeros(np.shape(self.f))
            for i in range(max([len(self.x), len(self.y)])):
                if (i < len(self.y)):
                    self.fx[:, i] = init_1d_spline(self.x, self.f[:, i], self.hx)
                if (i < len(self.x)):
                    self.fy[i, :] = init_1d_spline(self.y, self.f[i, :], self.hy)
            # end for
            for i in range(len(self.y)):
                self.fxy[:, i] = init_1d_spline(self.x, self.fy[:, i], self.hx)
            # end for
        else:
            print('Either dims is missing or specific dims is not available')
        # end if

    def eval2d(self, x, y):
        if isscalar(x):
            x = array([x])
        if isscalar(y):
            y = array([y])
        Nx = len(self.x) - 1
        Ny = len(self.y) - 1
        f = zeros((len(x), len(y)))
        A = zeros((4, 4))
        ii = 0
        for valx in x:
            i = floor(where(self.x <= valx)[0][-1]).astype(int)
            if (i == Nx):
                i -= 1
            jj = 0
            for valy in y:
                j = floor(where(self.y <= valy)[0][-1]).astype(int)
                if (j == Ny):
                    j -= 1
                u = (valx - self.x[i]) / self.hx[i]
                v = (valy - self.y[j]) / self.hy[j]
                pu = array([p1(u), p2(u), self.hx[i] * q1(u), self.hx[i] * q2(u)])
                pv = array([p1(v), p2(v), self.hy[j] * q1(v), self.hy[j] * q2(v)])
                A[0, :] = array([self.f[i, j], self.f[i, j + 1], self.fy[i, j], self.fy[i, j + 1]])
                A[1, :] = array([self.f[i + 1, j], self.f[i + 1, j + 1], self.fy[i + 1, j], self.fy[i + 1, j + 1]])
                A[2, :] = array([self.fx[i, j], self.fx[i, j + 1], self.fxy[i, j], self.fxy[i, j + 1]])
                A[3, :] = array([self.fx[i + 1, j], self.fx[i + 1, j + 1], self.fxy[i + 1, j], self.fxy[i + 1, j + 1]])

                f[ii, jj] = dot(pu, dot(A, pv))
                jj += 1
            ii += 1
        return f
    # end eval2d

def read_hdf5_file(fname):
    """
    Reads hdf5 file and extracts its contents
    :param fname: filename for hdf5 file
    :return: data and x and y grids
    """
    f = h5py.File(fname, "r")
    data = np.array(f["data"])
    x_grid = np.array(f["x_grid"])
    y_grid = np.array(f["y_grid"])

    return data, x_grid, y_grid


def create_hdf5_file(fname, data, x_grid, y_grid):
    """
    Creates hdf5 file
    :param fname: filename for hdf5 file
    :param data: data
    :param x_grid: x grid for data
    :param y_grid: y grid for data
    :return: None
    """

    f = h5py.File(fname, "w")
    set1 = f.create_dataset("data", data=data, dtype='f')
    set1.attrs["info"] = 'Interpolated values'

    set2 = f.create_dataset('x_grid', data=x_grid, dtype='f')
    set2.attrs["info"] = 'x grid'

    set3 = f.create_dataset('y_grid', data=y_grid, dtype='f')
    set3.attrs["info"] = 'y grid'
    f.close()

def interpolation():
    """
    Reads data and interpolates values in given range. Plots the results
    :return: None
    """
    data, xgrid, ygrid = read_hdf5_file("warm_up_data.h5")
    X, Y = np.meshgrid(xgrid, ygrid)

    # Create spline evaluator
    f = spline(x=xgrid, y=ygrid, f=data, dims=2)

    # New points are evaluated between (-1.5, -1.5) and (1.5, 1.5)
    x2 = np.linspace(-1.5, 1.5, 100)
    y2 = np.linspace(-1.5, 1.5, 100)
    newdata = f.eval2d(x2, y2)

    plt.figure()
    # Plot interpolated values
    ax1 = plt.subplot(211)
    plt.plot(x2, np.diag(newdata))
    plt.ylabel('z')
    plt.xlabel('x')
    plt.title("Interpolated values")

    # Plot original values
    ax2 = plt.subplot(212)
    plt.contourf(ygrid, xgrid, data)
    plt.plot(x2, y2, 'k-', label='Evaluated curve')
    plt.legend()
    plt.xlabel('x')
    plt.ylabel('y')
    plt.title('Contour of original data')
    plt.subplots_adjust(hspace=0.4)
    plt.show()

    create_hdf5_file('warm_up_interpolated.h5', newdata, x2, y2)
    print("Value at (0.1, 0.1): %1.5f" % (f.eval2d(0.1, 0.1)[0, 0],) )


def main():
    interpolation()

if __name__ == "__main__":
    main()