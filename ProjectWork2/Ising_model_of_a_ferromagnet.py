"""
Problem 5: 2D Ising model of a ferromagnet for project work 2
FYS-4096 Computational Physics
Author: Joonas Latukka
Modified: 29.4.2019

This code is based heavily on code used in Exercise 11, spin_mc_simple.py
"""


from numpy import *
import matplotlib.pyplot as plt
from scipy.special import erf
import numpy as np
import numba

spec = [
    ('spin', numba.float64),
    ('nearest_neighbors', numba.int64[:]),
    ('Js', numba.float64[:]),
    ('sys_dim', numba.int64),
    ('coords', numba.int64[:])
]

@numba.jitclass(spec)
class Walker:
    """
    Walker-class describing properties for each atom in grid
    """
    def __init__(self, spin: float, nn: np.ndarray, Js: np.ndarray, dim: int, coords: np.ndarray):
        self.spin = spin
        self.nearest_neighbors = nn
        self.Js = Js
        self.sys_dim = dim
        self.coords = coords

    def w_copy(self):
        return Walker(self.spin.copy(),
                      self.nearest_neighbors.copy(),
                      self.Js.copy(),
                      self.sys_dim,
                      self.coords.copy()
                )
    
@numba.njit
def Energy(Walkers):
    """
    Calculates the total energy in grid
    :param Walkers: Listing of walkers
    :return: Total energy
    """
    E = 0.0
    for walker in Walkers:
        E += site_Energy(Walkers, walker)
    return E/2

@numba.njit
def site_Energy(Walkers, walker):
    """
    Calculates the energy for each atom in a grid
    :param Walkers: Listing of walkers
    :param walker: walker which for energy is calculated
    :return: energy of walker
    """
    E = 0.0
    for k in range(len(walker.nearest_neighbors)):
        j = walker.nearest_neighbors[k]
        E += -walker.Js[k]*walker.spin*Walkers[j].spin
    return E

@numba.njit
def ising(Nblocks,Niters,Walkers,beta):
    """
    Ising-algorithm to calculate spin in given temperature
    :param Nblocks: How many times loop is iterated through
    :param Niters: How many times algorithm tries to update atoms in each block
    :param Walkers: Listing of walkers
    :param beta: variable based on temperature, 1/T
    :return: Walkers, energies, heat capacity, magnetization and magnetic susceptibility
    """
    M = len(Walkers)
    Eb = zeros((Nblocks,))
    Mb = zeros((Nblocks,))
    Cvb = zeros((Nblocks,))
    khib = zeros((Nblocks,))
    Accept=zeros((Nblocks,))
    AccCount=zeros((Nblocks,))

    obs_interval = 5
    for i in range(Nblocks):
        EbCount = 0
        for j in range(Niters):
            # Choose random walker from listing and save its spin and energy to variables
            site = int(random.rand()*M)
            s_old = 1.0*Walkers[site].spin
            E_old = site_Energy(Walkers,Walkers[site])

            s_new = -1.0*s_old # Spin is flipped to opposite direction
            Walkers[site].spin = 1.0*s_new
            E_new = site_Energy(Walkers,Walkers[site])

            # Ising algorithm
            q = np.exp(-beta*(E_new-E_old))
            p = np.random.rand()
            AccCount[i] += 1

            # Choose if spin is updated or reversed back to what it was before
            if p < q:
                Walkers[site].spin = s_new
                Accept[i] += 1
            else:
                Walkers[site].spin = s_old

            # Once in a while calculate obserables from current configuration
            if j % obs_interval == 0:
                E_tot = Energy(Walkers)/M # energy per spin
                Eb[i] += E_tot
                EbCount += 1
                M_tot = calc_magnetization(Walkers)/M
                Cv_tot = calc_heat_capacity(Walkers, 1.0/beta)
                khi_tot = calc_magn_susceptibility(Walkers, 1.0/beta)
                Mb[i] += M_tot
                Cvb[i] += Cv_tot
                khib[i] += khi_tot

        # Calculate averages of each observable
        Mb[i] /= EbCount
        Cvb[i] /= EbCount
        Eb[i] /= EbCount
        khib[i] /= EbCount
        Accept[i] /= AccCount[i]
    return Walkers, Eb, Accept, Cvb, Mb, khib

@numba.njit
def calc_heat_capacity(Walkers, T):
    """
    Calculate the average heat capacity of single atom
    :param Walkers: listing of walkers
    :param T: Temperature
    :return: Heat capacity
    """
    M = len(Walkers)
    # Calculate average of energies squared
    E2 = 0.0
    for walker in Walkers:
        E2 += (site_Energy(Walkers, walker)/2)**2
    E2 /= M
    # Average energy E
    E = Energy(Walkers)/M

    # Heat capacity calculated with eq 21 in lecture slides 11
    Cv = (E2-E**2)/(T**2)
    return Cv

@numba.njit
def calc_magnetization(Walkers):
    """
    Calculate magnetization in grid
    :param Walkers: listing of walkers
    :return: Total magnetization
    """
    M = 0
    # Sum all spins together
    for walker in Walkers:
        M += walker.spin
    return M

@numba.njit
def calc_magn_susceptibility(Walkers, T):
    """
    Calculate average magnetic susceptibility in grid
    :param Walkers: listing of walkers
    :param T: Temperature
    :return: magnetic susceptibility
    """
    M2 = 0
    # Equation 22 in lecture slides 11
    for walker in Walkers:
        M2 += walker.spin ** 2
    M2 /= len(Walkers)
    M = calc_magnetization(Walkers)/len(Walkers)

    khi = (M2-M**2)/T
    return khi

@numba.njit
def createWalkers(grid_side: int, num_of_nearest_neigbors: int):
    """
    Creates walkers that are being used in simulations
    :param grid_side: grid side length
    :param num_of_nearest_neigbors: 1 if only nearest neighbours, 2 if also next nearest neighbours
    :return: listing of walkers
    """
    Walkers = []
    dim = 2

    # Mapping helps to keep track of walkers next to some walker
    mapping = np.zeros((grid_side, grid_side), dtype=np.int_)
    ii = 0
    for i in range(grid_side):
        for j in range(grid_side):
            mapping[i, j]=ii
            ii += 1

    for i in range(grid_side):
        for j in range(grid_side):
            j1=mapping[i, (j-1) % grid_side]
            j2=mapping[i, (j+1) % grid_side]
            j3=mapping[(i-1) % grid_side, j]
            j4=mapping[(i+1) % grid_side, j]
            if num_of_nearest_neigbors == 1:
                # In case of only nearest neighbours are added, this is all that is for walker
                Walkers.append(Walker(0.5,
                                      np.array([j1, j2, j3, j4]),
                                      np.array([4.0, 4.0, 4.0, 4.0]),
                                      dim,
                                      np.array([i, j])
                                      )
                               )
                continue
            # Now also next-nearest neighbors are added
            j5 = mapping[(i-1) % grid_side, (j-1)%grid_side]
            j6 = mapping[(i + 1) % grid_side, (j - 1) % grid_side]
            j7 = mapping[(i - 1) % grid_side, (j + 1) % grid_side]
            j8 = mapping[(i + 1) % grid_side, (j + 1) % grid_side]
            Walkers.append(Walker(0.5,
                                  np.array([j1, j2, j3, j4, j5, j6, j7, j8]),
                                  np.array([4.0, 4.0, 4.0, 4.0, 1.0, 1.0, 1.0, 1.0]),
                                  dim,
                                  np.array([i, j])
                                  )
                           )
    return Walkers

def calc_statistics(Walkers, T_list: np.ndarray):
    """
    Calculate different observables in given temperatures
    :param Walkers: listing of walkers
    :param T_list: listing of temperatures that are used
    :return: Temperatures and corresponding energies, heat capacities, magnetizations and magnetic susceptibilities
    """
    Nblocks = 200
    Niters = 1000
    eq = 20  # equilibration "time"

    E_list = np.zeros_like(T_list)
    Cv_list = np.zeros_like(T_list)
    M_list = np.zeros_like(T_list)
    X_list = np.zeros_like(T_list)
    for ind, T in enumerate(T_list):
        print('T=', T)
        beta = 1.0 / T
        Walkers, Eb, Acc, Cv, Mb, khib = ising(Nblocks, Niters, Walkers, beta)
        Eb = Eb[eq:]
        Cv = Cv[eq:]
        Mb = Mb[eq:]
        khib = khib[eq:]
        E_list[ind] = np.mean(Eb)
        Cv_list[ind] = np.mean(Cv)
        M_list[ind] = np.mean(Mb)
        X_list[ind] = np.mean(khib)
    return T_list, E_list, Cv_list, M_list, X_list

def plot_statistics(T_list, E_list, Cv_list, M_list, X_list):
    """
    function used to plot different statistics
    :param T_list: Temperatures
    :param E_list: Energies
    :param Cv_list: Heat capacities
    :param M_list: Magnetizations
    :param X_list: Magnetic susceptibilities
    :return: None
    """
    plt.figure()
    ax1 = plt.subplot(221)
    plt.plot(T_list, E_list)
    plt.title('Energy of the system')
    plt.xlabel('T (K)')
    plt.ylabel(r'Energy ($k_b$)')
    plt.xticks(np.arange(0, 6, step=1))

    ax2 = plt.subplot(222)
    plt.plot(T_list, Cv_list)
    plt.title('Heat capacity')
    plt.xlabel('T (K)')
    plt.ylabel(r'Cv ($k_b$/K')
    plt.xticks(np.arange(0, 6, step=1))

    ax3 = plt.subplot(223)
    plt.plot(T_list, M_list)
    plt.title('Magnetization')
    plt.xlabel('T (K)')
    plt.ylabel('Magnetization')
    plt.xticks(np.arange(0, 6, step=1))

    ax4 = plt.subplot(224)
    plt.plot(T_list, X_list)
    plt.title('Magnetic susceptibility')
    plt.xlabel('T (K)')
    plt.ylabel('$\chi$')
    plt.xticks(np.arange(0, 6, step=1))

    plt.subplots_adjust(hspace=0.5, wspace=0.4)

def plot_statistics2(N_list, E_list, Cv_list, M_list, X_list):
    """
    Function used to plot statistics with changing grid size
    :param N_list: Grid side length listing
    :param E_list: Energies
    :param Cv_list: Heat capacities
    :param M_list: Magnetizations
    :param X_list: Magnetic susceptibilities
    :return: None
    """
    plt.figure()
    ax1 = plt.subplot(221)
    plt.plot(1/N_list, E_list)
    plt.title('Energy of the system')
    plt.xlabel('1/N')
    plt.ylabel(r'Energy ($k_b$)')

    ax2 = plt.subplot(222)
    plt.plot(1/N_list, Cv_list)
    plt.title('Heat capacity')
    plt.xlabel('1/N')
    plt.ylabel(r'Cv ($k_b$/K')

    ax3 = plt.subplot(223)
    plt.plot(1/N_list, M_list)
    plt.title('Magnetization')
    plt.xlabel('1/N')
    plt.ylabel('Magnetization')

    ax4 = plt.subplot(224)
    plt.plot(1/N_list, X_list)
    plt.title('Magnetic susceptibility')
    plt.xlabel('1/N')
    plt.ylabel('$\chi$')

    plt.subplots_adjust(hspace=0.5, wspace=0.4)

def compare_different_neigborings():
    """
    Run test with nearest neighbor and nn + next nearest neighbors configurations and plot figures of them
    :return: None
    """
    # Nearest neighbours
    T_list = np.linspace(0.5, 6, 100)
    Walkers = createWalkers(10, 1)
    T_list, E_list, Cv_list, M_list, X_list = calc_statistics(Walkers, T_list)
    plot_statistics(T_list, E_list, Cv_list, M_list, X_list)

    # Nearest + next nearest neighbours
    Walkers = createWalkers(10, 2)
    T_list, E_list, Cv_list, M_list, X_list = calc_statistics(Walkers, T_list)
    plot_statistics(T_list, E_list, Cv_list, M_list, X_list)
    plt.show()

def finite_size_comparison():
    """
    Run test with changing grid size and plot figures of them. Only nearest neighbors are considered
    :return: None
    """
    T_list = (6.0,)
    num_walkers_list = np.array((4, 8, 16, 32))
    L = len(num_walkers_list)
    E_list = np.zeros(L, dtype=float)
    Cv_list = np.zeros(L, dtype=float)
    M_list = np.zeros(L, dtype=float)
    X_list = np.zeros(L, dtype=float)

    for ind, num_walkers in enumerate(num_walkers_list):
        Walkers = createWalkers(num_walkers, 1)
        T, E, Cv, M, X = calc_statistics(Walkers, T_list)
        E_list[ind] = E[0]
        Cv_list[ind] = Cv[0]
        M_list[ind] = M[0]
        X_list[ind] = X[0]

    plot_statistics2(num_walkers_list, E_list, Cv_list, M_list, X_list)
    plt.show()


if __name__=="__main__":
    compare_different_neigborings()
    finite_size_comparison()
