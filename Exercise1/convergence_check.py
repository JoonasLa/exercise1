import numpy as np
import matplotlib.pyplot as plt
from num_calculus import eval_derivative, eval_2nd_derivative, riemann_sum, trapedzoid_sum, simpson_sum, monte_carlo_integration

def test_derivatives():
    dx = 1
    f = lambda x: np.exp(x)
    logx = []
    logerror1 = []
    logerror2 = []
    while dx > 1e-5:
        logx.append(np.log10(dx))
        error = abs(eval_derivative(f, 0, dx)-1)
        logerror1.append(np.log10(error))
        
        error2 = abs(eval_2nd_derivative(f, 0, dx)-1)
        logerror2.append(np.log10(error2))
        
        dx /= 2
    fig = plt.figure()
    plt.plot(logx, logerror1, 'k.', label="Error of 1st derivative")
    plt.plot(logx, logerror2, 'r.', label="Error of 2nd derivative")
    plt.legend()
    plt.grid("on")
    plt.xlabel('log(dx)')
    plt.ylabel('log(error)')
    plt.show()

def test_integrals():
    points = 4

    logx = []
    logerror_r = []
    logerror_t = []
    logerror_s = []
    while points < 1e5:
        dx = np.pi/2/points
        logx.append(np.log10(dx))
        x = np.linspace(0, np.pi/2, points)
        f = np.sin(x)
        
        error_r = abs(riemann_sum(x, f)-1)
        logerror_r.append(np.log10(error_r))
        
        error_t = abs(trapedzoid_sum(x, f)-1)
        logerror_t.append(np.log10(error_t))
        
        error_s = abs(simpson_sum(x, f)-1)
        logerror_s.append(np.log10(error_s))
        
        points *= 2
        
    fig = plt.figure()
    plt.plot(logx, logerror_r, 'k.', label="Riemann sum")
    plt.plot(logx, logerror_t, 'r.', label="Trapedzoid sum")
    plt.plot(logx, logerror_s, 'b.', label="Simpson sum")
    plt.legend()
    plt.grid("on")
    plt.xlabel('log(dx)')
    plt.ylabel('log(error)')
    plt.show()

def test_monte_carlo():
    fun = lambda x: np.sin(x)
    points = []
    results = []
    errors = []
    for totpoints in range(20, 1000, 20):
        int, error = monte_carlo_integration(fun, 0, np.pi/2, totpoints, 10)
        results.append(int)
        errors.append(2*error)
        points.append(totpoints)
    fig = plt.figure()
    plt.errorbar(points, results, yerr=errors, fmt='k.')
    plt.plot([0, 1000], [1, 1], 'k--')
    plt.xlabel('Number of points')
    plt.ylabel('Integration result')
    plt.show()

if __name__ == "__main__":
    test_derivatives()
    test_integrals()
    test_monte_carlo()
    
