from setuptools import setup

setup(name='num_calculus',
      version='0.1',
      description='Package for numerical calculus',
      url='http://gitlab.com/JoonasLa/exercise1',
      author='Joonas Latukka',
      author_email='joonas.latukka@tuni.fi',
      license='MIT',
      packages=['differentiation'],
      zip_safe=False)

