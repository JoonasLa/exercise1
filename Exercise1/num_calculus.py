""" Exercise 1 for Computational Physics """
import numpy as np

def eval_derivative( function, x, dx ):
    # Calculates differences between f(x+dx) and f(x-dx)
    # and divides it by the difference 2*dx
    return (function(x+dx)-function(x-dx))/(2*dx)

def eval_2nd_derivative(function, x, dx):
    # Calculates second derivative by f''(x) = a1-2*a2+a3
    return (function(x-dx)-2*function(x)+function(x+dx))/dx**2

def riemann_sum(x, f):
    # Sums all values from f multiplied by bin-widths calculated from x
    result = 0
    width = x[1]-x[0]
    for i in range(len(f)-1):
        result += width*f[i]
    return result

def trapedzoid_sum(x, f):
    # Sums all values in f using trapedzoidal rule
    result = 0
    width = x[1]-x[0]
    for i in range(len(f)-1):
        # Calculate average between f(i+1) and f(i)
        result += width*(f[i+1]+f[i])/2
    return result

def simpson_sum(x, f):
    # Sums all values in f using Simpson's rule
    result = 0
    width = x[1]-x[0]
    for i in range(0, len(f)-2, 2):
        result += f[i]+4*f[i+1]+f[i+2]
    result *= width/3
    if len(f) % 2 == 0:
        result += width/12*(-f[-3]+8*f[-2]+5*f[-1])
    return result

def monte_carlo_integration(fun,xmin,xmax,blocks,iters):
    # Initialize array to save iteration results
    block_values=np.zeros((blocks,))
    # Calculate integration area width
    L=xmax-xmin
    for block in range(blocks):
        for i in range(iters):
            # Get random x-value inside integration area
            x = xmin+np.random.rand()*L
            # Add function value in that point to block_array
            block_values[block]+=fun(x)
        # Values are averaged by dividing it by iter amount
        block_values[block]/=iters
    # Integral value and standard error are calculated with mean and std-operators.
    I = L*np.mean(block_values)
    dI = L*np.std(block_values)/np.sqrt(blocks)
    return I,dI

def test_first_derivative():
    # Test derivative for function f(x) = x^2
    function = lambda x: x**2
    x = 2
    dx = 0.01
    der = eval_derivative(function, x, dx)
    print("f'(2) = ", der)
    
def test_second_derivative():
    # Test second derivative for function f(x) = x^3
    function = lambda x: x**3
    x = 2.0
    dx = 0.01
    der = eval_2nd_derivative(function, x, dx)
    print("f''(2) = ", der)

def test_riemann_sum():
    x = np.linspace(0,np.pi/2,100)
    f = np.sin(x)
    I = riemann_sum(x,f)
    print("Integral of sin(x) from 0->pi/2 using riemann sum: ", I)

def test_trapedzoidal_sum():
    x = np.linspace(0,np.pi/2,100)
    f = np.sin(x)
    I = trapedzoid_sum(x,f)
    print("Integral of sin(x) from 0->pi/2 using trapedzoidal rule: ", I)
    
def test_simpson_sum():
    x = np.linspace(0,np.pi/2,100)
    f = np.sin(x)
    I = simpson_sum(x,f)
    print("Integral of sin(x) from 0->pi/2 using simpson's rule: ", I)
    
def test_monte_carlo():
    I,dI=monte_carlo_integration(np.sin,0.,np.pi/2,10,100)
    print(I,'+/-', 2*dI)
    
def main():
    test_first_derivative()
    test_second_derivative()
    test_riemann_sum()
    test_trapedzoidal_sum()
    test_simpson_sum()
    test_monte_carlo()

if __name__=="__main__":
    main()
