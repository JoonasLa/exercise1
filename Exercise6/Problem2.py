import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import simps

def eval_derivative(function, x, dx ):
    return (function(x+dx)-function(x-dx))/2/dx

def fem(rho, xgrid):
    N = xgrid.shape[0]
    A = np.zeros((N, N))
    b = np.zeros(N)
    h = xgrid[1]-xgrid[0]
    for i in range(1,N-1):
        for j in range(1, N-1):
            # Use analytical solution to calculate the A-matrix
            if i == j:
                A[j, i] = 2/h
            elif abs(i-j) == 1:
                A[j, i] = -1/h
    for i in range(1, N-1):
        # Use analytical solution for integrals to calculate the b-vector
        b[i] = np.pi/h*(xgrid[i-1]+xgrid[i+1]-2*xgrid[i])*np.cos(np.pi*xgrid[i]) + \
            1/h*(2*np.sin(np.pi*xgrid[i])-np.sin(np.pi*xgrid[i-1])-np.sin(np.pi*xgrid[i+1]))
    A[0, 0] = 1
    A[-1, -1] = 1
    a = np.linalg.inv(A) @ b
    return a

def fem2(rho, xgrid):
    N = xgrid.shape[0]
    A = np.zeros((N, N))
    b = np.zeros(N)
    h = xgrid[1]-xgrid[0]
    for i in range(1,N-1):
        for j in range(1, N-1):
            # Calculate numerically the integral ui'(x)uj'(x)dx
            fi = lambda x: u(i, x, xgrid)
            fj = lambda x: u(j, x, xgrid)
            xx = np.linspace(0, 1, 100)
            yy = np.zeros(100)
            for k in range(len(xx)):
                yy[k] = eval_derivative(fi, xx[k], 0.0001) * eval_derivative(fj, xx[k], 0.0001)
            A[i, j] = simps(yy, xx)
    A[0, 0] = 1
    A[-1, -1] = 1
    for i in range(1, N-1):
        # Calculating integral with simpsons rule. Must be multiplied by pi for some reason?
        xx = np.linspace(0, 1, 10)
        yy = np.zeros(10)
        for j in range(len(xx)):
            yy[j] = np.pi*rho(xx[j])*u(i, xx[j], xgrid)
        b[i] = simps(yy, xx)
    b[0] = 0
    b[-1] = 0
    a = np.linalg.inv(A) @ b
    return a


def u(i, x, xgrid):
    if xgrid[i-1] < x < xgrid[i]:
        return (x-xgrid[i-1])/(xgrid[i]-xgrid[i-1])
    if xgrid[i] < x < xgrid[i+1]:
        return (xgrid[i+1] - x)/(xgrid[i+1]-xgrid[i])
    return 0

def femsolver(x, a, xgrid):
    y = 0
    h = xgrid[1]-xgrid[0]
    for i in range(1, len(xgrid)-1):
        y += u(i, x, xgrid)*a[i]
    return y


def main():
    N = 20
    xgrid = np.linspace(0, 1, N)
    rho = lambda x: np.pi*np.sin(np.pi*x)
    a = fem(rho, xgrid)
    xx = np.linspace(0, 1, 50)
    y1 = np.zeros(50)
    a2 = fem2(rho, xgrid)
    y2 = np.zeros(50)
    for i in range(len(xx)):
        y1[i] = femsolver(xx[i], a, xgrid)
        y2[i] = femsolver(xx[i], a2, xgrid)
    fig = plt.figure()
    plt.plot(xx, y1, label='Finite element method')
    plt.plot(xx, np.sin(np.pi*xx), label='Analytical method')
    plt.plot(xx, y2, label='Finite element method, num integration')
    plt.legend()
    plt.show()

if __name__ == "__main__":
    main()