from numpy import *
from matplotlib.pyplot import *

def main():
    vmcE = array([-87.797656])
    Jastrow = array([-89.808824, -90.215671, -90.274830, -90.268042,
                     -90.284583, -90.272789, -90.249856, -90.281485,
                     -90.253904, -90.256952])
    dmc01 = -90.312670
    dmc005 = -90.479017
    objects = ('vmc', 'Jastrow', 'dmc 0.01', 'dmc 0.005')
    y = arange(len(objects))

    figure()
    bar(y, [-vmcE, -mean(Jastrow), -dmc01, -dmc005], color='r', align='center', alpha=0.5)
    xticks(y, objects)
    ylabel('- Energy')
    title('Problem 4 energies')
    show()


if __name__ == '__main__':
    main()