"""
Simple VMC and DMC code for FYS-4096 course at TUT

- Fill in more comments based on lecture slides
- Follow assignment instructions  


By Ilkka Kylanpaa
"""

import numpy as np
import matplotlib.pyplot as plt
import time


class Walker:
    def __init__(self, Re, spins, Rn, Zn, dim):
        """
        Random walker for metropolis algorithm. Coordinates are updated by the
        algorithm.

        # Example:
        >>> Walker(Re=[np.array([0.5, 0, 0]), np.array([-0.5, 0, 0])], \
                   spins=[0, 1], \
                   Rn=[np.array([-0.7, 0, 0]), np.array([0.7, 0, 0])], \
                   Zn=[1.0, 1.0], \
                   dim=3)

        :param Re:      Position coordinates of electrons.
        :param spins:   Spins of electrons
        :param Rn:      Position coordinates of nucleai
        :param Zn:      Charges of nucleai
        :param dim:
        """
        self.Ne = len(Re)
        self.Re = Re
        self.spins = spins
        self.Nn = len(Rn)
        self.Rn = Rn
        self.Zn = Zn
        self.sys_dim = dim

    def w_copy(self):
        """
        :return: Copy of walker
        """
        return Walker(Re=self.Re,
                      spins=self.spins,
                      Rn=self.Rn,
                      Zn=self.Zn,
                      dim=self.sys_dim)


def vmc_run(Nblocks, Niters, Delta, Walkers_in, Ntarget):
    """
    Variational monte carlo loop. Sample energy of wave function.
    :param Nblocks:     Number of blocks (used to in calculating std)
    :param Niters:      Number of iterations in block
    :param Delta:       Time step
    :param Walkers_in:  list of walkers, only one is needed
    :param Ntarget:     Number of walkers out (=1 in this code)
    :return:
    """
    Eb = np.zeros((Nblocks,))
    Accept = np.zeros((Nblocks,))

    vmc_walker = Walkers_in[0]  # just one walker needed
    Walkers_out = []
    for i in range(Nblocks):
        for j in range(Niters):
            # moving only electrons
            for k in range(vmc_walker.Ne):
                R = vmc_walker.Re[k]
                Psi_R = wfs(vmc_walker)

                # move the particle
                vmc_walker.Re[k] = R + Delta * (
                            np.random.rand(vmc_walker.sys_dim) - 0.5)

                # calculate wave function at the new position
                Psi_Rp = wfs(vmc_walker)

                # calculate the sampling probability
                A_RtoRp = min((Psi_Rp / Psi_R) ** 2, 1.0)

                # Metropolis
                if (A_RtoRp > np.random.rand()):
                    Accept[i] += 1.0 / vmc_walker.Ne
                else:
                    vmc_walker.Re[k] = R
                # end if
            # end for
            Eb[i] += E_local(vmc_walker)
        # end for
        if (len(Walkers_out) < Ntarget):
            Walkers_out.append(vmc_walker.w_copy())
        Eb[i] /= Niters
        Accept[i] /= Niters
        print('Block {0}/{1}'.format(i + 1, Nblocks))
        print('    E   = {0:.5f}'.format(Eb[i]))
        print('    Acc = {0:.5f}'.format(Accept[i]))

    return Walkers_out, Eb, Accept


def dmc_run(Nblocks, Niters, Walkers, tau, E_T, Ntarget):
    """
    Diffusion Monte Carlo run
    :param Nblocks:     Number of blocks (used to in calculating std)
    :param Niters:      Number of iterations in block
    :param Walkers:     Walkers in
    :param tau:         Time step
    :param E_T:         Some reference energy used to spawn walkers
    :param Ntarget:     Number of walkers out
    :return:
    """
    max_walkers = 2 * Ntarget
    lW = len(Walkers)
    while len(Walkers) < Ntarget:
        Walkers.append(Walkers[max(1, int(lW * np.random.rand()))].w_copy())

    Eb = np.zeros((Nblocks,))
    Accept = np.zeros((Nblocks,))
    AccCount = np.zeros((Nblocks,))

    obs_interval = 5
    mass = 1

    for i in range(Nblocks):
        EbCount = 0
        for j in range(Niters):
            Wsize = len(Walkers)
            Idead = []
            for k in range(Wsize):
                for np_ in range(Walkers[k].Ne):
                    Acc = 0.0
                    R = Walkers[k].Re[np_]
                    Psi_R = wfs(Walkers[k])
                    DriftPsi_R = 2 * Gradient(Walkers[k],
                                              np_) / Psi_R * tau / 2 / mass
                    E_L_R = E_local(Walkers[k])

                    DeltaR = np.random.randn(Walkers[k].sys_dim)
                    logGf = -0.5 * np.dot(DeltaR, DeltaR)

                    Walkers[k].Re[np_] = R + DriftPsi_R + DeltaR * np.sqrt(
                        tau / mass)

                    Psi_Rp = wfs(Walkers[k])
                    DriftPsi_Rp = 2 * Gradient(Walkers[k],
                                               np_) / Psi_Rp * tau / 2 / mass
                    E_L_Rp = E_local(Walkers[k])

                    DeltaR = R - Walkers[k].Re[np_] - DriftPsi_Rp
                    logGb = -np.dot(DeltaR, DeltaR) / 2 / tau * mass

                    A_RtoRp = min(1, (Psi_Rp / Psi_R) ** 2 * np.exp(logGb - logGf))

                    if (A_RtoRp > np.random.rand()):
                        Acc += 1.0 / Walkers[k].Ne
                        Accept[i] += 1
                    else:
                        Walkers[k].Re[np_] = R

                    AccCount[i] += 1

                tau_eff = Acc * tau
                GB = np.exp(-(0.5 * (E_L_R + E_L_Rp) - E_T) * tau_eff)
                MB = int(np.floor(GB + np.random.rand()))

                if MB > 1:
                    for n in range(MB - 1):
                        if (len(Walkers) < max_walkers):
                            Walkers.append(Walkers[k].w_copy())
                elif MB == 0:
                    Idead.append(k)

            Walkers = DeleteWalkers(Walkers, Idead)

            # Calculate observables every now and then
            if j % obs_interval == 0:
                EL = Observable_E(Walkers)
                Eb[i] += EL
                EbCount += 1
                E_T += 0.01 / tau * np.log(Ntarget / len(Walkers))

        Nw = len(Walkers)
        dNw = Ntarget - Nw
        for kk in range(abs(dNw)):
            ind = int(np.floor(len(Walkers) * np.random.rand()))
            if (dNw > 0):
                Walkers.append(Walkers[ind].w_copy())
            elif dNw < 0:
                Walkers = DeleteWalkers(Walkers, [ind])

        Eb[i] /= EbCount
        Accept[i] /= AccCount[i]
        print('Block {0}/{1}'.format(i + 1, Nblocks))
        print('    E   = {0:.5f}'.format(Eb[i]))
        print('    Acc = {0:.5f}'.format(Accept[i]))

    return Walkers, Eb, Accept


def DeleteWalkers(Walkers, Idead):
    if (len(Idead) > 0):
        if (len(Walkers) == len(Idead)):
            Walkers = Walkers[0]
        else:
            Idead.sort(reverse=True)
            for i in range(len(Idead)):
                del Walkers[Idead[i]]

    return Walkers


def H_1s(r1, r2):
    return np.exp(-np.sqrt(sum((r1 - r2) ** 2)))


def wfs(Walker):
    """
    Wave function of H2 including jastrow correlationg correction.

    :param Walker:
    :return:
    """
    # H2 approx
    f = H_1s(Walker.Re[0], Walker.Rn[0]) + H_1s(Walker.Re[0], Walker.Rn[1])
    f *= (H_1s(Walker.Re[1], Walker.Rn[0]) + H_1s(Walker.Re[1], Walker.Rn[1]))

    J = 0.0
    # Jastrow e-e
    for i in range(Walker.Ne - 1):
        for j in range(i + 1, Walker.Ne):
            r = np.sqrt(sum((Walker.Re[i] - Walker.Re[j]) ** 2))
            if (Walker.spins[i] == Walker.spins[j]):
                J += 0.25 * r / (1.0 + 1.0 * r)
            else:
                J += 0.5 * r / (1.0 + 1.0 * r)

    # Jastrow e-Ion
    for i in range(Walker.Ne):
        for j in range(Walker.Nn):
            r = np.sqrt(sum((Walker.Re[i] - Walker.Rn[j]) ** 2))
            J -= Walker.Zn[j] * r / (1.0 + 100.0 * r)

    return f * np.exp(J)


def potential(Walker):
    """
    Potential part of local energy

    :param Walker:
    :return:
    """

    V = 0.0
    r_cut = 1.0e-4
    # e-e
    for i in range(Walker.Ne - 1):
        for j in range(i + 1, Walker.Ne):
            r = np.sqrt(sum((Walker.Re[i] - Walker.Re[j]) ** 2))
            V += 1.0 / max(r_cut, r)

    # e-Ion
    for i in range(Walker.Ne):
        for j in range(Walker.Nn):
            r = np.sqrt(sum((Walker.Re[i] - Walker.Rn[j]) ** 2))
            V -= Walker.Zn[j] / max(r_cut, r)

    # Ion-Ion
    for i in range(Walker.Nn - 1):
        for j in range(i + 1, Walker.Nn):
            r = np.sqrt(sum((Walker.Rn[i] - Walker.Rn[j]) ** 2))
            V += 1.0 / max(r_cut, r)

    return V


def Local_Kinetic(Walker):
    """
    Kinetic part of local energy

    :param Walker:
    :return:
    """

    # laplacian -0.5 \nabla^2 \Psi / \Psi
    h = 0.001
    h2 = h * h
    K = 0.0
    Psi_R = wfs(Walker)
    for i in range(Walker.Ne):
        for j in range(Walker.sys_dim):
            Y = Walker.Re[i][j]
            Walker.Re[i][j] -= h
            wfs1 = wfs(Walker)
            Walker.Re[i][j] += 2.0 * h
            wfs2 = wfs(Walker)
            K -= 0.5 * (wfs1 + wfs2 - 2.0 * Psi_R) / h2
            Walker.Re[i][j] = Y
    return K / Psi_R


def Gradient(Walker, particle):

    h = 0.001
    dPsi = np.zeros(shape=np.shape(Walker.Re[particle]))
    for i in range(Walker.sys_dim):
        Y = Walker.Re[particle][i]
        Walker.Re[particle][i] -= h
        wfs1 = wfs(Walker)
        Walker.Re[particle][i] += 2.0 * h
        wfs2 = wfs(Walker)
        dPsi[i] = (wfs2 - wfs1) / 2 / h
        Walker.Re[particle][i] = Y

    return dPsi


def E_local(Walker):
    return Local_Kinetic(Walker) + potential(Walker)


def Observable_E(Walkers):
    E = 0.0
    Nw = len(Walkers)
    for i in range(Nw):
        E += E_local(Walkers[i])
    E /= Nw
    return E

def plot(ax, Eb, num_blocks, title):
    """Plot walkers convergence of energy by block."""

    E = Eb.mean()
    E_err = Eb.std() / np.sqrt(num_blocks)

    x = np.arange(len(Eb))
    ax.plot(x, Eb)
    ax.plot([x[0], x[-1]], [E, E], ls="-", c="k")
    ax.plot([x[0], x[-1]], [E+E_err/2, E+E_err/2], ls="--", c="k", lw=1.0)
    ax.plot([x[0], x[-1]], [E-E_err/2, E-E_err/2], ls="--", c="k", lw=1.0)
    ax.set_title(title)
    ax.set_xlabel("block")
    ax.set_ylabel("energy")

    ax.text(0.1, 0.1, "E = {:.3f} ± {:.3f} Ha".format(E, E_err),
            transform=ax.transAxes)



def main():
    Walkers = []
    Walkers.append(Walker(Re=[np.array([0.5, 0, 0]), np.array([-0.5, 0, 0])],
                          spins=[0, 1],
                          Rn=[np.array([-0.7, 0, 0]), np.array([0.7, 0, 0])],
                          Zn=[1.0, 1.0],
                          dim=3))

    vmc_only = False
    Ntarget = 100
    vmc_time_step = 2.0
    num_vmc_blocks = 100
    num_dmc_blocks = 10

    start=time.time()
    Walkers, Eb, Acc = vmc_run(num_vmc_blocks, 50, vmc_time_step, Walkers, Ntarget)
    end=time.time()
    vmc_time = (end-start) / num_vmc_blocks

    fig, (ax1, ax2) = plt.subplots(1,2, figsize=(8,4))
    fig.suptitle("Energy convergence, not including Jastrow term.")
    plot(ax1, Eb, num_vmc_blocks, title="VMC walker energy")
    print("VMC time per block", vmc_time)

    if not vmc_only:
        start=time.time()
        Walkers, Eb_dmc, Accept_dmc = dmc_run(num_dmc_blocks, 10, Walkers, 0.05,
                                              np.mean(Eb), Ntarget)
        end=time.time()
        dmc_time = (end-start) / num_dmc_blocks
        plot(ax2, Eb_dmc, num_vmc_blocks, title="DMC walker energy")
        print("DMC time per block", dmc_time)

    fig.tight_layout()
    plt.show()


if __name__ == "__main__":
    main()
